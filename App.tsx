import React, {Component} from 'react';
import {StyleSheet, View, LogBox} from 'react-native';

require('moment/locale/vi');
import {connect} from 'react-redux';
// import * as Apis from './src/networking/Apis'
// import OneSignal from "react-native-onesignal"
import * as Routers from './src/navigations/Navigations';
import {reLogin} from './src/redux/actions/loginAction';
// import { changeDataOneSignal } from './src/modules/onesignal'

class App extends Component {
  constructor(props) {
    super(props);
    // console.disableYellowBox = true;
    LogBox.ignoreAllLogs(true);
    // onesignal app id
    // OneSignal.init(Apis.AppIdOneSignal);
    // OneSignal.addEventListener('ids', this.onIds);
    // OneSignal.configure(); 	// triggers the ids event
    // // //set top alert when foreground
    // OneSignal.inFocusDisplaying(2);
    // this.onIds = this.onIds.bind(this)
  }

  componentDidMount() {
    Routers.goHome();
    // this.props.reLogin()
    // SplashScreen.hide()
    // Apis.getContact('?tk=0968675568')
  }

  componentWillUnmount() {
    // OneSignal.removeEventListener('ids', this.onIds);
  }

  // allways trigger with a id
  // onIds(device) {
  //   changeDataOneSignal(device)
  // }

  render() {
    return <View style={styles.container} />;
  }
}

const mapDispatchToProps = {
  reLogin,
};

export default connect(null, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
