const uri =
  'https://conmik.com/uploaded/files/PT-AE8000EA_Projection_Screen_Size_Information.pdf';

import React, {Component} from 'react';
import {StyleSheet, ScrollView} from 'react-native';

import * as Views from '../views/ViewManagers';
import * as Routers from '../navigations/Navigations';
import WebView from 'react-native-webview';

class FilePDF extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      is_error: false,
    };
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  hideLoading = () => {
    if (this._isMounted) {
      this.setState({isLoading: false});
    }
    if (this.BaseView) {
      this.BaseView.refresh();
    }
  };

  _onError = () => {
    this.hideLoading();
    this.setState({is_error: true});
  };

  _onNoData = () => {
    this.setState({is_error: false, isLoading: true});
  };

  render() {
    const {isLoading, is_error} = this.state;
    const title = 'file PDF';
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title={title}
        onLeftPress={this.goBack}
        visibleError={is_error}
        onNoData={this._onNoData}
        footerView={
          <Views.ProgressLoading
            visible={isLoading}
            backgroundColor="transparent"
          />
        }>
        <ScrollView contentContainerStyle={styles.container}>
          <WebView
            useWebKit={true}
            style={{flex: 1}}
            source={{uri}}
            onLoadEnd={this.hideLoading}
            onError={this._onError}
            onShouldStartLoadWithRequest={() => true}
          />
        </ScrollView>
      </Views.BaseView>
    );
  }
}

export default FilePDF;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
