import React from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';

import * as Fonts from '../../config/Fonts';

class Brachs extends React.PureComponent {
  render() {
    const {brands} = this.props;
    return (
      <View
        style={{
          marginTop: 25,
          backgroundColor: '#feeeee',
          paddingHorizontal: 10,
          paddingVertical: 15,
        }}>
        <FlatList
          scrollEnabled={false}
          data={brands}
          keyExtractor={(item, index) => index + ''}
          renderItem={({item, index}) => (
            <View
              style={{
                marginVertical: 5,
                borderRadius: 5,
                padding: 20,
                backgroundColor: 'white',
              }}>
              <Text style={styles.name}>
                CHI NHÁNH TẠI {item.name || '---'}
              </Text>

              <Text style={styles.phone}>
                Điện thoại: {item.phone || '---'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  color: '#606060',
                  fontFamily: Fonts.myriadpro_regular,
                }}>
                Địa chỉ: {item.address || '---'}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
}

export default Brachs;

const styles = StyleSheet.create({
  name: {
    color: '#284e72',
    fontSize: 16,
    marginBottom: 10,
    fontFamily: Fonts.myriadpro_semibold,
  },
  phone: {
    fontSize: 15,
    color: '#606060',
    marginBottom: 5,
    fontFamily: Fonts.myriadpro_regular,
  },
});
