import React from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;

class Headquarters extends React.PureComponent {
  render() {
    const {Default, info} = this.props;
    return (
      <View style={{marginTop: 25}}>
        <Text style={styles.name}>{Default.company.toUpperCase()}</Text>

        <Text style={styles.head}>
          {'Trụ sở chính tại ' + (info.center || '---')}
        </Text>

        <View
          style={{
            flexDirection: 'row',
            width: WIDTH_COVER,
            margin: 10,
            height: 5,
          }}>
          <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
          <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
          <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
        </View>

        <View style={{paddingHorizontal: 30, paddingBottom: 10, paddingTop: 5}}>
          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Địa chỉ : </Text>
            {info.address || '---'}
          </Text>

          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Phone : </Text>
            {info.phone || '---'}
          </Text>

          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Mail : </Text>
            {info.mail || '---'}
          </Text>

          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Website : </Text>
            {info.website || '---'}
          </Text>

          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Fax : </Text>
            {info.fax || '---'}
          </Text>

          <Text style={styles.text}>
            <Text style={{fontWeight: 'bold'}}>Hotline : </Text>
            {info.hotline || '---'}
          </Text>

          <View
            style={{
              backgroundColor: '#aadaff',
              padding: 15,
              marginTop: 5,
              borderRadius: 5,
            }}>
            <Text style={styles.text}>
              <Text style={{fontWeight: 'bold'}}>Fanpage : </Text>
              {info.fanpage || '---'}
            </Text>
            <Text style={styles.text}>
              <Text style={{fontWeight: 'bold'}}>Youtube : </Text>
              {info.youtube || '---'}
            </Text>
            <Text style={styles.text}>
              <Text style={{fontWeight: 'bold'}}>Twitter : </Text>
              {info.twitter || '---'}
            </Text>
            <Text style={styles.text}>
              <Text style={{fontWeight: 'bold'}}>Pinterest : </Text>
              {info.pinterest || '---'}
            </Text>
            <Text style={styles.text}>
              <Text style={{fontWeight: 'bold'}}>Zalo Page : </Text>
              {info.zalo || '---'}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default Headquarters;

const styles = StyleSheet.create({
  name: {
    color: '#284e72',
    fontSize: 17,
    marginHorizontal: 15,
    fontFamily: Fonts.myriadpro_semibold,
  },
  head: {
    color: '#284e72',
    fontStyle: 'italic',
    fontSize: 15,
    marginHorizontal: 15,
    // fontFamily: Fonts.myriadpro_regular
  },
  text: {
    marginBottom: 5,
    fontSize: 15,
    color: '#606060',
    fontFamily: Fonts.myriadpro_regular,
  },
});
