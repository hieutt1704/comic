import React, {PureComponent} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Apis from '../../networking/Apis';
import FastImage from 'react-native-fast-image';
import * as Icons from '../../config/IconManagers';

const {width} = Dimensions.get('window');

class Header extends PureComponent {
  render() {
    const {banner, info, onPressHotline} = this.props;
    const imageCover = banner
      ? banner.indexOf('http') == 0
        ? {uri: banner}
        : {uri: Apis.domain + banner}
      : Icons.services;
    return (
      <View style={styles.image}>
        <FastImage source={imageCover} style={styles.image} />

        <TouchableOpacity
          style={styles.formHotline}
          activeOpacity={0.8}
          onPress={onPressHotline}>
          <View style={styles.vHotline}>
            <View
              style={{
                alignItems: 'flex-end',
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text style={styles.title}>HOTLINE</Text>
              <Text style={styles.text}>{info.hotline || '---'}</Text>
            </View>
          </View>

          <View style={styles.vIcon}>
            <FastImage
              source={Icons.phone}
              style={styles.icon}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Header;

const styles = StyleSheet.create({
  image: {
    height: width / 2,
    width,
    backgroundColor: Colors.silverCONMIK,
  },
  formHotline: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
  vHotline: {
    backgroundColor: Colors.redCONMIK,
    paddingRight: 55,
    paddingLeft: 15,
    borderRadius: 50,
    marginBottom: 7.5,
    height: 35,
  },
  title: {
    color: 'white',
    fontSize: 11,
    marginBottom: Platform.OS == 'ios' ? 0 : -3,
    fontFamily: Fonts.utm_avo_bold,
  },
  text: {
    color: 'white',
    fontSize: 11,
    fontFamily: Fonts.utm_avo,
  },
  vIcon: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'white',
    position: 'absolute',
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 30,
    width: 30,
  },
});
