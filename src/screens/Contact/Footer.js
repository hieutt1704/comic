import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Apis from '../../networking/Apis';
import FastImage from 'react-native-fast-image';
import * as Icons from '../../config/IconManagers';

const {width} = Dimensions.get('window');
const publics = [
  {icon: Icons.facebook_red},
  {icon: Icons.youtube_red},
  {icon: Icons.twitter_red},
  {icon: Icons.google_red},
];

class Footers extends React.PureComponent {
  render() {
    const {Default, info, onPressSocialNetwork} = this.props;
    const blackQR =
      info && info.black_qr
        ? info.black_qr.indexOf('http') == 0
          ? {uri: info.black_qr}
          : {uri: Apis.domain + info.black_qr}
        : Icons.black_qr;
    const dmca =
      info && info.dmca
        ? info.dmca.indexOf('http') == 0
          ? {uri: info.dmca}
          : {uri: Apis.domain + info.dmca}
        : Icons.dmca;
    const redQR =
      info && info.red_qr
        ? info.red_qr.indexOf('http') == 0
          ? {uri: info.red_qr}
          : {uri: Apis.domain + info.red_qr}
        : Icons.red_qr;
    return (
      <View style={styles.container}>
        <Text style={styles.name}>{Default.eng}</Text>

        <View style={styles.dash} />

        <Text style={[styles.title, {marginBottom: 5}]}>
          Văn phòng đại diện conmik tại Việt Nam
        </Text>

        <Text style={styles.italic}>
          {'Trụ sở chính tại ' + (info.center || '---')}
        </Text>

        <Text style={styles.text}>Địa chỉ: {info.address || '---'}</Text>

        <Text style={styles.text}>Phone: {info.phone || '---'}</Text>

        <Text style={styles.text}>Mail: {info.mail || '---'}</Text>

        <Text style={styles.text}>Fax: {info.fax || '---'}</Text>

        <FastImage source={blackQR} style={styles.image} />

        <Text style={styles.title}>Văn phòng giới thiệu sản phẩm</Text>

        <Text style={styles.text}>Địa chỉ: {info.address2 || '---'}</Text>

        <Text style={styles.text}>Tel: {info.tel || '---'}</Text>

        <Text style={styles.text}>Mail: {info.gmail || '---'}</Text>

        <Text style={styles.text}>Fax: {info.fax2 || '---'}</Text>

        <FastImage source={dmca} style={styles.image} />

        <Text style={styles.title}>Chúng tôi trên mạng xã hội</Text>

        <View style={{flexDirection: 'row'}}>
          {publics.map((item, index) => (
            <TouchableOpacity
              style={styles.vIcon}
              key={index}
              activeOpacity={0.8}
              onPress={() => onPressSocialNetwork(item)}>
              <FastImage source={item.icon} style={styles.icon} />
            </TouchableOpacity>
          ))}
        </View>

        <FastImage source={redQR} style={styles.image} />
      </View>
    );
  }
}

export default Footers;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eb1e25',
    padding: 30,
  },
  name: {
    color: 'white',
    fontSize: 19,
    fontWeight: 'bold',
  },
  dash: {
    height: 0.5,
    backgroundColor: 'white',
    width: '100%',
    marginTop: 5,
    marginBottom: 15,
  },
  italic: {
    fontStyle: 'italic',
    fontSize: 15,
    color: 'white',
    marginBottom: 20,
    // fontFamily: Fonts.myriadpro_regular
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 17,
    marginBottom: 20,
    fontFamily: Fonts.myriadpro_semibold,
  },
  text: {
    marginBottom: 5,
    fontSize: 15,
    color: 'white',
    fontFamily: Fonts.myriadpro_regular,
  },
  image: {
    width: width - 60,
    height: width - 60,
    backgroundColor: 'white',
    marginTop: 20,
    marginBottom: 30,
  },
  vIcon: {
    width: 40,
    height: 40,
    backgroundColor: 'white',
    borderRadius: 20,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
});
