import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  RefreshControl,
  Linking,
  Platform,
} from 'react-native';

import * as Utils from '../../Utils';
import {connect} from 'react-redux';
import * as Apis from '../../networking/Apis';
import * as Views from '../../views/ViewManagers';
import * as Icons from '../../config/IconManagers';
import * as Storage from '../../modules/asyncStorge';
import * as Routers from '../../navigations/Navigations';

import MapComp from './Map';
import Brachs from './Brachs';
import Header from './Header';
import Footers from './Footer';
import Headquarters from './HeadQuarters';

const Default = {
  company: 'Công ty cổ phần conmik việt nam',
  eng: 'CONMIK GROUP GLOBAL',
  center: 'Hà Nội',
  address: 'Tầng 2, 150, tổ 10, Đường Láng, P.Láng Thượng, Q.Đống Đa, Hà Nội',
  phone: '0967 966 789 - 0243.2595539',
  mail: 'info@conmik.com',
  fax: '043.7661900',
  hotline: '0969.92.45.76',
  website: 'conmik.com',
  fanpage: 'https://www.facebook.com/conmikvn',
  youtube: 'https://bit.ly/2L5HOWE',
  twitter: 'https://twitter.com/conmikvn',
  pinterest: 'https://www.pinterest.com/conmikvn/',
  zalo: 'https://zalo.me/4604459548335720635',
  address2: 'Lô G11 khu liền kề 19A-19B X7 Lê Trọng Tấn, Hà Đông, Hà Nội',
  tel: '0246 260 0303',
  mail2: 'chongthamconmik@gmail.com',
  fax2: '0243 7661900',
};

class Contact extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      is_refresh: false,
      headquarters: {},
      brands: [],
      banner: '',
    };
  }

  componentDidMount = async () => {
    if (this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    const data_contact = await Storage.getStorageData(Storage.contact);
    if (data_contact) {
      const contact = JSON.parse(data_contact);
      this.setState({
        headquarters: contact.info,
        brands: contact.brands,
        banner: contact.banner,
      });
      this._getData();
    } else {
      await this._getData();
    }
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  componentWillUnmount() {
    this._mounted = false;
  }

  _onMenu = () => {
    Routers.showMenu(true);
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  _getData = showFail => {
    const {token, username} = this.props.loginState;
    const params = `?tk=${username}&token=${token}`;
    return Apis.getContact(params).then(res => {
      console.log('lien he', params, res);
      if (res.status == 1 && res.data) {
        if (this._mounted) {
          this.setState({
            headquarters: res.data.info,
            brands: res.data.brands,
            banner: res.data.banner,
          });
        }
        Storage.setStorageData(Storage.contact, JSON.stringify(res.data));
      } else if (showFail) {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData(true);
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _onPressSocialNetwork = social => {
    const {headquarters} = this.state;
    switch (social.icon) {
      case Icons.facebook_red:
        this._openUrl(headquarters.fanpage);
        break;
      case Icons.youtube_red:
        this._openUrl(headquarters.youtube);
        break;
      case Icons.twitter_red:
        this._openUrl(headquarters.twitter);
        break;
      case Icons.zalo_red:
        this._openUrl(headquarters.zalo);
        break;
      case Icons.google_red:
        this._sendMail(headquarters.gmail);
        break;
      default:
        break;
    }
  };

  _sms = phone => {
    this._link(
      `sms:${phone}`,
      'Không thể nhắn tin tới số điện thoại này',
      phone,
    );
  };

  _call = phone => {
    let phoneNumber =
      Platform.OS !== 'android' ? `telprompt:${phone}` : `tel:${phone}`;
    this._link(phoneNumber, 'Không thể gọi tới số điện thoại này', phone);
  };

  _sendMail = mail => {
    this._link(`mailto:${mail}`, 'Không thể mở email này');
  };

  _openUrl = url => {
    if (url) {
      this._link(url, 'Không thể mở đường dẫn này');
    } else {
      Utils.showToast('Không thể mở đường dẫn này');
    }
  };

  _link(url, textFail) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          Utils.showToast(textFail);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => Utils.showToast(textFail));
  }

  render() {
    const {banner, brands, is_refresh, headquarters} = this.state;
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title="Liên hệ"
        logo={Icons.logo}
        rightIcon={Icons.search}
        leftIcon={Icons.menu}
        onLeftPress={this._onMenu}
        onRightPress={this._goSearch}>
        <ScrollView
          style={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          <Header
            banner={banner}
            info={headquarters}
            onPressHotline={() => this._call(headquarters.hotline)}
          />

          <Headquarters Default={Default} info={headquarters} />

          <MapComp info={headquarters} />

          <Brachs brands={brands} />

          <Footers
            Default={Default}
            info={headquarters}
            onPressSocialNetwork={this._onPressSocialNetwork}
          />
        </ScrollView>
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Contact);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
