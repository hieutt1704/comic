import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';

import * as Colors from '../../config/Colors';
import FastImage from 'react-native-fast-image';
import * as Icons from '../../config/IconManagers';
import MapView, {Marker} from 'react-native-maps';

const {width} = Dimensions.get('window');
const WIDTH_MAP = width - 20;
const HEIGHT_MAP = (3 * WIDTH_MAP) / 2;
const RATIO = 5;
const DEFAULT = {
  latitude: 21.023247,
  longitude: 105.799722,
  latitudeDelta: 0.003,
  longitudeDelta: 0.003,
};

class MapComp extends React.Component {
  constructor(props) {
    super(props);
    const {info} = this.props;
    const latitude =
      info && info.location && info.location.latitude
        ? parseFloat(info.location.latitude)
        : DEFAULT.latitude;
    const longitude =
      info && info.location && info.location.longitude
        ? parseFloat(info.location.longitude)
        : DEFAULT.longitude;
    this.state = {
      region: {
        latitude,
        longitude,
        latitudeDelta: 0.003,
        longitudeDelta: 0.003,
      },
      local: {
        latitude,
        longitude,
      },
    };
  }

  componentDidUpdate(prevProps) {
    const {info} = this.props;
    if (JSON.stringify(info) != JSON.stringify(prevProps.info)) {
      const latitude =
        info && info.location && info.location.latitude
          ? parseFloat(info.location.latitude)
          : DEFAULT.latitude;
      const longitude =
        info && info.location && info.location.longitude
          ? parseFloat(info.location.longitude)
          : DEFAULT.longitude;
      this.setState({
        region: {
          ...this.state.region,
          latitude,
          longitude,
        },
        local: {
          latitude,
          longitude,
        },
      });
    }
  }

  _onComik = () => {
    const {region, local} = this.state;
    this.setState({
      region: {
        ...region,
        ...local,
      },
    });
  };

  _onZoomIn = () => {
    const {region} = this.state;
    this.setState({
      region: {
        ...region,
        latitudeDelta: region.latitudeDelta / RATIO,
        longitudeDelta: region.longitudeDelta / RATIO,
      },
    });
  };

  _onZoomOut = () => {
    const {region} = this.state;
    this.setState({
      region: {
        ...region,
        latitudeDelta: region.latitudeDelta * RATIO,
        longitudeDelta: region.longitudeDelta * RATIO,
      },
    });
  };

  onRegionChange = region => {
    this.state.region = region;
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>BẢN ĐỒ</Text>
        </View>

        <MapView
          // provider={PROVIDER_GOOGLE}
          style={styles.map}
          initialRegion={DEFAULT}
          region={this.state.region}
          onRegionChange={this.onRegionChange}>
          <Marker
            isPreselected={true}
            coordinate={this.state.local}
            title={'Vật liệu chống thấm Conmik'}
          />
        </MapView>

        <View style={styles.vZoom}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this._onZoomIn}
            style={styles.button}>
            <Text style={styles.text}>+</Text>
          </TouchableOpacity>

          <View style={styles.dash} />

          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this._onZoomOut}
            style={styles.button}>
            <Text style={styles.text}>-</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.vLocal}>
          <TouchableOpacity activeOpacity={0.8} onPress={this._onComik}>
            <FastImage
              source={Icons.logo}
              style={styles.logo}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default MapComp;

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    marginHorizontal: 10,
    height: HEIGHT_MAP,
    width: WIDTH_MAP,
    backgroundColor: Colors.silverCONMIK,
  },
  header: {
    backgroundColor: '#eb1e25',
    height: 45,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  title: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    top: 45,
  },
  vZoom: {
    position: 'absolute',
    right: 10,
    bottom: 10,
    width: 40,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  button: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    color: 'grey',
    fontWeight: 'bold',
  },
  dash: {
    height: 0.5,
    width: 30,
    marginLeft: 5,
    backgroundColor: 'grey',
  },
  vLocal: {
    position: 'absolute',
    left: 5,
    bottom: Platform.OS == 'android' ? 25 : 20,
    width: 40,
  },
  logo: {
    width: 40,
    height: 40,
  },
});
