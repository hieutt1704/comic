import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import Swiper from 'react-native-swiper';
import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Views from '../../views/ViewManagers';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;

export default class HomeNews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {news} = this.props;
    if (
      JSON.stringify(news) != JSON.stringify(nextProps.news) ||
      this.state.index != nextState.index
    ) {
      return true;
    }
    return false;
  }

  _onIndexChanged = index => {
    this.setState({index});
  };

  _onNext = () => {
    const {news} = this.props;
    const {index} = this.state;
    if (index != news.length - 1 && this.SwiperNew) {
      this.SwiperNew.scrollBy(1, true);
    }
  };

  _onPrev = () => {
    const {index} = this.state;
    if (index != 0 && this.SwiperNew) {
      this.SwiperNew.scrollBy(-1, true);
    }
  };

  render() {
    const {news, onPress} = this.props;
    const {index} = this.state;
    const prevColor = index == 0 ? Colors.greyCONMIK : Colors.yellowSERVICE;
    const nextColor =
      index == news.length - 1 ? Colors.greyCONMIK : Colors.yellowSERVICE;
    const prevOpacity = index == 0 ? 1 : 0.8;
    const nextOpacity = index == news.length - 1 ? 1 : 0.8;
    return (
      <View style={{paddingHorizontal: 10}}>
        <View style={styles.dash} />

        <Text style={styles.title}>TIN TỨC</Text>

        <Swiper
          ref={ref => (this.SwiperNew = ref)}
          autoplay={false}
          showsPagination={false}
          loop={false}
          height={WIDTH_COVER / 2 + 120}
          width={WIDTH_COVER}
          loadMinimal={true}
          onIndexChanged={this._onIndexChanged}>
          {news.map((item, index) => (
            <Views.ItemNew
              key={index}
              item={item}
              onPress={onPress}
              isHome={true}
            />
          ))}
        </Swiper>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginTop: 10,
          }}>
          <TouchableOpacity
            activeOpacity={prevOpacity}
            onPress={this._onPrev}
            style={{
              width: 30,
              height: 30,
              backgroundColor: prevColor,
              marginRight: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>
              {'<'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={nextOpacity}
            onPress={this._onNext}
            style={{
              width: 30,
              height: 30,
              backgroundColor: nextColor,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>
              {'>'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 18,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  dash: {
    height: 7.5,
    backgroundColor: Colors.silverCONMIK,
    width: WIDTH_COVER,
    marginTop: 25,
  },
  titleNew: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
  },
  content: {
    fontSize: 14,
    color: '#606060',
    marginTop: 5,
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_regular,
  },
});
