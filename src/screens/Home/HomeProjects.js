import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Swiper from 'react-native-swiper';
import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Apis from '../../networking/Apis';
import FastImage from 'react-native-fast-image';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;

export default class HomeProject extends Component {
  _index = 0;

  shouldComponentUpdate(nextProps, nextState) {
    const {projects} = this.props;
    if (projects.length != nextProps.projects) {
      return false;
    }
    return true;
  }

  _onIndexChanged = index => {
    const {projects} = this.props;
    const length = projects.length - 1;
    //vi tri hien tai _index dang o cuối mảng index về đầu mảng next 1
    // vi tri hien tai _index dang ở dầu mảng index tới cuối mảng prev -1
    const move =
      index == 0 && this._index == length
        ? 1
        : index == length && this._index == 0
        ? -1
        : index - this._index;
    this._index = index;
    if (this.SwiperNameProject) {
      this.SwiperNameProject.scrollBy(move, true);
    }
  };

  render() {
    const {projects, onPress} = this.props;
    return (
      <View>
        <View style={{paddingHorizontal: 10}}>
          <View style={styles.dash} />

          <Text style={styles.title}>DỰ ÁN</Text>
        </View>
        <View style={{marginBottom: 50}}>
          <Swiper
            autoplay={true}
            showsButtons={true}
            showsPagination={false}
            height={WIDTH_COVER / 2}
            width={width}
            loadMinimal={true}
            onIndexChanged={this._onIndexChanged}>
            {projects.map((item, index) => (
              <ItemProject
                key={index}
                index={index}
                item={item}
                onPress={onPress}
              />
            ))}
          </Swiper>

          <Swiper
            ref={ref => (this.SwiperNameProject = ref)}
            showsPagination={false}
            scrollEnabled={false}
            height={70}
            width={width}
            pageMargin={-100}
            loadMinimal={true}
            containerStyle={{
              width: width - 100,
              position: 'absolute',
              top: WIDTH_COVER / 2 - 20,
            }}>
            {projects.map((item, index) => (
              <TouchableOpacity
                style={styles.project}
                key={index}
                onPress={() => onPress(item)}
                activeOpacity={0.7}>
                <View
                  style={{
                    width: 5,
                    backgroundColor: Colors.redMDC,
                    height: 30,
                    marginTop: 7.5,
                  }}
                />

                <View style={{flex: 1, padding: 10}}>
                  <Text style={styles.nameProject} numberOfLines={2}>
                    {item.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </Swiper>
        </View>
      </View>
    );
  }
}

const ItemProject = ({item, onPress}) => {
  const image = item.image
    ? item.image.indexOf('http') == 0
      ? item.image
      : Apis.domain + item.image
    : 'http://index';
  return (
    <TouchableOpacity
      onPress={() => onPress({...item, image})}
      activeOpacity={0.7}>
      <FastImage
        source={{uri: image}}
        style={{
          height: WIDTH_COVER / 2,
          width,
          backgroundColor: Colors.silverCONMIK,
        }}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  project: {
    flexDirection: 'row',
    width: width - 120,
    marginLeft: 60,
    borderRadius: 5,
    borderWidth: 0.3,
    borderColor: 'grey',
    height: 70,
    backgroundColor: 'white',
  },
  nameProject: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  title: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 18,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  dash: {
    height: 7.5,
    backgroundColor: Colors.silverCONMIK,
    width: WIDTH_COVER,
    marginTop: 25,
  },
});
