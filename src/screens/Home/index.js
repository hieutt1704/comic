import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  RefreshControl,
  Linking,
} from 'react-native';

import * as Utils from '../../Utils';
import {connect} from 'react-redux';
import * as Colors from '../../config/Colors';
import * as Apis from '../../networking/Apis';
// import OneSignal from "react-native-onesignal"
import FastImage from 'react-native-fast-image';
import * as Views from '../../views/ViewManagers';
import * as Icons from '../../config/IconManagers';
import * as Contants from '../../config/Constants';
import * as Storage from '../../modules/asyncStorge';
import SplashScreen from "react-native-splash-screen"
import * as Routers from '../../navigations/Navigations';
import {changeIsLogin} from '../../redux/actions/loginAction';
import ProgressLoading from '../../views/common/ProgressLoading';


import HomeNews from './HomeNews';
import HomeProject from './HomeProjects';
import HomeProduct from './HomeProduct';
import HomeServices from './HomeServices';

const {width} = Dimensions.get('window');
const HEIGHT_IMAGE = 680;
const WIDTH_IMAGE = 680;
const WIDTH_COVER = width - 20;
const HEIGHT_COVER = (HEIGHT_IMAGE * WIDTH_COVER) / WIDTH_IMAGE;

class Home extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      is_refresh: false,
      logo: '',
      row_s: 2,
      row_p: 2,
      products: [],
      services: [],
      projects: [],
      news: [],
      isLoading: true,
    };
    this.onReceived = this.onReceived.bind(this);
    this.onOpened = this.onOpened.bind(this);
  }

  async componentDidMount() {
    if (this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    const res = await this._getData();
    this.setState({
      isLoading: false
    })
    if (res.status != 1) {
      const data_home = await Storage.getStorageData(Storage.home);
      if (data_home) {
        this._setData(JSON.parse(data_home));
      }
    }
    const user = await Storage.getStorageData(Storage.is_login);
    if (user) {
      this.props.changeIsLogin(JSON.parse(user));
    }
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
    SplashScreen.hide()

    // Linking
    //     .getInitialURL()
    //     .then(url => this.handleOpenURL({ url }))
    //     .catch(console.error);

    // Linking.addEventListener('url', this.handleOpenURL)

    // OneSignal.addEventListener('received', this.onReceived);
    // OneSignal.addEventListener('opened', this.onOpened);
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  handleOpenURL = event => {
    // console.log('Deeplink: ', event)
    if (event && event.url) {
      Utils.alert(event.url);
    }
  };

  onReceived(notification) {
    // const data = notification.payload.additionalData
    // if (data.noti) {
    //     Routers.goAbout({ item: data.noti })
    // }
  }

  onOpened(openResult) {
    const data = openResult.notification.payload.additionalData;
    if (data.noti) {
      Routers.goAbout({item: data.noti});
      setTimeout(() => {
        Routers.showMenu(false);
      }, 50);
    }
  }

  _onMenu = () => {
    Routers.showMenu(true);
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  _getData = showFail => {
    const {token, username} = this.props.loginState;
    const params = `?tk=${username}&token=${token}`;
    return Apis.getHome(params).then(res => {
      console.log('home', params, res);
      if (res.status == 1 && res.data) {
        this._setData(res.data);
        Storage.setStorageData(Storage.home, JSON.stringify(res.data));
      } else if (showFail) {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _setData = data_home => {
    const {logo, products, services, project, news, row_p, row_s} = data_home;
    if (this._mounted) {
      this.setState({
        logo,
        products,
        services,
        projects: project,
        news,
        row_p,
        row_s,
      });
    }
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData(true);
    if (this._mounted) {
      setTimeout(() => {
        this.setState({is_refresh: false});
      }, 5);
    }
  };

  _onItemDetail = (item, typeObject) => {
    Routers.goDetail({item, typeObject});
  };

  _onProject = item => {
    if (this.ImageModal) {
      this.ImageModal.showModal([item]);
    }
  };

  _onNew = item => {
    Routers.goNewDetail({item});
  };

  render() {
    const {logo, products, services, projects, news, is_refresh, row_p, row_s} =
      this.state;
    const cover = logo
      ? logo.indexOf('http') == 0
        ? {uri: logo}
        : {uri: Apis.domain + logo}
      : Icons.cover_home;
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title="Trang chủ"
        logo={Icons.logo}
        rightIcon={Icons.search}
        leftIcon={Icons.menu}
        onLeftPress={this._onMenu}
        onRightPress={this._goSearch}
        bottomModal={<Views.ImageModal ref={ref => (this.ImageModal = ref)} />}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          <View
            style={{
              flexDirection: 'row',
              width: WIDTH_COVER,
              margin: 10,
              height: 5,
            }}>
            <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
          </View>

          <FastImage source={cover} style={styles.image} />

          {products && products.length > 0 ? (
            <HomeProduct
              row_p={row_p || 2}
              products={products}
              onPress={item =>
                this._onItemDetail(item, Contants.TypeObject.PRODUCT)
              }
            />
          ) : null}

          {services && services.length > 0 ? (
            <HomeServices
              row_s={row_s || 2}
              services={services}
              onPress={item =>
                this._onItemDetail(item, Contants.TypeObject.SERVICE)
              }
            />
          ) : null}

          {projects && projects.length > 0 && !is_refresh ? (
            <HomeProject projects={projects} onPress={this._onProject} />
          ) : null}

          {news && news.length > 0 && !is_refresh ? (
            <HomeNews
              ref={ref => (this.HomeNews = ref)}
              news={news}
              onPress={this._onNew}
            />
          ) : null}
          <View style={{height: 10}} />
        </ScrollView>
        <ProgressLoading
          visible={this.state.isLoading}
          style={styles.center}
          backgroundColor="white"
        />
      </Views.BaseView>
    );
  }
}
const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps, {changeIsLogin})(Home);

const styles = StyleSheet.create({
  image: {
    width: WIDTH_COVER,
    height: HEIGHT_COVER,
    marginHorizontal: 10,
    backgroundColor: Colors.silverCONMIK,
  },
});
