import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  FlatList,
  ScrollView,
} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Views from '../../views/ViewManagers';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;

export default class HomeProduct extends React.PureComponent {
  _getData = () => {
    const {products, row_p} = this.props;
    const nums = parseInt(row_p) * 2;
    const length = Math.ceil(products.length / nums);
    let data = new Array(length).fill([]);
    return data.map((e, i) => {
      return products.filter(
        (product, index) => i * nums <= index && index + 1 <= (i + 1) * nums,
      );
    });
  };

  render() {
    const {onPress} = this.props;
    const data = this._getData();
    return (
      <View style={{paddingHorizontal: 10}}>
        <Text style={styles.title}>SẢN PHẨM NỔI BẬT</Text>

        <ScrollView horizontal={true}>
          {data.map((e, index) => (
            <FlatList
              key={index}
              data={e}
              numColumns={2}
              scrollEnabled={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index + ''}
              renderItem={({item, index}) => (
                <Views.ItemProduct
                  index={index}
                  item={item}
                  onPress={onPress}
                />
              )}
              ItemSeparatorComponent={() => (
                <View style={{width: 5, height: 5}} />
              )}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 18,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  dash: {
    height: 7.5,
    backgroundColor: Colors.silverCONMIK,
    width: WIDTH_COVER,
    marginTop: 25,
  },
});
