import React from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    FlatList,
    Text
} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Views from '../../views/ViewManagers';
import * as Icons from '../../config/IconManagers';

const { width } = Dimensions.get('window');
const WIDTH_COVER = width - 20;

const PRODUCTS = [
    {
        "id": "66",
        "title": "CONMIK BA 108",
        "url": "/mang-chong-tham/conmik-ba-108.html",
        "image": "",
        "posttime": "24/11/2023 07:13",
        "intro": "1. PHẠM VI SỬ DỤNG\r\n\r\n2. ƯU ĐIỂM\r\n\r\n3. TH&Ocirc;NG SỐ KỸ THUẬT\r\n\r\n4. HƯỚNG DẪN THI C&Ocirc;NG",
        "color": "#fbbc05"
    },
    {
        "id": "65",
        "title": "CONMIK LUXURY SA V",
        "url": "/mang-chong-tham/conmik-luxury-sa-v.html",
        "image": "/uploaded/mang%20chong%20tham%20goc%20hdpe.jpg",
        "posttime": "15/11/2023 06:56",
        "intro": "* PHẠM VI SỬ DỤNG:\r\n\r\n\tV&aacute;ch tường hầm\r\n\tT&agrave;u điện ngầm\r\n\tĐường hầm\r\n\tHang động\r\n\tS&agrave;n m&aacute;i\r\n\tC&aacute;c dự &aacute;n li&ecirc;n quan đến v&aacute;ch tường\r\n\r\n* ĐẶC T&Iacute;NH ƯU VIỆT:\r\n\r\n\r\n\tB&aacute;m d&iacute;nh chặt với mặt b&ecirc; t&ocirc;ng, bảo vệ b&ecirc; t&ocirc;ng\r\n\tThi c&ocirc;ng, ứng dụng dễ d&agrave;ng\r\n\tKh&oacute; biến dạng\r\n\tKh&aacute;ng thời tiết, kh&aacute;ng UV\r\n\tChống lại axit, kiềm, nấm mốc\r\n\tTh&acirc;n thiện với m&ocirc;i trường\r\n\r\n* TH&Ocirc;NG SỐ KỸ THUẬT:\r\n&nbsp;\r\n\r\n\t\r\n\t\t\r\n\t\t\tSTT\r\n\t\t\tMục kiểm tra\r\n\t\t\tTh&ocirc;ng số\r\n\t\t\r\n\t\t\r\n\t\t\t&nbsp;\r\n\t\t\t1\r\n\t\t\t\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;&nbsp;Độ bền&nbsp;...",
        "color": "#34a853"
    },
    {
        "id": "64",
        "title": "CONMIK SEAL",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-1-thanh-phan/conmik-seal.html",
        "image": "/uploaded/Seal.png",
        "posttime": "07/11/2023 06:14",
        "intro": "PHẠM VI SỬ DỤNG:\r\n\r\n- M&aacute;ng dẫn nước v&agrave; m&aacute;i bằng\r\n\r\n- Nh&agrave; m&aacute;y điện hạt nh&acirc;n, đường hầm, m&oacute;ng, ph&ograve;ng điều khiển\r\n\r\n- Đường t&agrave;u điện ngầm, bể bơi\r\n\r\n- Nh&agrave; m&aacute;y c&ocirc;ng nghiệp, m&oacute;ng c&aacute;c t&ograve;a nh&agrave; c&ocirc;ng sở, s&agrave;n để xe.\r\n\r\n- S&agrave;n v&agrave; tường tầng hầm\r\n\r\nƯU ĐIỂM:\r\n\r\n- Hiệu quả, dễ thi c&ocirc;ng, kh&ocirc;ng chứa Clo\r\n\r\n- Thẩm thấu v&agrave;o b&ecirc; t&ocirc;ng, v&aacute; hệ&nbsp;...",
        "color": "#fbbc05"
    },
    {
        "id": "63",
        "title": "CONMIK LUXURY SA",
        "url": "/mang-chong-tham/conmik-luxury-sa.html",
        "image": "/uploaded/z4882606210840_47c4a7f9947603bcd0695512c5dc23fb.jpg",
        "posttime": "06/11/2023 08:28",
        "intro": "Conmik Luxury SA l&agrave; m&agrave;ng HDPE li&ecirc;n kết ho&agrave;n to&agrave;n, được thi c&ocirc;ng trước bao gồm vật liệu chống thấm composite nhiều lớp cho hiệu suất chống thấm vượt trội, bao gồm một lớp m&agrave;ng PE mật độ cao, lớp polymer tự d&iacute;nh v&agrave; lớp bảo vệ độc đ&aacute;o. Lớp polymer tự d&iacute;nh sẽ phản ứng ho&aacute; học khi gặp b&ecirc;&nbsp;...",
        "color": "#fbbc05"
    },
    {
        "id": "50",
        "title": "Màng tự dính chuyên dụng (Conmik Lux BA9)",
        "url": "/mang-chong-tham-tu-dinh/mang-tu-dinh-chuyen-dung-conmik-bitustick-alu-12-mm.html",
        "image": "/uploaded/files/alu.jpg",
        "posttime": "26/09/2019 07:37",
        "intro": "Catalog Sản phẩm&nbsp;( tải file catalog  tại đ&acirc;y )\r\n&nbsp;PHẠM VI SỬ DỤNG :\r\n\r\n\r\n\tChống thấm s&agrave;n m&aacute;i.\r\n\tChống thấm v&ugrave;ng ch&oacute;p, khe m&aacute;i, ống lạnh.\r\n\tChống thấm ( dột ) m&aacute;i t&ocirc;n, nơi tiếp gi&aacute;p giữa 2 miếng t&ocirc;n chồng l&ecirc;n nhau, ốc v&iacute;t, ch&acirc;n tường, lỗ nhỏ, m&aacute;ng xối &hellip;\r\n\tChống n&oacute;ng , c&aacute;ch nhiệt m&aacute;i t&ocirc;n.\r\n\r\nM&agrave;ng tự d&iacute;nh chống thấm bảo vệ&nbsp;...",
        "color": "#2855a1"
    },
    {
        "id": "48",
        "title": "Màng chống thấm Conmik Bitumik mặt đá",
        "url": "/mang-chong-tham-kho-nong/mang-chong-tham-conmik-bitumik-mat-da.html",
        "image": "/uploaded/san-pham/3%20SP%20trong%201%20copy.jpg",
        "posttime": "13/12/2018 08:06",
        "intro": "Catalog Sản phẩm&nbsp;( tải&nbsp;file catalog&nbsp;tại đ&acirc;y&nbsp;)\r\n\r\n\r\n1. M&ocirc; tả\r\nConmik Bitumik l&agrave; m&agrave;ng chống thấm c&oacute; khả năng chịu nhiệt, chống lại tia tử ngoại, tia UV v&agrave; khả năng chống thấm cao, được sản xuất từ hỗn hợp gi&agrave;u bitum v&agrave; AtacticPolyproplene. B&ecirc;n trong m&agrave;ng được gia cố bằng lưới Polyester sản xuất theo phương ph&aacute;p Spunbond kh&ocirc;ng đan.\r\n&gt;&gt; xem th&ecirc;m :&nbsp;M&agrave;ng&nbsp;...",
        "color": "#4285f4"
    },
    {
        "id": "43",
        "title": "Conmik Contite WaterShield",
        "url": "/vat-lieu-chong-tham/conmik-contite-watershield.html",
        "image": "/uploaded/Conmik%20Contite%20WaterShield.jpg",
        "posttime": "05/11/2018 07:06",
        "intro": "Catalog Sản phẩm&nbsp;( tải&nbsp;file catalog&nbsp;tại đ&acirc;y&nbsp;)\r\nƯu điểm\r\n\r\n&nbsp;\r\n\r\n- C&oacute; khả năng kh&aacute;ng kiềm tuyệt vời.\r\n\r\n- Phạm vi sử dụng rộng r&atilde;i, d&ugrave;ng cho nhiều loại bề mặt trong x&acirc;y dựng.\r\n\r\n- Sản phẩm cũng rất hiệu quả cho c&aacute;c mặt nền bị ướt.\r\n\r\n- Độ th&acirc;m nhập s&acirc;u.\r\n\r\n- Ngăn sự thấm nước bề mặt v&agrave; giữ lại nguy&ecirc;n trạng h&igrave;nh dạng ban đầu.\r\n\r\n- &nbsp;Vật&nbsp;...",
        "color": "#4285f4"
    },
    {
        "id": "25",
        "title": "Conmik Flex 600",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-2-thanh-phan/flex-600.html",
        "image": "/uploaded/san-pham/conmikflex600.jpeg",
        "posttime": "24/09/2018 06:36",
        "intro": "Conmik Flex 600 l&agrave; m&agrave;ng chống thấm polymer hai th&agrave;nh phần gốc xi măng linh hoạt được đ&oacute;ng g&oacute;i th&agrave;nh bộ d&ugrave;ng ngay. Sản phẩm ph&ugrave; hợp với b&ecirc; t&ocirc;ng v&agrave; vữa để ngăn rỉ nước, tr&aacute;m v&agrave; v&aacute; c&aacute;c lỗ rỗng, vết nứt tr&ecirc;n bề mặt.\r\nConmik Flex 600 c&oacute; chứa xi măng Portland, phụ gia l&agrave;m dẻo, c&aacute;t hạt c&ugrave;ng&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "64",
        "title": "CONMIK SEAL",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-1-thanh-phan/conmik-seal.html",
        "image": "/uploaded/Seal.png",
        "posttime": "07/11/2023 06:14",
        "intro": "PHẠM VI SỬ DỤNG:\r\n\r\n- M&aacute;ng dẫn nước v&agrave; m&aacute;i bằng\r\n\r\n- Nh&agrave; m&aacute;y điện hạt nh&acirc;n, đường hầm, m&oacute;ng, ph&ograve;ng điều khiển\r\n\r\n- Đường t&agrave;u điện ngầm, bể bơi\r\n\r\n- Nh&agrave; m&aacute;y c&ocirc;ng nghiệp, m&oacute;ng c&aacute;c t&ograve;a nh&agrave; c&ocirc;ng sở, s&agrave;n để xe.\r\n\r\n- S&agrave;n v&agrave; tường tầng hầm\r\n\r\nƯU ĐIỂM:\r\n\r\n- Hiệu quả, dễ thi c&ocirc;ng, kh&ocirc;ng chứa Clo\r\n\r\n- Thẩm thấu v&agrave;o b&ecirc; t&ocirc;ng, v&aacute; hệ&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "25",
        "title": "Conmik Flex 600",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-2-thanh-phan/flex-600.html",
        "image": "/uploaded/san-pham/conmikflex600.jpeg",
        "posttime": "24/09/2018 06:36",
        "intro": "Conmik Flex 600 l&agrave; m&agrave;ng chống thấm polymer hai th&agrave;nh phần gốc xi măng linh hoạt được đ&oacute;ng g&oacute;i th&agrave;nh bộ d&ugrave;ng ngay. Sản phẩm ph&ugrave; hợp với b&ecirc; t&ocirc;ng v&agrave; vữa để ngăn rỉ nước, tr&aacute;m v&agrave; v&aacute; c&aacute;c lỗ rỗng, vết nứt tr&ecirc;n bề mặt.\r\nConmik Flex 600 c&oacute; chứa xi măng Portland, phụ gia l&agrave;m dẻo, c&aacute;t hạt c&ugrave;ng&nbsp;...",
        "color": "#34a853"
    },
    {
        "id": "24",
        "title": "Conmik Flex",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-2-thanh-phan/flex.html",
        "image": "/uploaded/san-pham/conmik%20flex.png",
        "posttime": "24/09/2018 06:27",
        "intro": "Conmik Flex l&agrave; m&agrave;ng chống thấm polymer hai th&agrave;nh phần gốc xi măng linh hoạt được đ&oacute;ng g&oacute;i th&agrave;nh bộ d&ugrave;ng ngay. Sản phẩm ph&ugrave; hợp với b&ecirc; t&ocirc;ng v&agrave; vữa để ngăn rỉ nước, tr&aacute;m v&agrave; v&aacute; c&aacute;c lỗ rỗng, vết nứt tr&ecirc;n bề mặt.\r\nConmik Flex c&oacute; chứa xi măng Portland, phụ gia l&agrave;m dẻo, c&aacute;t hạt c&ugrave;ng với latex&nbsp;...",
        "color": "#34a853"
    },
    {
        "id": "23",
        "title": "Conmik Seal 100",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-2-thanh-phan/seal-100.html",
        "image": "/uploaded/san-pham/conmik seal 100.png",
        "posttime": "24/09/2018 06:17",
        "intro": "Conmik Seal 100&nbsp;l&agrave; m&agrave;ng chống thấm hai th&agrave;nh phần gồm th&agrave;nh phần chất lỏng chưa polyme v&agrave; th&agrave;nh phần bột chứa xi măng, chất l&agrave;m đầy v&agrave; cốt liệu. Khi trộn hai th&agrave;nh phần tạo th&agrave;nh vữa c&oacute; thể lăn hoặc chải, tạo th&agrave;nh lớp m&agrave;ng chống thấm linh hoạt, kh&aacute;ng &aacute;p lực thủy tĩnh cao, độ b&aacute;m d&iacute;nh tốt với&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "22",
        "title": "Conmik Seal 200",
        "url": "/vat-lieu-chong-tham-goc-xi-mang-1-thanh-phan/seal-200.html",
        "image": "/uploaded/san-pham/conmikseal200.jpeg",
        "posttime": "24/09/2018 06:07",
        "intro": "M&Ocirc; TẢ:\r\nKhi xuất hiện hơi ẩm, hoạt chất trong CONMIK SEAL 200 thẩm thấu v&agrave;o b&ecirc; t&ocirc;ng v&agrave; phản ứng với v&ocirc;i tự do để sản xuất tinh thể kh&ocirc;ng h&ograve;a tan. Sự ph&aacute;t triển của tinh thể l&agrave;m giảm t&igrave;nh trạng lỗ rộng bằng c&aacute;ch kh&oacute;a c&aacute;c mao mạch v&agrave; tr&aacute;m v&agrave;o c&aacute;c vết nứt mảnh (rộng đến 0.5mm) g&acirc;y&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "61",
        "title": "CONMIK PU 80H",
        "url": "/goc-polyurethane/conmik-pu-80h.html",
        "image": "",
        "posttime": "16/03/2023 09:49",
        "intro": "CONMIK PU 80H l&agrave; hỗn hợp c&oacute; h&agrave;m lượng chất rắn cao, polyurethane hai th&agrave;nh phần với đặc t&iacute;nh kh&aacute;ng&nbsp; m&agrave;i m&ograve;n, kh&aacute;ng h&oacute;a chất v&agrave; c&oacute; độ b&aacute;m d&iacute;nh tuyệt vời.\r\nĐẶC T&Iacute;NH:\r\n- Chất rắn cao/VOC thấp\r\n- Khả năng phục hồi để thoải m&aacute;i v&agrave; ngăn ngừa chấn thương\r\n- Bề mặt liền mạch, kh&ocirc;ng xốp v&agrave; mịn, dễ d&agrave;ng bảo tr&igrave;,&nbsp;...",
        "color": "#2855a1"
    },
    {
        "id": "56",
        "title": "CONMIK 919 R",
        "url": "/hoa-chat-goc-polyurea/conmik-919-r.html",
        "image": "/uploaded/san-pham/1.png",
        "posttime": "28/11/2022 02:48",
        "intro": "CONMIK 919 R&nbsp;l&agrave; vật liệu chống thấm gốc polyurea thi c&ocirc;ng lạnh, 2 th&agrave;nh phần , kh&ocirc; nhanh. N&oacute; tạo ra một lớp m&agrave;ng đ&agrave;n hồi v&agrave; bền tr&ecirc;n c&aacute;c bề mặt. N&ecirc;n sử dụng lớp Conmik top coat b&ecirc;n tr&ecirc;n để d&ugrave;ng ở những khu vực tiếp x&uacute;c trực tiếp với tia cực t&iacute;m v&agrave; người đi bộ qua lại.\r\n\r\n1.&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "55",
        "title": "CONMIK EPOXY 618",
        "url": "/hoa-chat-goc-epoxy/conmik-epoxy-618.html",
        "image": "/uploaded/san-pham/610478c4e24722197b56.jpg",
        "posttime": "09/06/2022 08:38",
        "intro": "Conmik Epoxy 618 l&agrave; lớp phủ epoxy Hi-Build hai th&agrave;nh phần, 100% chất rắn, được sử dụng cho c&aacute;c khu vực bể nước sạch, bể chứa, giao th&ocirc;ng xe n&acirc;ng ch&acirc;n cao v&agrave; xe n&acirc;ng hạng nhẹ cần chống m&agrave;i m&ograve;n, chống ăn m&ograve;n nhẹ v&agrave; l&agrave; một lớp phủ chống thấm trang tr&iacute; cho tường, s&agrave;n,....\r\nTh&agrave;nh phần ch&iacute;nh\r\n\r\n\r\n\tHạt nhựa: Nhựa&nbsp;...",
        "color": "#4285f4"
    },
    {
        "id": "54",
        "title": "CONMIK 808",
        "url": "/goc-polyurethane/conmik-808.html",
        "image": "/uploaded/san-pham/conmik%20808.jpg",
        "posttime": "08/06/2022 04:17",
        "intro": "Conmik 808 l&agrave; m&agrave;ng chống thấm gốc Polyurethane (PU) đ&agrave;n hồi liền mạch kh&ocirc;ng mối nối thi c&ocirc;ng dạng lỏng 1 th&agrave;nh phần.\r\nConmik 808 được thiết kế để thi c&ocirc;ng bằng ru l&ocirc;, chổi hoặc m&aacute;y phun l&ecirc;n bể mặt ngang v&agrave; một số bề mặt thẳng đứng, bảo dưỡng nhờ phản ứng tương t&aacute;c với bề mặt v&agrave; độ ẩm&nbsp;...",
        "color": "#2855a1"
    },
    {
        "id": "52",
        "title": "Conmik Super Waterproof ",
        "url": "/goc-polyurethane/conmik-super-waterproof.html",
        "image": "/uploaded/san-pham/Conmik%20super%20waterproof.jpg",
        "posttime": "09/11/2020 03:32",
        "intro": "Đặc biệt, Conmik Super Waterproof c&oacute; khả năng tự liền vết r&aacute;ch. C&aacute;c đặc t&iacute;nh cơ học tuyệt vời: sức căng cao, chống r&aacute;ch v&agrave; m&ograve;n. Conmik Super Waterproof c&oacute; khả năng kh&aacute;ng h&oacute;a chất tuyệt vời v&agrave; được d&ugrave;ng cho những khu vực c&oacute; mối nối nứt cao. Hơn nữa, kh&ocirc;ng bị ảnh hưởng bởi sự truyền hơi nước. M&agrave;ng&nbsp;...",
        "color": "#ff903f"
    },
    {
        "id": "51",
        "title": "Conmik Super Primer",
        "url": "/goc-polyurethane/conmik-super-primer.html",
        "image": "/uploaded/san-pham/primer.png",
        "posttime": "09/11/2020 03:17",
        "intro": "N&oacute; tạo bề mặt đ&oacute;ng rắn nhanh v&agrave; kh&ocirc;ng c&oacute; chất độc hại, cũng như kh&ocirc;ng chứa dung m&ocirc;i (kh&ocirc;ng VOC / C&aacute;c th&agrave;nh phần hữu cơ dễ bay hơi)\r\nPHẠM VI SỬ DỤNG :\r\n\r\n\t\r\n\t\t\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t\tD&ugrave;ng l&agrave;m lớp l&oacute;t tr&ecirc;n bề mặt kim loại, kim loại mạ kẽm, nh&ocirc;m, đ&aacute; hoa.\r\n\t\t\t\tĐược sử dụng như lớp l&oacute;t tr&ecirc;n c&aacute;c bề mặt tr&aacute;t v&agrave; phủ.\r\n\t\t\t\tC&oacute;&nbsp;...",
        "color": "#4285f4"
    },
    {
        "id": "45",
        "title": "Conmik Polyurea P",
        "url": "/conmik-pu-po-2/conmik-pu-polyurea-p.html",
        "image": "/uploaded/Conmik%20Polyurea%20P.jpg",
        "posttime": "22/11/2018 07:33",
        "intro": "Catalog Sản phẩm&nbsp;( tải&nbsp;file catalog&nbsp;tại đ&acirc;y&nbsp;)\r\nĐặc điểm:\r\n- Kh&ocirc; cực nhanh c&oacute; thể &aacute;p dụng tr&ecirc;n bề mặt nghi&ecirc;ng hoặc thẳng đứng m&agrave; kh&ocirc;ng bị chảy v&agrave; tiết kiệm thời gian.\r\n- T&iacute;nh chất vật l&yacute; tuyệt vời như khả năng kh&aacute;ng t&aacute;c động b&ecirc;n ngo&agrave;i, kh&aacute;ng m&agrave;i m&ograve;n , kh&aacute;ng lạnh, kh&aacute;ng nhiệt, khả năng chịu nước v&agrave; độ bền cao.\r\n- N&oacute;&nbsp;...",
        "color": "#4285f4"
    },
    {
        "id": "44",
        "title": "Conmik Polyurea 6000",
        "url": "/conmik-pu-polyurea-6000/conmik-pu-polyurea-6000.html",
        "image": "/uploaded/Conmik%20Polyurea%206000.jpg",
        "posttime": "22/11/2018 07:10",
        "intro": "Catalog Sản phẩm&nbsp;( tải&nbsp;file catalog&nbsp;tại đ&acirc;y&nbsp;)\r\nN&oacute; cung cấp một m&agrave;ng chống thấm nguy&ecirc;n khối đ&agrave;n hồi với khả năng chống thấm nước tốt, chống h&oacute;a chất, chống m&agrave;i m&ograve;n v&agrave; m&agrave;ng chống ch&aacute;y cực tốt.\r\n\r\n\r\n\r\nBạn c&oacute; thể muốn đọc :&nbsp;\r\n-&nbsp;&nbsp;L&agrave;m thế n&agrave;o để chống thấm bể bơi ?&nbsp;\r\n-&nbsp;&nbsp;Polyurea vật liệu vạn năng&nbsp;\r\n-&nbsp;&nbsp;Si&ecirc;u vật liệu chống thấm Polyurea&nbsp;\r\n\r\nƯu điểm:\r\n- 100% chất rắn&nbsp;...",
        "color": "#4285f4"
    }
]

const SERVICES = [
    {
        "id": "11",
        "title": "Chống thấm cổ ống xuyên sàn",
        "url": "/thi-cong-chong-tham/chong-tham-co-ong-xuyen-san.html",
        "image": "/uploaded/Dichvu/ch%E1%BB%91ng-th%E1%BA%A5m-c%E1%BB%95-%E1%BB%91ng-xuy%C3%AAn-s%C3%A0n-2.jpg",
        "posttime": "07/06/2019 13:19",
        "intro": "Cổ ống xuy&ecirc;n s&agrave;n l&agrave; c&aacute;ch thiết kế phổ biến cho hạ tầng c&ocirc;ng tr&igrave;nh phụ như toilet, nh&agrave; tắm. Ở thời điểm hiện tại, đ&acirc;y được l&agrave; phương ph&aacute;p được đ&aacute;nh gi&aacute; cao nhất. V&agrave; tất nhi&ecirc;n, đối với hoạt động thi c&ocirc;ng tổng thể, cổ ống xuy&ecirc;n s&agrave;n chiếm vai tr&ograve; kh&ocirc;ng hề đơn giản.",
        "color": "#2855a1"
    },
    {
        "id": "10",
        "title": "Chống thấm ngược",
        "url": "/thi-cong-chong-tham/chong-tham-nguoc.html",
        "image": "/uploaded/Dichvu/Flood-infusion-waterproofing-problem.jpg",
        "posttime": "07/06/2019 13:11",
        "intro": "Chống thấm ngược&nbsp;c&ugrave;ng với chống thấm tường, chống thấm nh&agrave; vệ sinh, chống thấm cầu thang hiện đang l&agrave; c&aacute;c phương ph&aacute;p phổ biến tr&ecirc;n thị trường gi&uacute;p cho kh&ocirc;ng gian sống th&ecirc;m sạch sẽ v&agrave; tho&aacute;ng đ&atilde;ng rất nhiều.",
        "color": "#4285f4"
    },
    {
        "id": "9",
        "title": "Chống thấm hố thang máy",
        "url": "/thi-cong-chong-tham/chong-tham-ho-thang-may.html",
        "image": "/uploaded/Dichvu/chong-tham-ho-thang-may-uy-tin.jpg",
        "posttime": "07/06/2019 13:04",
        "intro": "Chống thấm hố p&iacute;t thang m&aacute;y cho gia đ&igrave;nh, khu chung cư hay c&aacute;c khu nh&agrave; ở, văn ph&ograve;ng, kh&aacute;ch sạn cao tầng l&agrave; một việc l&agrave;m cần thiết v&igrave; hố thang m&aacute;y thường được đặt ở vị tr&iacute; thấp nhất t&ograve;a nh&agrave; như tầng hầm n&ecirc;n dễ bị thấm do c&aacute;c nguồn nước ngầm.",
        "color": "#34a853"
    },
    {
        "id": "8",
        "title": "Thi công chống thấm tầng hầm",
        "url": "/thi-cong-chong-tham-tran-nha/thi-cong-chong-tham-tang-ham.html",
        "image": "/uploaded/Dichvu/tang%20ham.jpg",
        "posttime": "24/04/2019 07:07",
        "intro": "Chống thấm tầng hầm&nbsp;bản chất l&agrave; chống nước từ dưới nền l&ecirc;n tr&ecirc;n v&agrave; ngầm từ ngo&agrave;i tường v&agrave;o trong. Việc chống thấm l&agrave; từ mọi hướng trong tầng hầm.&nbsp;",
        "color": "#fbbc05"
    },
    {
        "id": "7",
        "title": "Thi công chống thấm nhà vệ sinh",
        "url": "/thi-cong-chong-tham-nha-ve-sinh/thi-cong-chong-tham-nha-ve-sinh.html",
        "image": "/uploaded/Dichvu/Chong%20tham%20nha%20ve%20sinh%20bang%20Conmik.jpg",
        "posttime": "24/04/2019 07:06",
        "intro": "Đối với bất kỳ hạng mục c&ocirc;ng tr&igrave;nh n&agrave;o c&oacute; thi c&ocirc;ng chống thấm nh&agrave; vệ sinh. Th&igrave; đ&acirc;y lu&ocirc;n l&agrave; một trong những khu vực được ưu ti&ecirc;n xử l&yacute; chống thấm đầu ti&ecirc;n.",
        "color": "#2855a1"
    },
    {
        "id": "6",
        "title": "Thi công chống thấm bể nước",
        "url": "/thi-cong-chong-tham-be-nuoc/thi-cong-chong-tham-be-nuoc.html",
        "image": "/uploaded/Dichvu/17bf951528add5f38cbc.jpg",
        "posttime": "24/04/2019 07:04",
        "intro": "Bể nước sinh hoạt, bể nước ngầm hay bể nước n&oacute;i chung l&agrave; một trong những hạng mục thiết yếu trong cuộc sống sinh hoạt h&agrave;ng ng&agrave;y. Đặc th&ugrave; của bể nước đ&oacute; l&agrave; lu&ocirc;n chứa nước n&ecirc;n việc chống thấm cần phải được xử l&yacute; triệt để.",
        "color": "#2855a1"
    }
]

export default class FakeScreen extends React.PureComponent {
    render() {
        const { isProduct } = this.props
        return (
            <Views.BaseView
                ref={ref => (this.BaseView = ref)}
                title={isProduct ? "Sản phẩm" : "Dịch vụ"}
                logo={Icons.logo}
                rightIcon={Icons.search}
                leftIcon={Icons.menu}
                onLeftPress={() => {}}
                onRightPress={() => {}}
                bottomModal={<Views.ImageModal ref={ref => (this.ImageModal = ref)} />}>
                <FlatList
                    data={isProduct ? PRODUCTS : SERVICES}
                    numColumns={2}
                    contentContainerStyle={{ alignItems: 'center' }}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index + ''}
                    renderItem={({ item, index }) => (
                        <Views.ItemProduct
                            index={index}
                            item={item}
                            onPress={() => { }}
                        />
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={{ width: 5, height: 5 }} />
                    )}
                />
            </Views.BaseView>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        marginTop: 20,
        marginBottom: 10,
        fontSize: 22,
        color: 'black',
        fontFamily: Fonts.utm_avo,
        textAlign: 'center',

    },
    dash: {
        height: 7.5,
        backgroundColor: Colors.silverCONMIK,
        width: WIDTH_COVER,
        marginTop: 25,
    },
});
