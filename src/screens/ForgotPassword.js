import React, {Component} from 'react';
import {Text, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';

import * as Utils from '../Utils';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import {fontSize} from '../modules/fontSize';
import * as Views from '../views/ViewManagers';
import * as Routers from '../navigations/Navigations';
import LinearGradient from 'react-native-linear-gradient';

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
    };
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _getPassword = () => {
    const {phone} = this.state;
    if (!phone) {
      Utils.showToast('Chưa nhập số điện thoại');
    } else {
      const params = `?sdt=${phone}&rs=1`;
      if (this.ProgressLoading) {
        this.ProgressLoading.show();
      }
      Apis.forgot(params).then(res => {
        if (this.ProgressLoading) {
          this.ProgressLoading.hide();
        }
        if (res.status == 1) {
          Utils.showToast(
            'Chúng tôi đã gửi mật khẩu về số điện thoại của bạn. Vui lòng kiểm tra tin nhắn',
          );
          this._goBack();
        } else {
          Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      });
    }
  };

  render() {
    return (
      <Views.BaseView
        style={styles.container}
        title="Quên mật khẩu"
        onLeftPress={this._goBack}
        footerModal={
          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor="#00000030"
          />
        }>
        <ScrollView style={{flex: 1}}>
          <Text style={styles.title}>
            Vui lòng nhập số điện thoại mà bạn đã đăng ký với hệ thống, chúng
            tôi sẽ gửi mật khẩu qua SMS tới số điện thoại của bạn.
          </Text>

          <Views.InputView
            value={this.state.phone}
            styleForm={styles.form}
            styleContainer={styles.inputView}
            placeholder={'Số điện thoại'}
            style={styles.textinput}
            formText={true}
            returnKeyType={'go'}
            autoCapitalize={'none'}
            onSubmitEditing={this._getPassword}
            keyboardType="phone-pad"
            onChangeText={phone => this.setState({phone})}
          />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.button}
            onPress={this._getPassword}>
            <LinearGradient
              colors={['#a11e23', '#e91e24']}
              style={styles.gradient}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}>
              <Text style={[styles.text, {fontSize: fontSize + 2}]}>
                {'Lấy mật khẩu'.toUpperCase()}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </ScrollView>
      </Views.BaseView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 18,
    color: 'black',
    marginHorizontal: 15,
    marginTop: 15,
    fontFamily: Fonts.myriadpro_regular,
    textAlign: 'justify',
  },
  inputView: {
    marginHorizontal: 25,
    marginTop: 25,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 0.3,
    padding: 10,
    height: 45,
  },
  textinput: {
    flex: 1,
    color: 'black',
    fontFamily: Fonts.myriadpro_regular,
    fontSize: 18,
  },
  button: {
    marginHorizontal: 70,
    marginTop: 30,
    marginBottom: 10,
    height: 50,
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 25,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
});
