import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  View,
  Text,
  TouchableOpacity,
  Animated,
  RefreshControl,
} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Colors from '../config/Colors';
import * as Views from '../views/ViewManagers';
import * as Contants from '../config/Constants';
import FastImage from 'react-native-fast-image';
import * as Icons from '../config/IconManagers';
import * as Routers from '../navigations/Navigations';
import AutoHeightWebView from 'react-native-autoheight-webview';

const Entities = require('html-entities').AllHtmlEntities;

const SIZE = 60;
const {height, width} = Dimensions.get('window');
const HEIGHT_IMAGE = (2 * width) / 3;
const WIDTH_IMAGE_SERVICE = (3 * width) / 5;
const WIDTH_WEBVIEW = width - 20;

class Detail extends Component {
  _mounted = true;
  _scrollY = new Animated.Value(0);
  constructor(props) {
    super(props);
    const {item} = this.props;
    this.state = {
      item,
      is_refresh: false,
      isShowFile: false,
    };
  }

  componentDidMount = async () => {
    if (this.ProgressLoading) {
      this.ProgressLoading.show();
    }
    await this._getData();
  };

  componentWillUnmount() {
    this._mounted = false;
  }

  _getData = () => {
    const {item, loginState, typeObject} = this.props;
    const {token, username} = loginState;
    const type =
      typeObject == Contants.TypeObject.PRODUCT ? 'product' : 'service';
    const params = `?tk=${username}&token=${token}&id=${item.id}&data_type=${type}`;
    return Apis.getDetail(params).then(res => {
      console.log('Get detail: ', res, params);
      if (res.status == 1 && res.data) {
        if (this._mounted) {
          this.setState({
            item: {...item, ...res.data, color: res.data.color || item.color},
            is_error: false,
          });
        }
        const html = res.data.content ? res.data.content : '';
        if (!html) {
          this.hideLoading();
        }
      } else {
        Utils.showToast(res.msg || 'Không cập nhật được bài viết chi tiết');
        this.hideLoading();
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  hideLoading = () => {
    if (this.ProgressLoading) {
      this.ProgressLoading.hide();
    }
    this.setState({isShowFile: true});
  };

  _showImage = url => {
    if (this.ImageModal) {
      this.ImageModal.showModal([{url}]);
    }
  };

  _signup = () => {
    if (this.SignupModal) {
      this.SignupModal.showModal();
    }
  };

  handleWebViewNavigationStateChange = newNavState => {
    const {url} = newNavState;
    if (!url) {
      return;
    }
    // handle certain doctypes
    if (url.indexOf('http') == 0) {
      // this.webview.goForward();
      // this.webview.stopLoading();
      // open a modal with the PDF viewer
    }
  };

  handleWebViewRequest = request => {
    const {url} = request;
    if (url.indexOf('http') == 0 && Platform.OS == 'ios') {
      return false;
    }
    return true;
  };

  render() {
    const {is_login} = this.props.loginState;
    const {is_refresh, item, isShowFile} = this.state;
    const title = item && item.title ? item.title : '---';
    const html = item && item.content ? item.content : '';
    const hide_image = item && item.id && item.id == 11;
    const file_html =
      is_login && item && item.fileurl && item.fileurl != 'null'
        ? item.fileurl
        : '';
    const opacity = this._scrollY.interpolate({
      inputRange: [0, HEIGHT_IMAGE],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    const tintColor = this._scrollY.interpolate({
      inputRange: [0, HEIGHT_IMAGE],
      outputRange: ['white', '#6d6d6d'],
      extrapolate: 'clamp',
    });
    return (
      <Views.BaseView
        footerModal={
          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor={'transparent'}
            style={styles.loading}
            size={SIZE}
          />
        }
        bottomModal={
          <Views.SignupModal ref={ref => (this.SignupModal = ref)} />
        }>
        <ScrollView
          scrollEventThrottle={16}
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {y: this._scrollY}}},
          ])}
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          <Info {...this.props} item={item} onPress={this._showImage} />

          {html ? (
            <AutoHeightWebView
              useWebKit
              style={styles.webview}
              source={{
                html: Utils.getContentHtml(html, hide_image),
                baseUrl: 'http://index',
              }}
              originWhitelist={['*']}
              onLoadEnd={this.hideLoading}
              scrollEnabled={false}
              bounces={false}
              onShouldStartLoadWithRequest={() => true}
              viewportContent={'width=device-width, user-scalable=no'}
            />
          ) : null}

          <View
            style={{
              flexDirection: 'row',
              width: width - 20,
              marginHorizontal: 10,
              height: 5,
              marginBottom: 10,
            }}>
            <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
          </View>

          {is_login ? (
            file_html || !isShowFile ? (
              <AutoHeightWebView
                useWebKit
                ref={ref => (this.webview = ref)}
                style={styles.webview}
                source={{html: file_html}}
                originWhitelist={['*']}
                scrollEnabled={false}
                bounces={false}
                viewportContent={'width=device-width, user-scalable=no'}
                onShouldStartLoadWithRequest={this.handleWebViewRequest}
                onNavigationStateChange={
                  this.handleWebViewNavigationStateChange
                }
              />
            ) : (
              <View style={styles.webview}>
                <Text style={styles.textNo}>Không có tài liệu đính kèm</Text>
              </View>
            )
          ) : (
            <View style={styles.viewDownload}>
              <TouchableOpacity
                onPress={this._signup}
                style={styles.buttonDownload}
                activeOpacity={0.7}>
                <FastImage
                  source={Icons.download}
                  style={styles.download}
                  resizeMode="contain"
                />

                <Text style={styles.textDown}>Tải tài liệu</Text>
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>

        <Animated.View style={[styles.header, {opacity}]}>
          <Text style={styles.name} numberOfLines={1}>
            {title}
          </Text>
        </Animated.View>

        <TouchableOpacity
          onPress={this._goBack}
          style={[styles.viewIcon]}
          activeOpacity={0.7}>
          <Animated.Image
            source={Icons.back}
            style={[styles.icon, {tintColor}]}
            resizeMode="contain"
          />
        </TouchableOpacity>

        <Views.ImageModal ref={ref => (this.ImageModal = ref)} />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Detail);

class Info extends React.PureComponent {
  render() {
    const {item, typeObject, onPress} = this.props;
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const title = item && item.title ? item.title : '---';
    const description =
      item && item.intro
        ? // item.intro.length > 500 ?
          //     item.intro.slice(0, 499) :
          item.intro
        : '---';
    const backgroundColor =
      item && item.color ? item.color : Colors.silverSERVICE;
    const sale =
      item && item.sale
        ? parseInt(item.sale) >= 100
          ? 'free'
          : parseInt(item.sale) > 0
          ? `-${item.sale}%`
          : ''
        : '';
    return (
      <View>
        {typeObject == Contants.TypeObject.PRODUCT ? (
          <TouchableOpacity activeOpacity={0.7} onPress={() => onPress(image)}>
            <FastImage source={{uri: image}} style={styles.image} />

            {sale ? (
              <View style={styles.sale}>
                <FastImage source={Icons.sale} style={styles.iconSale} />

                <Text style={styles.textSale}>{sale}</Text>
              </View>
            ) : null}
            <Text style={styles.title} numberOfLines={5}>
              {title.toUpperCase()}
            </Text>
          </TouchableOpacity>
        ) : typeObject == Contants.TypeObject.SERVICE ? (
          <View style={styles.services}>
            <View style={[styles.backgroundColor, {backgroundColor}]}>
              <Text style={styles.title2} numberOfLines={5}>
                {title.toUpperCase()}
              </Text>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => onPress(image)}>
              <FastImage source={{uri: image}} style={styles.image2} />
            </TouchableOpacity>
          </View>
        ) : null}

        <Text style={styles.content}>{Entities?.decode(description)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    top: (height - SIZE) / 2,
    left: (width - SIZE) / 2,
  },
  header: {
    width,
    height: 50,
    position: 'absolute',
    top: 0,
    left: 0,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f2f2f2',
  },
  name: {
    fontSize: 18,
    color: 'black',
    flex: 1,
    marginRight: 40,
    marginLeft: 40,
    textAlign: 'center',
    fontFamily: Fonts.utm_avo,
  },
  viewIcon: {
    height: 50,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    position: 'absolute',
    top: 0,
    left: 0,
  },
  title: {
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
    margin: 10,
    marginBottom: 5,
  },
  image: {
    width,
    height: HEIGHT_IMAGE,
    backgroundColor: Colors.silverCONMIK,
  },

  content: {
    color: '#094a72',
    fontSize: 16,
    marginVertical: 10,
    marginHorizontal: 10,
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
  },
  icon: {
    width: 24,
    height: 24,
  },
  services: {
    flexDirection: 'row',
  },
  backgroundColor: {
    height: HEIGHT_IMAGE,
    width: width - WIDTH_IMAGE_SERVICE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image2: {
    width: WIDTH_IMAGE_SERVICE,
    height: HEIGHT_IMAGE,
    backgroundColor: Colors.greyCONMIK,
  },
  title2: {
    fontSize: 16,
    color: 'white',
    marginHorizontal: 10,
    fontFamily: Fonts.utm_avo_bold,
    textAlign: 'center',
  },
  sale: {
    position: 'absolute',
    right: 10,
  },
  iconSale: {
    width: 50,
    height: 50,
  },
  textSale: {
    color: 'white',
    position: 'absolute',
    width: 50,
    height: 50,
    textAlign: 'center',
    paddingTop: 15,
    fontSize: 14,
    fontWeight: 'bold',
  },
  webview: {
    width: WIDTH_WEBVIEW,
    marginHorizontal: 10,
    marginBottom: 10,
    opacity: 0.99,
  },
  viewDownload: {
    width,
    alignItems: 'center',
  },
  buttonDownload: {
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  download: {
    width: 16,
    height: 16,
    marginRight: 10,
  },
  textDown: {
    color: 'black',
    fontFamily: Fonts.utm_avo_bold,
    fontSize: 16,
  },
  textNo: {
    color: 'grey',
    fontFamily: Fonts.utm_avo_bold,
    fontSize: 16,
  },
});
