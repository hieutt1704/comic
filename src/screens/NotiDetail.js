import React, {Component} from 'react';
import {Text, View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Contants from '../config/Constants';
import * as Views from '../views/ViewManagers';
import * as Storage from '../modules/asyncStorge';
import * as Routers from '../navigations/Navigations';

class NotiDetail extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    const {notis} = this.props;
    this.state = {
      notis: notis || [],
      page: 0,
      is_refresh: false,
      is_load_more: false,
      end_page: true,
    };
  }

  componentDidMount() {
    this._getNoti();
  }

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _onNoData = async () => {
    this.BaseView.showLoadCenter();
    await this._getNoti();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  _getNoti = () => {
    const {loginState, type} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&type=${type}&page=0`;
    return Apis.getNoti(params).then(res => {
      // console.log('getNoti ' + type, params, res)
      if (res.status == 1 && res.data && res.data.notis) {
        if (type == 0) {
          Storage.setStorageData(
            Storage.noti_sales,
            JSON.stringify(res.data.notis),
          );
        }
        if (type == 1) {
          Storage.setStorageData(
            Storage.noti_products,
            JSON.stringify(res.data.notis),
          );
        }
        if (type == 2) {
          Storage.setStorageData(
            Storage.noti_services,
            JSON.stringify(res.data.notis),
          );
        }
        if (type == 3) {
          Storage.setStorageData(
            Storage.noti_news,
            JSON.stringify(res.data.notis),
          );
        }
        if (this._mounted) {
          this.setState({
            page: 1,
            notis: res.data.notis,
            end_page: res.data.notis.length < res.data.size,
          });
        }
      } else {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getNoti();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _onLoadMore = () => {
    const {loginState, type} = this.props;
    const {token, username} = loginState;
    const {is_load_more, end_page, notis, page} = this.state;
    if (end_page || is_load_more) {
      return;
    }
    const params = `?tk=${username}&token=${token}&type=${type}&page=${
      page + 1
    }`;
    this.setState({is_load_more: true}, () => {
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getNoti(params).then(res => {
          // console.log("get more getNoti", params, res)
          if (this._mounted) {
            this.setState({is_load_more: false});
          }
          if (res.status == 1) {
            this.setState({
              page: page + 1,
              notis: [...notis, ...res.data.notis],
              end_page: res.data.notis.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _goNotiDetail = item => {
    Routers.goAbout({item, typeObject: Contants.TypeObject.NOTI});
  };

  render() {
    const {type} = this.props;
    const {is_refresh, is_load_more, notis} = this.state;
    const title =
      type == 0
        ? 'Khuyến mãi'
        : type == 1
        ? 'Sản phẩm mới'
        : type == 2
        ? 'Dịch vụ nổi bật'
        : 'Tin tức mới';
    return (
      <Views.BaseView
        title={title}
        data={notis}
        onNoData={this._onNoData}
        onLeftPress={this._goBack}
        ref={ref => (this.BaseView = ref)}>
        <FlatList
          data={notis}
          initialNumToRender={5}
          refreshing={is_refresh}
          onRefresh={this._onRefresh}
          keyExtractor={(item, index) => index + ''}
          renderItem={({item, index}) => (
            <ItemNoti item={item} index={index} onPress={this._goNotiDetail} />
          )}
          ItemSeparatorComponent={() => <View style={{width: 5, height: 5}} />}
          onEndReached={this._onLoadMore}
          onEndReachedThreshold={0.2}
          ListFooterComponent={
            <Views.ProgressLoading
              size={40}
              visible={is_load_more}
              typeLoading={Contants.TypeLoading.LOAD_MORE}
            />
          }
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(NotiDetail);

class ItemNoti extends React.PureComponent {
  render() {
    const {item, index, onPress} = this.props;
    const title = item && item.title ? item.title : '---';
    const content = item && item.intro ? item.intro : '---';
    const date = item && item.date ? item.date : '---';
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={[styles.container, {borderTopWidth: index != 0 ? 0.3 : 0}]}
        onPress={() => onPress(item)}>
        <Text style={styles.title} numberOfLines={1}>
          {title}
        </Text>

        <Text style={styles.content} numberOfLines={2}>
          {content}
        </Text>

        <Text style={styles.date}>{date}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    padding: 10,
    borderColor: 'grey',
    borderTopWidth: 0.3,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontFamily: Fonts.myriadpro_regular,
  },
  content: {
    marginTop: 5,
    fontSize: 16,
    color: '#606060',
    fontFamily: Fonts.myriadpro_regular,
  },
  date: {
    marginTop: 5,
    fontSize: 14,
    color: 'grey',
    textAlign: 'right',
    fontFamily: Fonts.myriadpro_regular,
  },
});
