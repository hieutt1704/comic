import React, {Component} from 'react';
import {FlatList, StyleSheet, Dimensions, View} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Colors from '../config/Colors';
import * as Apis from '../networking/Apis';
import * as Views from '../views/ViewManagers';
import * as Icons from '../config/IconManagers';
import * as Storage from '../modules/asyncStorge';
import {TypeLoading} from '../config/Constants';
import * as Routers from '../navigations/Navigations';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;

class News extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      is_refresh: false,
      is_load_more: false,
      end_page: false,
      news: [],
      page: 0,
    };
  }

  async componentDidMount() {
    const data_news = await Storage.getStorageData(Storage.news);
    if (data_news && this._mounted) {
      this.setState({news: JSON.parse(data_news)});
    }
    if (this.state.news.length == 0 && this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    await this._getData();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  }

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _getData = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?tk=${username}&token=${token}&page=0`;
    return Apis.getNews(params).then(res => {
      if (res.status == 1 && res.data) {
        Storage.setStorageData(Storage.news, JSON.stringify(res.data.news));
        if (this._mounted) {
          this.setState({
            page: 0,
            news: res.data.news,
            end_page: res.data.news.length < res.data.size,
          });
        }
      } else {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _onLoadMore = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const {is_load_more, end_page, news, page} = this.state;
    if (end_page || is_load_more) {
      return;
    }
    const params = `?tk=${username}&token=${token}&page=${page + 1}`;
    this.setState({is_load_more: true}, () => {
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getNews(params).then(res => {
          // console.log(res, "get more news", params)
          if (this._mounted) {
            this.setState({is_load_more: false});
          }
          if (res.status == 1) {
            this.setState({
              page: page + 1,
              news: [...news, ...res.data.news],
              end_page: res.data.news.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _showNewDetail = item => {
    Routers.goNewDetail({item});
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  render() {
    const {is_refresh, is_load_more, news} = this.state;
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title="Tin tức"
        onLeftPress={this._goBack}
        rightIcon={Icons.search}
        onRightPress={this._goSearch}>
        <FlatList
          style={styles.container}
          data={news}
          initialNumToRender={1}
          refreshing={is_refresh}
          onRefresh={this._onRefresh}
          keyExtractor={(item, index) => index + ''}
          ListHeaderComponent={
            <View style={styles.vDash}>
              <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
              <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
              <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
            </View>
          }
          renderItem={({item, index}) => (
            <Views.ItemNew
              index={index}
              item={item}
              onPress={this._showNewDetail}
            />
          )}
          onEndReached={this._onLoadMore}
          onEndReachedThreshold={0.2}
          ListFooterComponent={
            is_load_more ? (
              <Views.ProgressLoading
                size={40}
                visible={is_load_more}
                typeLoading={TypeLoading.LOAD_MORE}
              />
            ) : (
              <View style={{height: 10}} />
            )
          }
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(News);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  vDash: {
    flexDirection: 'row',
    width: WIDTH_COVER,
    marginHorizontal: 10,
    marginTop: 10,
    height: 5,
  },
});
