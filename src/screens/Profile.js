import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  RefreshControl,
} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Colors from '../config/Colors';
import * as Views from '../views/ViewManagers';
import FastImage from 'react-native-fast-image';
import * as Icons from '../config/IconManagers';
import * as Storage from '../modules/asyncStorge';
import * as Routers from '../navigations/Navigations';

const SIZE_LOAD = 40;
const HEIGHT_COVER = 250;
const {width} = Dimensions.get('window');

class Profile extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      is_refresh: false,
      profile: {},
    };
  }

  async componentDidMount() {
    const local_profile = await Storage.getStorageData(Storage.profile);
    if (local_profile) {
      this.setState({profile: JSON.parse(local_profile)});
    }
    if (Object.keys(this.state.profile).length == 0) {
      this.ProgressLoading.show();
    }
    await this._getProfile();
    if (this.ProgressLoading) {
      this.ProgressLoading.hide();
    }
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _goEdit = () => {
    const {profile} = this.state;
    Routers.goEditProfile({profile, updateProfile: this._updateProfile});
  };

  _updateProfile = profile => {
    this.setState({profile});
    Storage.setStorageData(Storage.profile, JSON.stringify(profile));
  };

  _getProfile = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}`;
    return Apis.getProfile(params).then(res => {
      console.log('profile', params, res);
      if (res.status == 1 && res.data) {
        Storage.setStorageData(Storage.profile, JSON.stringify(res.data));
        if (this._mounted) {
          this.setState({profile: res.data});
        }
      } else {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getProfile();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _showModalPickImage = () => {
    if (this.PickImageModal) {
      this.PickImageModal.showModal();
    }
  };

  _changeCoverImage = async res => {
    this.LoadingCover.show();
    const dataImage = await Utils.resizeImage(res);
    const image = {
      uri: dataImage.uri,
      name: dataImage.fileName,
      type: dataImage.type,
      // size: dataImage.fileSize,
    };
    // console.log(dataImage)
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?tk=${username}&token=${token}`;
    const data = new FormData();
    data.append('file', image);
    const resp = await Apis.updateCover(params, data);
    if (resp.status == 1) {
      const profile = {...this.state.profile, cover: res.uri};
      this.setState({profile});
      Storage.setStorageData(Storage.profile, JSON.stringify(profile));
    } else {
      Utils.showToast(res.msg || 'Cập nhật ảnh nền lỗi');
    }
    if (this.LoadingCover) {
      this.LoadingCover.hide();
    }
  };

  _showImage = source => {
    // if (source && source.uri)
    //     this.ImageModal.showModal([source])
  };

  //file:// content://
  _checkUrl = url => {
    return url.indexOf('http') == 0 ||
      url.indexOf('file://') == 0 ||
      url.indexOf('content://') == 0
      ? {uri: url}
      : {uri: Apis.domain + url};
  };

  render() {
    const {type_account} = this.props.loginState;
    const {profile, is_refresh} = this.state;
    const name = profile.name || '---';
    const phone = profile.telephone || '---';
    const email = profile.email || '---';
    const birthday = profile.birthday || '---';
    const address = profile.address || '---';
    const company = profile.company || '---';
    const position = profile.position || '---';
    const avatar = profile.avatar
      ? this._checkUrl(profile.avatar)
      : Icons.banner;
    // Icons.no_image
    const cover_souce = profile.cover
      ? this._checkUrl(profile.cover)
      : Icons.services;
    const activeOpacityAvatar = 1; // avatar == Icons.no_image ? 1 : 0.7
    const activeOpacityCover = 1; //cover_souce == Icons.services ? 1 : 0.7
    const type_icon =
      type_account == 'gg'
        ? Icons.google_color
        : type_account == 'fb'
        ? Icons.facebook_color
        : null;
    return (
      <Views.BaseView
        footerModal={
          <Views.ProgressLoading ref={ref => (this.ProgressLoading = ref)} />
        }
        bottomModal={<Views.ImageModal ref={ref => (this.ImageModal = ref)} />}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          {/* COVER */}
          <TouchableOpacity
            onPress={() => this._showImage(cover_souce)}
            activeOpacity={activeOpacityCover}>
            <FastImage source={cover_souce} style={styles.cover} />
          </TouchableOpacity>

          <Views.ProgressLoading
            ref={ref => (this.LoadingCover = ref)}
            style={styles.loadCover}
            size={SIZE_LOAD}
            backgroundColor="transperent"
          />

          {/* <TouchableOpacity
                        onPress={this._showModalPickImage}
                        activeOpacity={0.7}
                        style={styles.vcamera}
                    >
                        <FastImage
                            style={styles.camera}
                            tintColor="black"
                            source={Icons.ICON_CAMERA}
                            resizeMode="contain"
                            tintColor='white'
                        />
                    </TouchableOpacity> */}

          <View style={styles.titleView}>
            <View style={{width: 140}} />

            <Text style={styles.title}>{name}</Text>
          </View>

          <TouchableOpacity
            onPress={() => this._showImage(avatar)}
            activeOpacity={activeOpacityAvatar}
            style={styles.vAvatar}>
            <FastImage source={avatar} style={styles.avatar} />
          </TouchableOpacity>

          {/* INFO */}
          <View style={styles.info}>
            <Text style={styles.text}>{name}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{phone}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{email}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{birthday}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{company}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{position}</Text>

            <View style={styles.dash} />

            <Text style={styles.text}>{address}</Text>
          </View>
        </ScrollView>

        {/* HEADER */}
        <View style={styles.header}>
          <TouchableOpacity
            onPress={this._goBack}
            style={[styles.viewIcon]}
            activeOpacity={0.7}>
            <FastImage
              source={Icons.back}
              style={styles.icon}
              resizeMode="contain"
              tintColor="white"
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this._goEdit}
            style={[styles.viewIcon]}
            activeOpacity={0.7}>
            <Text style={styles.textEdit}>Sửa</Text>
          </TouchableOpacity>
        </View>

        {type_account ? (
          <View
            style={[
              styles.viewIcon,
              {position: 'absolute', bottom: 20, left: 20},
            ]}>
            <FastImage
              source={type_icon}
              style={{width: 40, height: 40}}
              resizeMode="contain"
            />
          </View>
        ) : null}

        <Views.PickImageModal
          title="Cập nhật ảnh nền"
          ref={ref => (this.PickImageModal = ref)}
          pickImage={this._changeCoverImage}
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cover: {
    width,
    height: HEIGHT_COVER,
    backgroundColor: Colors.greyMDC,
  },
  header: {
    width,
    position: 'absolute',
    top: 0,
    left: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewIcon: {
    height: 50,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  icon: {
    width: 24,
    height: 24,
  },
  titleView: {
    position: 'absolute',
    top: HEIGHT_COVER - 60,
    height: 60,
    width,
    flexDirection: 'row',
    backgroundColor: '#00000070',
    alignItems: 'center',
  },
  title: {
    flex: 1,
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    fontFamily: Fonts.utm_avo,
  },
  textEdit: {
    fontSize: 17,
    color: 'white',
    fontFamily: Fonts.utm_avo,
  },
  edit: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  vAvatar: {
    position: 'absolute',
    top: HEIGHT_COVER - 130,
    left: 15,
  },
  avatar: {
    borderWidth: 2,
    borderColor: 'white',
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor: 'grey',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  info: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    fontSize: 14,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  dash: {
    marginVertical: 10,
    height: 0.5,
    backgroundColor: Colors.greyCONMIK,
  },
  phone: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  flag: {
    width: 40,
    height: 20,
  },
  logoutView: {
    backgroundColor: '#58ccb7',
    height: 125,
    paddingHorizontal: 30,
    alignItems: 'center',
  },
  button: {
    marginTop: 15,
    height: 40,
    width: '100%',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  vcamera: {
    position: 'absolute',
    top: HEIGHT_COVER - 90,
    right: 5,
    width: 40,
    height: 25,
    borderRadius: 5,
    backgroundColor: '#00000070',
    justifyContent: 'center',
    alignItems: 'center',
  },
  camera: {
    width: 16,
    height: 16,
  },
  loadCover: {
    position: 'absolute',
    top: (HEIGHT_COVER - SIZE_LOAD) / 2,
    left: (width - SIZE_LOAD) / 2,
  },
});
