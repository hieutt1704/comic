import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Keyboard,
  View,
} from 'react-native';

import * as Utils from '../Utils';
import I18n from '../modules/i18n';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import {fontSize} from '../modules/fontSize';
import * as Views from '../views/ViewManagers';
import * as Icons from '../config/IconManagers';
import FastImage from 'react-native-fast-image';
import SplashScreen from "react-native-splash-screen"
import * as Routers from '../navigations/Navigations';
import LinearGradient from 'react-native-linear-gradient';
// import { LoginManager, AccessToken } from "react-native-fbsdk";
import {login, loginSocial} from '../redux/actions/loginAction';
// import { GoogleSignin, statusCodes } from 'react-native-google-signin';

const {width, height} = Dimensions.get('window');
const HEIGHT_BANGE = 1153;
const WIDTH_BANGE = 1737;
const HEIGHT_IMAGE = (width * HEIGHT_BANGE) / WIDTH_BANGE;
const HEIGHT_FORM = height - HEIGHT_IMAGE;
const SIZE = Utils.isIphoneX() ? HEIGHT_FORM / 200 : HEIGHT_FORM / 400;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      password: '',
    };
  }

  async componentDidMount() {
    // GoogleSignin.configure({});
    SplashScreen.hide()
  }

  _onLogin = async () => {
    const {phone, password} = this.state;
    if (!phone) {
      Utils.showToast('Chưa nhập số điện thoại');
    } else if (!password) {
      Utils.showToast('Chưa nhập mật khẩu');
    } else if (Utils.validatePassword(password)) {
      Utils.showToast('Mật khẩu không được chứa dấu cách hoặc ký đặc biệt');
    } else {
      const params = {phone, password};
      if (this.ProgressLoading) {
        this.ProgressLoading.show();
      }
      this.props.login(params).then(res => {
        if (this.ProgressLoading) {
          this.ProgressLoading.hide();
        }
        if (res.status == 1 && res.data && res.data.token) {
          Routers.goHome();
        } else {
          Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      });
    }
  };

  _onForgotPassword = () => {
    Routers.goForgot();
  };

  _onSignup = () => {
    Routers.goSignup();
  };

  _loginFacebook = () => {
    // // Attempt a login using the Facebook login dialog asking for default permissions.
    // let e = this
    // if (this.ProgressLoading) this.ProgressLoading.show()
    // LoginManager.logInWithPermissions(["public_profile"]).then(
    //   function (result) {
    //     // console.log("Login FB: ", result)
    //     if (result.isCancelled) {
    //       // user cancelled the login flow
    //       if (e.ProgressLoading) e.ProgressLoading.hide()
    //     } else {
    //       // get fb token
    //       AccessToken.getCurrentAccessToken().then(
    //         (data) => {
    //           // console.log(data)
    //           const params = { username: data.userID, token: data.accessToken, type: "fb" }
    //           e.props.loginSocial(params).then(res => {
    //             if (e.ProgressLoading) e.ProgressLoading.hide()
    //             if (res.status != 1)
    //               Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống')
    //           })
    //         }
    //       ).catch(er => {
    //         if (e.ProgressLoading) e.ProgressLoading.hide()
    //       })
    //     }
    //   },
    //   function (error) {
    //     // console.log("Login fail with error: " + error);
    //     Utils.showToast('Opps, lỗi kết nối với hệ thống')
    //     if (e.ProgressLoading) e.ProgressLoading.hide()
    //   }
    // );
  };

  _loginGoogle = async () => {
    // if (this.ProgressLoading) this.ProgressLoading.show()
    // try {
    //   await GoogleSignin.hasPlayServices();
    //   const userInfo = await GoogleSignin.signIn();
    //   const token = await GoogleSignin.getTokens();
    //   console.log("LOGIN GOOGLE: ", userInfo, token)
    //   const params = { username: userInfo.user.id, token: token.accessToken, type: "gg" }
    //   this.props.loginSocial(params).then(res => {
    //     if (this.ProgressLoading) this.ProgressLoading.hide()
    //     if (res.status != 1)
    //       Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống')
    //   })
    // } catch (error) {
    //   if (this.ProgressLoading) this.ProgressLoading.hide()
    //   if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    //     // user cancelled the login flow
    //   } else if (error.code === statusCodes.IN_PROGRESS) {
    //     Utils.showToast('Đang chờ google xác thực')
    //     // operation (e.g. sign in) is in progress already
    //   } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    //     Utils.showToast('Máy chưa cài google store')
    //     // play services not available or outdated
    //   } else {
    //     // some other error
    //     // console.log(error)
    //     Utils.showToast('Opps, lỗi kết nối với hệ thống')
    //   }
    // }
  };

  render() {
    const {phone, password} = this.state;
    return (
      <Views.BaseView>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.container}
          onPress={Keyboard.dismiss}>
          <FastImage source={Icons.bange} style={styles.image} />

          <Views.InputView
            value={phone}
            ref={ref => (this.Username = ref)}
            styleContainer={styles.inputView}
            styleForm={styles.form}
            style={styles.textinput}
            placeholder={'Số điện thoại'}
            formText={true}
            returnKeyType={'next'}
            autoCapitalize={'none'}
            onSubmitEditing={() => this.Password.focus()}
            keyboardType={'number-pad'}
            icon={Icons.account_rad}
            styleFormText={styles.formText}
            styleIcon={styles.styleIcon}
            onChangeText={phone => this.setState({phone})}
          />

          <Views.InputView
            value={password}
            ref={ref => (this.Password = ref)}
            styleContainer={styles.inputView}
            styleForm={styles.form}
            style={styles.textinput}
            placeholder={I18n.t('password')}
            secureTextEntry={true}
            formText={true}
            autoCapitalize={'none'}
            onSubmitEditing={this._onLogin}
            icon={Icons.password_rad}
            styleFormText={styles.formText}
            styleIcon={styles.styleIcon}
            onChangeText={password => this.setState({password})}
          />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.button}
            onPress={this._onLogin}>
            <LinearGradient
              colors={['#a11e23', '#e91e24']}
              style={styles.gradient}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}>
              <Text style={[styles.text, {fontSize: fontSize + 2}]}>
                {I18n.t('login').toUpperCase()}
              </Text>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.forgot}
            onPress={this._onForgotPassword}>
            <Text
              style={{
                fontSize,
                color: 'grey',
                fontFamily: Fonts.myriadpro_regular,
              }}>
              {I18n.t('forgot').toUpperCase()}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.forgot}
            onPress={this._onSignup}>
            <Text
              style={{
                fontSize,
                color: 'grey',
                fontFamily: Fonts.myriadpro_regular,
              }}>
              {I18n.t('signup').toUpperCase()}
            </Text>
          </TouchableOpacity>

          <View style={styles.other}>
            <View style={styles.dash} />

            <Text
              style={{
                fontSize: 17,
                color: 'black',
                marginHorizontal: 10,
                fontFamily: Fonts.myriadpro_regular,
              }}>
              {I18n.t('other').toUpperCase()}
            </Text>

            <View style={styles.dash} />
          </View>

          <View style={styles.viewIcon}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{marginRight: 40}}
              onPress={this._loginFacebook}>
              <FastImage source={Icons.facebook} style={[styles.icon]} />
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.7} onPress={this._loginGoogle}>
              <FastImage source={Icons.google} style={styles.icon} />
            </TouchableOpacity>
          </View>

          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor="#00000030"
          />
        </TouchableOpacity>
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

const mapDispatchToProps = {
  login,
  loginSocial,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  login: {
    fontSize: fontSize + 2,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center',
    marginTop: 5 * SIZE,
  },
  inputView: {
    marginHorizontal: 40,
    marginTop: 5 * SIZE,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    height: 50,
  },
  styleIcon: {
    width: 25,
    height: 25,
    marginRight: 10,
    marginBottom: 5 * SIZE,
  },
  formText: {
    paddingBottom: 5 * SIZE,
    borderBottomWidth: 0.3,
    borderColor: 'grey',
  },
  textinput: {
    flex: 1,
    color: 'black',
    fontFamily: Fonts.myriadpro_regular,
    fontSize: 18,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  image: {
    width: width,
    height: HEIGHT_IMAGE,
  },
  button: {
    marginHorizontal: 70,
    marginTop: 10 * SIZE,
    marginBottom: 10 * SIZE,
    height: 50,
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 25,
  },
  forgot: {
    marginHorizontal: 15,
    marginBottom: 10 * SIZE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  other: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  dash: {
    width: 50,
    height: 0.5,
    backgroundColor: 'grey',
  },
  viewIcon: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10 * SIZE,
  },
  icon: {
    width: 60,
    height: 60,
  },
});
