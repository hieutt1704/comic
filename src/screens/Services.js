import React, { Component } from 'react';
import { FlatList, StyleSheet, View, Dimensions } from 'react-native';

import * as Utils from '../Utils';
import { connect } from 'react-redux';
import * as Colors from '../config/Colors';
import * as Apis from '../networking/Apis';
import * as Views from '../views/ViewManagers';
import * as Icons from '../config/IconManagers';
import FastImage from 'react-native-fast-image';
import * as Constants from '../config/Constants';
import * as Storage from '../modules/asyncStorge';
import * as Routers from '../navigations/Navigations';
import FakeScreen from './FakeScreen';
import * as fireBaseApi from '../networking/firebaseApi';

const { width } = Dimensions.get('window');

class Services extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      banner: '',
      is_refresh: false,
      is_load_more: false,
      end_page: false,
      services: [],
      page: 1,
      flag: false
    };
  }

  componentDidMount = async () => {
    const data_services = await Storage.getStorageData(Storage.services);
    if (data_services && this._mounted) {
      const data = JSON.parse(data_services);
      this.setState({ services: data.services, banner: data.banner });
    }
    if (this.state.services.length == 0 && this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    await this._getFlag();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  _getFlag = async () => {
    try {
      const flag = await fireBaseApi.getFlag();
      flag ?
        this.setState({
          flag: flag
        })
        : await this._getData();
    } catch (error) {
      this._getData();
    }
  }

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _getData = showFail => {
    const { loginState } = this.props;
    const { token, username } = loginState;
    const params = `?tk=${username}&token=${token}&page=0`;
    return Apis.getServices(params).then(res => {
      console.log('get serviecs:', params, res);
      if (res.status == 1 && res.data) {
        Storage.setStorageData(Storage.services, JSON.stringify(res.data));
        if (this._mounted) {
          this.setState({
            page: 0,
            services: res.data.services,
            banner: res.data.banner,
            end_page: res.data.services.length < res.data.size,
          });
        }
      } else if (showFail) {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({ is_refresh: true });
    await this._getData(true);
    if (this._mounted) {
      this.setState({ is_refresh: false });
    }
  };

  _onLoadMore = () => {
    const { loginState } = this.props;
    const { token, username } = loginState;
    const { is_load_more, end_page, services, page } = this.state;
    if (end_page || is_load_more) {
      return;
    }
    const params = `?tk=${username}&token=${token}&page=${page + 1}`;
    this.setState({ is_load_more: true }, () => {
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getServices(params).then(res => {
          console.log(res, 'get more services', params);
          if (this._mounted) {
            this.setState({ is_load_more: false });
          }
          if (res.status == 1) {
            this.setState({
              page: page + 1,
              services: [...services, ...res.data.services],
              end_page: res.data.services.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _showServiceDetail = item => {
    Routers.goDetail({ item, typeObject: Constants.TypeObject.SERVICE });
  };

  _onMenu = () => {
    Routers.showMenu(true);
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  render() {
    const { is_refresh, services, banner, is_load_more, flag } = this.state;
    const imageCover = banner
      ? banner.indexOf('http') == 0
        ? { uri: banner }
        : { uri: Apis.domain + banner }
      : Icons.services;
    return (
      flag ? <FakeScreen /> :
        <Views.BaseView
          ref={ref => (this.BaseView = ref)}
          title="Dịch vụ"
          logo={Icons.logo}
          rightIcon={Icons.search}
          leftIcon={Icons.menu}
          onLeftPress={this._onMenu}
          onRightPress={this._goSearch}
          onNoData={this.componentDidMount}
          data={services}>
          <FlatList
            style={styles.container}
            data={services}
            initialNumToRender={1}
            numColumns={2}
            refreshing={is_refresh}
            onRefresh={this._onRefresh}
            keyExtractor={(item, index) => index + ''}
            ListHeaderComponent={
              <View>
                <FastImage
                  source={imageCover}
                  style={styles.image}
                  resizeMode="cover"
                />
                <View style={styles.dash} />
              </View>
            }
            renderItem={({ item, index }) => (
              <Views.ItemService
                index={index}
                item={item}
                onPress={this._showServiceDetail}
                type={Constants.TypeObject.SERVICE}
              />
            )}
            ItemSeparatorComponent={() => (
              <View style={{ width: 10, height: 10 }} />
            )}
            onEndReached={this._onLoadMore}
            onEndReachedThreshold={0.2}
            ListFooterComponent={
              is_load_more ? (
                <Views.ProgressLoading
                  size={40}
                  visible={is_load_more}
                  typeLoading={Constants.TypeLoading.LOAD_MORE}
                />
              ) : (
                <View style={{ height: 10 }} />
              )
            }
          />
        </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Services);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: width / 2,
    width,
    backgroundColor: Colors.silverCONMIK,
  },
  dash: {
    height: 7.5,
    backgroundColor: Colors.silverCONMIK,
    width: width - 20,
    marginTop: 10,
    marginBottom: 15,
  },
});
