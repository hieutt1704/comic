import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableOpacity,
  View,
} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Colors from '../config/Colors';
import * as Views from '../views/ViewManagers';
import * as Icons from '../config/IconManagers';
import FastImage from 'react-native-fast-image';
import * as Constants from '../config/Constants';
// import * as Storage from '../modules/asyncStorge'
import * as Routers from '../navigations/Navigations';
import {getStatusBarHeight} from 'react-native-status-bar-height';

const Entities = require('html-entities').AllHtmlEntities;

const {width} = Dimensions.get('window');
const STATUS_HEIGHT = getStatusBarHeight(true);
const WIDTH_IMAGE = width - 20;

class Products extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: this.props.item || {},
      is_refresh: false,
      is_load_more: false,
      end_page: true,
      products: [],
      page: 0,
    };
  }

  componentDidMount = async () => {
    // const data_products = await Storage.getStorageData(Storage.products)
    // if (data_products && this._mounted) {
    //     this.setState({ products: JSON.parse(data_products) })
    // }
    if (this.state.products.length == 0 && this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    await this._getData();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  componentWillUnmount() {
    console.log('????');
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _getData = () => {
    const {selectedCategory} = this.state;
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?tk=${username}&token=${token}&cate_id=${selectedCategory.id}&page=0`;
    console.log('????');
    return Apis.getProducts(params).then(res => {
      console.log('get products:', params, res);
      if (res.status == 1 && res.data) {
        // Storage.setStorageData(Storage.products, JSON.stringify(res.data.products))
        if (this._mounted) {
          this.setState({
            page: 0,
            products: res.data.products,
            end_page: res.data.products.length < res.data.size,
          });
        }
      } else {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _onLoadMore = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const {is_load_more, end_page, products, selectedCategory, page} =
      this.state;
    if (end_page || is_load_more) {
      return;
    }
    const params = `?tk=${username}&token=${token}&cate_id=${
      selectedCategory.id
    }&page=${page + 1}`;
    this.setState({is_load_more: true}, () => {
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getProducts(params).then(res => {
          // console.log(res, "get more products", params)
          if (this._mounted) {
            this.setState({is_load_more: false});
          }
          if (res.status == 1) {
            this.setState({
              page: page + 1,
              products: [...products, ...res.data.products],
              end_page: res.data.products.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _showProductDetail = item => {
    Routers.goDetail({item, typeObject: Constants.TypeObject.PRODUCT});
  };

  _onPressTitle = () => {
    const {selectedCategory} = this.state;
    const {cate_details} = this.props;
    if (this.DropDownModal && cate_details && cate_details.data) {
      this.DropDownModal.showModal(
        STATUS_HEIGHT,
        width - 100,
        50,
        cate_details.data,
        selectedCategory,
      );
    }
  };

  _pickItemDrop = item => {
    this.setState({selectedCategory: item});
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  render() {
    const {is_refresh, selectedCategory, products, is_load_more} = this.state;
    const title =
      selectedCategory && selectedCategory.title ? selectedCategory.title : '';
    const content =
      selectedCategory && selectedCategory.content
        ? selectedCategory.content
        : '';
    const viewContent = content ? (
      <Text style={styles.content}>{Entities?.decode(content)}</Text>
    ) : (
      <View style={{height: 10}} />
    );
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title={title}
        rightIcon={Icons.search}
        onLeftPress={this._goBack}
        onRightPress={this._goSearch}
        data={products}
        onNoData={this.componentDidMount}
        headerView={products.length == 0 ? viewContent : null}>
        <FlatList
          style={styles.container}
          data={products}
          initialNumToRender={1}
          refreshing={is_refresh}
          onRefresh={this._onRefresh}
          keyExtractor={(item, index) => index + ''}
          ListHeaderComponent={viewContent}
          renderItem={({item, index}) => (
            <ItemProduct
              index={index}
              item={item}
              onPress={this._showProductDetail}
            />
          )}
          onEndReached={this._onLoadMore}
          onEndReachedThreshold={0.2}
          ListFooterComponent={
            <Views.ProgressLoading
              size={40}
              visible={is_load_more}
              typeLoading={Constants.TypeLoading.LOAD_MORE}
            />
          }
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Products);

class ItemProduct extends React.PureComponent {
  render() {
    const {item, onPress} = this.props;
    const title = item && item.title ? item.title.toUpperCase() : '---';
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    return (
      <View>
        <TouchableOpacity onPress={() => onPress(item)} activeOpacity={0.7}>
          <FastImage source={{uri: image}} style={styles.image} />
        </TouchableOpacity>
        <View style={styles.viewText}>
          <Text style={styles.text} numberOfLines={5}>
            {title}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    width: WIDTH_IMAGE,
    height: WIDTH_IMAGE / 2,
    marginLeft: 10,
    backgroundColor: Colors.silverCONMIK,
  },
  viewText: {},
  text: {
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
    margin: 10,
    marginBottom: 20,
  },
  content: {
    marginHorizontal: 25,
    marginVertical: 10,
    fontSize: 16,
    textAlign: 'justify',
    color: '#094a72',
    fontFamily: Fonts.myriadpro_semibold,
  },
});
