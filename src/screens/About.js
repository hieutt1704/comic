import React, {Component} from 'react';
import {StyleSheet, ScrollView, Dimensions, RefreshControl} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Apis from '../networking/Apis';
import * as Views from '../views/ViewManagers';
import * as Contants from '../config/Constants';
import * as Storage from '../modules/asyncStorge';
import * as Routers from '../navigations/Navigations';
import AutoHeightWebView from 'react-native-autoheight-webview';

const SIZE = 60;
const {height, width} = Dimensions.get('window');

class About extends Component {
  _isMounted = true;
  constructor(props) {
    super(props);
    const {item} = this.props;
    this.state = {
      firtLoading: true,
      isLoading: true,
      is_error: false,
      data: item || {},
      is_refresh: false,
    };
  }

  async componentDidMount() {
    const {typeObject} = this.props;
    if (typeObject == Contants.TypeObject.ABOUT) {
      const about = await Storage.getStorageData(Storage.about);
      if (about && this._isMounted) {
        this.setState({
          firtLoading: false,
          data: JSON.parse(about),
        });
      }
    }
    await this._getData();
    if (this._isMounted && this.state.firtLoading) {
      this.setState({firtLoading: false});
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _getAbout = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?tk=${username}&token=${token}`;
    return Apis.getAbout(params).then(res => {
      // console.log('GET ABOUT:', res)
      if (res.status == 1 && res.data) {
        Storage.setStorageData(Storage.about, JSON.stringify(res.data));
        if (this._isMounted) {
          this.setState({data: res.data});
        }
      }
      return res;
    });
  };

  _getNotiDetail = () => {
    const {loginState, item} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&id=${item.id}`;
    return Apis.getNoti(params).then(res => {
      // console.log('GET NOTI ITEM:', params, res)
      if (res.status == 1 && res.data && this._isMounted) {
        this.setState({data: res.data});
      }
      return res;
    });
  };

  _getData = () => {
    const {typeObject} = this.props;
    if (typeObject == Contants.TypeObject.ABOUT) {
      return this._getAbout();
    }
    return this._getNotiDetail();
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData();
    if (this._isMounted) {
      this.setState({is_refresh: false});
    }
  };

  hideLoading = () => {
    if (this._isMounted) {
      this.setState({isLoading: false});
    }
  };

  _onError = () => {
    this.hideLoading();
    if (this._isMounted) {
      this.setState({is_error: true});
    }
  };

  _onNoData = async () => {
    this.setState({isLoading: true, is_error: false, firtLoading: true});
    await this._getData();
    if (this._isMounted) {
      this.setState({firtLoading: false});
    }
  };

  render() {
    const {item, typeObject} = this.props;
    const {isLoading, is_error, data, is_refresh, firtLoading} = this.state;
    const title =
      typeObject == Contants.TypeObject.ABOUT
        ? 'Giới thiệu'
        : item && item.title
        ? item.title
        : '---';
    const source =
      data && data.content
        ? {html: Utils.getContentHtml(data.content), baseUrl: 'http://index'}
        : {uri: 'http://index'};
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        title={title}
        onLeftPress={this.goBack}
        visibleError={is_error} // hiển thị màn hình error / webview
        onNoData={this._onNoData}
        footerModal={
          //Chỉ hiển thị loading ở giữa ko ngăn các hành động khác
          <Views.ProgressLoading
            visible={isLoading}
            backgroundColor="transparent"
            style={styles.loading}
            size={SIZE}
          />
        }>
        <ScrollView
          contentContainerStyle={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          {firtLoading ? null : (
            <AutoHeightWebView
              useWebKit
              style={{width: width - 20, margin: 10}}
              source={source}
              scrollEnabled={false}
              originWhitelist={['*']}
              onLoadEnd={this.hideLoading}
              onError={this._onError}
              onShouldStartLoadWithRequest={() => true}
              viewportContent={'width=device-width, user-scalable=no'}
            />
          )}
        </ScrollView>
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(About);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    top: (height - SIZE) / 2,
    left: (width - SIZE) / 2,
  },
});
