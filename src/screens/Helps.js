import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, Dimensions, Platform} from 'react-native';

import {connect} from 'react-redux';
import * as Views from '../views/ViewManagers';
import FastImage from 'react-native-fast-image';
import * as Contants from '../config/Constants';
import * as Icons from '../config/IconManagers';
import * as Routers from '../navigations/Navigations';
import {logout} from '../redux/actions/loginAction';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import * as Apis from '../networking/Apis';

const {width} = Dimensions.get('window');
const STATUS_HEIGHT = getStatusBarHeight(true);
const menu = [
  {icon: Icons.info, title: 'Giới thiệu'},
  {icon: Icons.noti, title: 'Thông báo'},
  {icon: Icons.cup, title: 'Tin tức'},
  {icon: Icons.user, title: 'Tài khoản'},
  // { icon: Icons.password, title: 'Đổi mật khẩu' },
  // { icon: Icons.exit, title: 'Đăng xuất' },
];
const HEIGHT_IMAGE = 500;
const WIDTH_IMAGE = 750;
const WIDTH_COVER = (3 * width) / 4;
const HEIGHT_COVER = (HEIGHT_IMAGE * WIDTH_COVER) / WIDTH_IMAGE;

class Helps extends Component {
  state = {
    menus: [],
  };

  async componentDidMount() {
    const res = await Apis.getMenu();
    console.log('res', res.data);
    const updateMenus =
      res?.data?.map?.(item => {
        return {
          ...item,
          icon: item.icon,
        };
      }) || [];
    this.setState({menus: [...menu, ...updateMenus]});
    // console.log([...menu, ...updateMenus])
  }

  _onMenu = item => {
    const check = item.type || item.title;
    switch (check) {
      case 'Giới thiệu':
        Routers.goAbout({typeObject: Contants.TypeObject.ABOUT});
        break;
      case 'Thông báo':
        Routers.goNoti();
        break;
      case 'Tin tức':
        Routers.goNews();
        break;
      case 'Tài khoản':
        Routers.goProfile();
        break;
      case 'Đổi mật khẩu':
        Routers.goChangePassword();
        // Routers.goFilePDF()
        break;
      case 'Đăng xuất':
        logout();
        break;
      case 1: // danh muc
        if (
          item.title === 'Thi công chống thấm trần nhà' ||
          item.title === 'Thi công chống thấm chuyên nghiệp'
        ) {
          Routers.goDetail({item: item, typeObject: 'SERVICE'});
        } else {
          Routers.goProducts({item: item});
        }
        break;
      case 2: // trang chi tiet
        console.log('goDetail', check);
        Routers.goDetail({item: item, typeObject: item.type_cate});
        break;
      default:
        break;
    }
    this._handleDrawer();
  };

  _handleDrawer = () => {
    if (Platform.OS == 'ios') {
      setTimeout(() => {
        Routers.showMenu(false);
      }, 50);
    } else {
      Routers.showMenu(false);
    }
  };

  _getMenu = () => {
    const {menus} = this.state;
    const {is_login} = this.props.loginState;
    return is_login ? menus : menus.filter(e => e.title != 'Tài khoản');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.status} />

        <ScrollView>
          <FastImage source={Icons.banner} style={styles.banner} />

          {this._getMenu().map((item, index) => (
            <Views.ItemHelps
              key={index}
              item={item}
              index={index}
              onPress={this._onMenu}
            />
          ))}

          <View style={{height: 10}} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Helps);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  banner: {
    width: WIDTH_COVER,
    height: HEIGHT_COVER,
  },
  info: {
    marginTop: 10,
    marginHorizontal: 10,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    borderRadius: 5,
  },
  status: {
    backgroundColor: '#d4d4d4',
    height: STATUS_HEIGHT,
  },
});
