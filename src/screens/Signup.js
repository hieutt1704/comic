import React, {Component} from 'react';
import {Text, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';

import * as Utils from '../Utils';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Views from '../views/ViewManagers';
import {fontSize} from '../modules/fontSize';
import * as Routers from '../navigations/Navigations';
import LinearGradient from 'react-native-linear-gradient';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
    };
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _onSignup = async () => {
    const {phone} = this.state;
    if (!phone) {
      Utils.showToast('Chưa nhập số điện thoại');
    } else {
      const params = `?sdt=${phone}`;
      if (this.ProgressLoading) {
        this.ProgressLoading.show();
      }
      Apis.signup(params).then(res => {
        console.log('Signup', res);
        if (this.ProgressLoading) {
          this.ProgressLoading.hide();
        }
        if (res.status == 1) {
          Utils.showToast(
            'Đăng ký thành công, hệ thống sẽ gửi mật khẩu về số điện thoại của bạn',
          );
          this._goBack();
        } else {
          Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      });
    }
  };

  render() {
    const {phone} = this.state;
    return (
      <Views.BaseView
        title="Đăng ký"
        onLeftPress={this._goBack}
        footerModal={
          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor="#00000030"
          />
        }>
        <ScrollView style={{flex: 1}}>
          <Views.InputView
            ref={ref => (this.PhoneNumber = ref)}
            value={phone}
            formText={true}
            placeholder="Số điện thoại"
            styleContainer={styles.inputView}
            styleForm={styles.form}
            keyboardType="phone-pad"
            onSubmitEditing={this._onSignup}
            onChangeText={phone => this.setState({phone})}
            style={styles.textinput}
          />

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.button}
            onPress={this._onSignup}>
            <LinearGradient
              colors={['#a11e23', '#e91e24']}
              style={styles.gradient}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}>
              <Text style={[styles.text, {fontSize: fontSize + 2}]}>
                {'Đăng ký'.toUpperCase()}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </ScrollView>
      </Views.BaseView>
    );
  }
}

export default Signup;

const styles = StyleSheet.create({
  inputView: {
    marginHorizontal: 25,
    marginTop: 25,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 0.3,
    padding: 10,
    height: 45,
  },
  button: {
    marginHorizontal: 70,
    marginTop: 30,
    marginBottom: 10,
    height: 50,
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 25,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  textinput: {
    flex: 1,
    fontFamily: Fonts.myriadpro_regular,
    fontSize: 18,
  },
});
