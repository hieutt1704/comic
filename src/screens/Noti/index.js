import React, {Component, createRef} from 'react';
import {Text, View, StyleSheet, FlatList} from 'react-native';

import {connect} from 'react-redux';
import * as Fonts from '../../config/Fonts';
import * as Apis from '../../networking/Apis';
import * as Contants from '../../config/Constants';
import * as Views from '../../views/ViewManagers';
import * as Storage from '../../modules/asyncStorge';
import * as Routers from '../../navigations/Navigations';

import Menu from './Menu';

class Noti extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      noti_sales: [],
      noti_products: [],
      noti_services: [],
      noti_news: [],
      sales: [],
      page: 0,
      is_refresh: false,
      end_page: false,
    };
  }

  is_load_more = createRef();

  componentDidMount = async () => {
    if (this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    const data_sales = await Storage.getStorageData(Storage.sales);
    const data_noti_sales = await Storage.getStorageData(Storage.noti_sales);
    const data_noti_products = await Storage.getStorageData(
      Storage.noti_products,
    );
    const data_noti_services = await Storage.getStorageData(
      Storage.noti_services,
    );
    const data_noti_news = await Storage.getStorageData(Storage.noti_news);
    if (data_sales && this._mounted) {
      this.setState({sales: JSON.parse(data_sales)});
    }
    if (data_noti_sales && this._mounted) {
      this.setState({noti_sales: JSON.parse(data_noti_sales)});
    }
    if (data_noti_products && this._mounted) {
      this.setState({noti_products: JSON.parse(data_noti_products)});
    }
    if (data_noti_services && this._mounted) {
      this.setState({noti_services: JSON.parse(data_noti_services)});
    }
    if (data_noti_news && this._mounted) {
      this.setState({noti_news: JSON.parse(data_noti_news)});
    }
    const {sales, noti_news, noti_products, noti_sales, noti_services} =
      this.state;
    if (
      sales.length > 0 ||
      noti_news.length > 0 ||
      noti_products.length > 0 ||
      noti_sales.length > 0 ||
      noti_services.length > 0
    ) {
      this.BaseView.hideLoadCenter();
    }
    await Promise.all[
      (
        this._getSales(),
      this._getNoti(0),
      this._getNoti(1),
      this._getNoti(2),
      this._getNoti(3)
    )
    ];
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
    
  };

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  _getSales = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&page=0`;
    return Apis.getSales(params).then(res => {
      // console.log('getSales', params, res)
      if (res.status == 1 && res.data) {
        Storage.setStorageData(Storage.sales, JSON.stringify(res.data.sales));
        if (this._mounted) {
          this.setState({
            page: 0,
            sales: res.data.sales,
            end_page: res.data.sales.length < res.data.size,
          });
        }
      }
      return res;
    });
  };

  _getNoti = type => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&type=${type}&page=0`;
    return Apis.getNoti(params).then(res => {
      // console.log('getNoti ' + type, params, res)
      if (res.status == 1 && res.data) {
        if (type == 0) {
          Storage.setStorageData(
            Storage.noti_sales,
            JSON.stringify(res.data.notis),
          );
          if (this._mounted) {
            this.setState({noti_sales: res.data.notis});
          }
        }
        if (type == 1) {
          Storage.setStorageData(
            Storage.noti_products,
            JSON.stringify(res.data.notis),
          );
          if (this._mounted) {
            this.setState({noti_products: res.data.notis});
          }
        }
        if (type == 2) {
          Storage.setStorageData(
            Storage.noti_services,
            JSON.stringify(res.data.notis),
          );
          if (this._mounted) {
            this.setState({noti_services: res.data.notis});
          }
        }
        if (type == 3) {
          Storage.setStorageData(
            Storage.noti_news,
            JSON.stringify(res.data.notis),
          );
          if (this._mounted) {
            this.setState({noti_news: res.data.notis});
          }
        }
      }
      return res;
    });
  };

  _onRefresh = async () => {
    console.log("====<>11111111")
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await Promise.all[
      (this._getSales(),
      this._getNoti(0),
      this._getNoti(1),
      this._getNoti(2),
      this._getNoti(3))
    ];
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };
  _onLoadMore = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const {end_page, sales, page} = this.state;
    if (end_page || this.is_load_more.current) {
      return;
    }
    const params = `?tk=${username}&token=${token}&page=${page + 1}`;
    this.is_load_more.current = true
  
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getSales(params).then(res => {
          this.is_load_more.current = false
          if (res.status == 1 && res.data && this._mounted) {
            this.setState({
              page: page + 1,
              sales: [...sales, ...res.data.sales],
              end_page: res.data.sales.length < res.data.size,
            });
          }
        });
      }, 500);
    
  };

  _onPressItemMenu = index => {
    const {noti_news, noti_products, noti_sales, noti_services} = this.state;
    const notis =
      index == 0
        ? noti_sales
        : index == 1
        ? noti_products
        : index == 2
        ? noti_services
        : noti_news;
    Routers.goNotiDetail({type: index, notis});
  };

  _onPressItemSale = item => {
    Routers.goDetail({item, typeObject: Contants.TypeObject.PRODUCT});
  };


  render() {
    const {
      sales,
      is_refresh,
      is_load_more,
      noti_news,
      noti_products,
      noti_sales,
      noti_services,
    } = this.state;
console.log("====>111111111", noti_news, noti_products, noti_sales)
    return (
      <Views.BaseView
        title={'Thông báo'}
        onLeftPress={this._goBack}
        ref={ref => (this.BaseView = ref)}>
        <FlatList
          data={sales}
          numColumns={2}
          initialNumToRender={2}
          refreshing={is_refresh}
          onRefresh={this._onRefresh}
          keyExtractor={(item, index) => index + ''}
          ListHeaderComponent={ () => 
            <View>
              <Menu
                noti_news={noti_news}
                noti_products={noti_products}
                noti_sales={noti_sales}
                noti_services={noti_services}
                onPress={this._onPressItemMenu}
              />

              <View style={styles.dash} />

              <Text style={styles.title}>Chương trình khuyến mại</Text>

              <View style={{height: 10}} />

              {sales.length == 0 ? (
                <Text style={styles.no}>Chưa có trương trình nào</Text>
              ) : null}
            </View>
          }
          renderItem={({item, index}) => (
            <Views.ItemProduct
              index={index}
              item={item}
              search={true}
              onPress={this._onPressItemSale}
            />
          )}
          ItemSeparatorComponent={() => <View style={{width: 5, height: 5}} />}
          onEndReached={this._onLoadMore}
          onEndReachedThreshold={0.2}
          ListFooterComponent={
             this.is_load_more.current ? (
              <Views.ProgressLoading
                size={40}
                visible={ this.is_load_more.current }
                typeLoading={Contants.TypeLoading.LOAD_MORE}
              />
            ) : (
              <View style={{height: 10}} />
            )
          }
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Noti);

const styles = StyleSheet.create({
  dash: {
    backgroundColor: '#d4d4d4',
    height: 7.5,
    width: '100%',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 30,
    color: 'black',
    marginVertical: 10,
    marginTop: 20,
    fontFamily: Fonts.myriadpro_regular,
  },
  no: {
    color: '#606060',
    fontSize: 17,
    marginLeft: 30,
    fontFamily: Fonts.myriadpro_regular,
  },
});
