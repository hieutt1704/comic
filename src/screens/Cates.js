import React, { Component } from 'react';
import { SectionList, StyleSheet, Text, Dimensions, View, ScrollView, RefreshControl } from 'react-native';

import * as Utils from '../Utils';
import { connect } from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Colors from '../config/Colors';
import * as Apis from '../networking/Apis';
import * as fireBaseApi from '../networking/firebaseApi';
import * as Views from '../views/ViewManagers';
import * as Icons from '../config/IconManagers';
import * as Storage from '../modules/asyncStorge';
import { TypeLoading } from '../config/Constants';
import * as Routers from '../navigations/Navigations';
import HomeProduct from '../screens/Home/HomeProduct';
import FakeScreen from './FakeScreen';

const { width } = Dimensions.get('window');

class Cates extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      is_refresh: false,
      is_load_more: false,
      end_page: true,
      cates: [],
      page: 0,
      flag: false
    };
  }

  componentDidMount = async () => {
    const data_cates = await Storage.getStorageData(Storage.cates);
    if (data_cates && this._mounted) {
      this.setState({ cates: JSON.parse(data_cates) });
    }
    if (this.state.cates.length == 0 && this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    await this._getFlag();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  _getFlag = async () => {
    try {
      const flag = await fireBaseApi.getFlag();
      flag ?
        this.setState({
          flag: flag
        })
        : await this._getData();
    } catch (error) {
        this._getData();
    }
  }

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeout);
  }

  _getData = showFail => {
    const { loginState } = this.props;
    const { token, username } = loginState;
    const params = `?tk=${username}&token=${token}&page=0`;
    return Apis.getCates(params).then(res => {
      console.log('get cates:', params, res);
      if (res.status == 1 && res.data && res.data.cates) {
        const data = res.data.cates.map(e => {
          return {
            ...e,
            data: e.data || [],
          };
        });
        Storage.setStorageData(Storage.cates, JSON.stringify(data));
        if (this._mounted) {
          this.setState({
            page: 0,
            cates: res.data.cates.map(e => {
              return {
                ...e,
                data: e.data || [],
              };
            }),
            end_page: res.data.cates.length < res.data.size,
          });
        }
      } else if (showFail) {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({ is_refresh: true });
    await this._getData(true);
    if (this._mounted) {
      this.setState({ is_refresh: false });
    }
  };

  _showCatesDetail = (item, section) => {
    Routers.goProducts({ item, cate_details: section });
  };

  _onMenu = () => {
    Routers.showMenu(true);
  };

  _goSearch = () => {
    Routers.goSearch();
  };

  _onLoadMore = () => {
    const { loginState } = this.props;
    const { token, username } = loginState;
    const { is_load_more, end_page, cates, page } = this.state;


    const params = `?tk=${username}&token=${token}&page=${page + 1}`;
    if (end_page || is_load_more) {
      return;
    }
    this.setState({ is_load_more: true }, () => {
      this.loadmoreTimeout = setTimeout(() => {
        Apis.getCates(params).then(res => {
          // console.log(res, "get more cates", params)
          if (this._mounted) {
            this.setState({ is_load_more: false });
          }
          if (res.status == 1 && res.data && res.data.cates) {
            const data = res.data.cates.map(e => {
              return {
                ...e,
                data: e.data || [],
              };
            });
            this.setState({
              page: page + 1,
              cates: [...cates, ...data],
              end_page: res.data.cates.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  render() {
    const { is_refresh, cates, is_load_more, flag } = this.state;
    const lastSection =
      cates && cates.length - 1 >= 0 ? cates[cates.length - 1] : {};
    return (
      flag ?
        <FakeScreen isProduct={true}/>
        :
        <Views.BaseView
          ref={ref => (this.BaseView = ref)}
          title="Sản phẩm"
          logo={Icons.logo}
          rightIcon={Icons.search}
          leftIcon={Icons.menu}
          onLeftPress={this._onMenu}
          onRightPress={this._goSearch}
          data={cates}
        >
          <SectionList
            style={styles.container}
            sections={cates}
            initialNumToRender={1}
            refreshing={is_refresh}
            onRefresh={this._onRefresh}
            keyExtractor={(item, index) => index + ''}
            ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
            renderSectionFooter={({ section }) =>
              section.title == lastSection.title ? (
                <View style={{ height: 10 }} />
              ) : (
                <View style={styles.dash} />
              )
            }
            renderItem={({ item, index, section }) => (
              <Views.ItemCate
                index={index}
                item={item}
                onPress={() => this._showCatesDetail(item, section)}
              />
            )}
            renderSectionHeader={({ section: { title } }) => (
              <Text style={styles.title}>{title.toUpperCase()}</Text>
            )}
            onEndReached={this._onLoadMore}
            onEndReachedThreshold={0.2}
            ListFooterComponent={
              <Views.ProgressLoading
                size={40}
                backgroundColor="white"
                visible={is_load_more}
                typeLoading={TypeLoading.LOAD_MORE}
              />
            }
          />
        </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Cates);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 10,
    fontSize: 18,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  dash: {
    height: 7.5,
    backgroundColor: Colors.silverCONMIK,
    width: width - 20,
    marginTop: 20,
    marginLeft: 10,
  },
});
