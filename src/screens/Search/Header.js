import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Keyboard} from 'react-native';

import * as Colors from '../../config/Colors';
import * as Views from '../../views/ViewManagers';
import * as Icons from '../../config/IconManagers';

export default class Header extends React.PureComponent {
  _onSearch = () => {
    Keyboard.dismiss();
    if (this.props.onSearch) {
      this.props.onSearch();
    }
  };

  _goBack = () => {
    Keyboard.dismiss();
    if (this.props.goBack) {
      this.props.goBack();
    }
  };

  render() {
    const {value, onDeleteIcon, onChangeText} = this.props;
    return (
      <View style={styles.header}>
        <Views.InputView
          value={value}
          formText={true}
          rightIcon={Icons.search}
          styleContainer={{flex: 1}}
          styleForm={styles.form}
          placeholder="Tìm kiếm"
          returnKeyType="go"
          style={styles.textinput}
          showDeleteIcon={true}
          onDeleteIcon={onDeleteIcon}
          onChangeText={onChangeText}
          onSubmitEditing={this._onSearch}
          onRightIcon={this._onSearch}
        />

        <TouchableOpacity
          onPress={this._goBack}
          activeOpacity={0.7}
          style={styles.out}>
          <Text style={styles.text}>Thoát</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderColor: 'grey',
    borderBottomWidth: 0.3,
  },
  form: {
    height: 40,
    backgroundColor: '#f2f2f2',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    marginLeft: 15,
    paddingHorizontal: 15,
  },
  out: {
    marginHorizontal: 15,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: Colors.redMDC,
    fontSize: 16,
    fontWeight: '500',
    // fontFamily: Fonts.myriadpro_regular,
  },
  textinput: {
    flex: 1,
    fontSize: 16,
    color: 'black',
    // fontFamily: Fonts.myriadpro_regular,
  },
});
