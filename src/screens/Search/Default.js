import React from 'react';
import {Text, View, StyleSheet, FlatList} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Views from '../../views/ViewManagers';
import * as Constants from '../../config/Constants';

import TopSearch from './TopSearch';

class Default extends React.PureComponent {
  render() {
    const {
      keywords,
      populars,
      end_page_keywords,
      is_load_more_keywords,
      is_load_more_populars,
      is_refresh,
      onRefresh,
      onPressItemKeyword,
      onPressItemPopular,
      onLoadMoreSearch,
      onLoadMorePopular,
    } = this.props;
    return (
      <FlatList
        data={populars}
        numColumns={2}
        initialNumToRender={2}
        refreshing={is_refresh}
        onRefresh={onRefresh}
        keyExtractor={(item, index) => index + ''}
        ListHeaderComponent={
          <View>
            <TopSearch
              keywords={keywords}
              end_page={end_page_keywords}
              is_load_more={is_load_more_keywords}
              onPressItem={onPressItemKeyword}
              onLoadMore={onLoadMoreSearch}
            />

            <View style={styles.dash} />

            <Text style={styles.title}>Tìm kiếm phổ biến</Text>

            <View style={{height: 10}} />

            {populars.length == 0 ? (
              <Text style={styles.no}>Chưa có tìm kiếm nào</Text>
            ) : null}
          </View>
        }
        renderItem={({item, index}) => (
          <Views.ItemProduct
            index={index}
            item={item}
            search={true}
            onPress={onPressItemPopular}
          />
        )}
        ItemSeparatorComponent={() => <View style={{width: 5, height: 5}} />}
        onEndReached={onLoadMorePopular}
        onEndReachedThreshold={0.2}
        ListFooterComponent={
          is_load_more_populars ? (
            <Views.ProgressLoading
              size={40}
              visible={is_load_more_populars}
              typeLoading={Constants.TypeLoading.LOAD_MORE}
            />
          ) : (
            <View style={{height: 10}} />
          )
        }
      />
    );
  }
}

export default Default;

const styles = StyleSheet.create({
  dash: {
    backgroundColor: '#d4d4d4',
    height: 7.5,
    width: '100%',
  },
  title: {
    fontSize: 18,
    // fontWeight: 'bold',
    marginLeft: 30,
    color: 'black',
    marginVertical: 10,
    marginTop: 20,
    fontFamily: Fonts.myriadpro_semibold,
  },
  no: {
    color: '#606060',
    fontSize: 17,
    marginLeft: 30,
    fontFamily: Fonts.myriadpro_regular,
  },
});
