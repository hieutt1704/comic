import React, {Component} from 'react';

import {connect} from 'react-redux';
import * as Apis from '../../networking/Apis';
import * as Views from '../../views/ViewManagers';
import * as Contants from '../../config/Constants';
import * as Storage from '../../modules/asyncStorge';
import * as Routers from '../../navigations/Navigations';

import Header from './Header';
import Result from './Result';
import Default from './Default';

class Search extends Component {
  _mounted = true;
  _params = '';
  constructor(props) {
    super(props);
    this.state = {
      page_key: 0,
      page_popular: 0,
      page_result: 0,
      keyword: '',
      is_result: false,
      keywords: [],
      populars: [],
      results: [],
      is_refresh: false,
      is_load_more_keywords: false,
      end_page_keywords: false,
      is_load_more_populars: false,
      end_page_populars: true,
      is_load_more_results: false,
      end_page_results: true,
    };
  }

  componentDidMount = async () => {
    if (this.BaseView) {
      this.BaseView.showLoadCenter();
    }
    const data_keywords = await Storage.getStorageData(Storage.keywords);
    const data_populars = await Storage.getStorageData(Storage.populars);
    if (data_keywords) {
      this.setState({keywords: JSON.parse(data_keywords)});
    }
    if (data_populars) {
      this.setState({populars: JSON.parse(data_populars)});
    }
    if (this.state.keywords.length > 0 || this.state.populars.length > 0) {
      this.BaseView.hideLoadCenter();
    }
    await Promise.all[(this._getTopKeywords(), this._getSearchPopular())];
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  componentWillUnmount() {
    this._mounted = false;
    clearTimeout(this.loadmoreTimeoutKeyword);
    clearTimeout(this.loadmoreTimeoutResult);
    clearTimeout(this.loadmoreTimeoutPopular);
  }

  _goBack = () => {
    if (this.state.is_result) {
      this.setState({is_result: false});
    } else {
      Routers.goBack(this.props.componentId);
    }
  };

  _getTopKeywords = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&page=0`;
    return Apis.getKeywords(params).then(res => {
      // console.log('getKeyword', params, res)
      if (res.status == 1 && res.data && res.data.keywords) {
        Storage.setStorageData(
          Storage.keywords,
          JSON.stringify(res.data.keywords),
        );
        if (this._mounted) {
          this.setState({
            page_key: 0,
            keywords: res.data.keywords,
            end_page_keywords: res.data.keywords.length < res.data.size,
          });
        }
      }
      return res;
    });
  };

  _getSearchPopular = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const params = `?token=${token}&tk=${username}&page=0`;
    return Apis.getSearchPopular(params).then(res => {
      // console.log('getSearchPopular', params, res)
      if (res.status == 1 && res.data && res.data.populars) {
        Storage.setStorageData(
          Storage.populars,
          JSON.stringify(res.data.populars),
        );
        if (this._mounted) {
          this.setState({
            page_popular: 0,
            populars: res.data.populars,
            end_page_populars: res.data.populars.length < res.data.size,
          });
        }
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    if (this.state.is_result) {
      await this._searchKeyword();
    } else {
      await Promise.all[(this._getTopKeywords(), this._getSearchPopular())];
    }
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _loadmoreTopSearch = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const {is_load_more_keywords, end_page_keywords, keywords, page_key} =
      this.state;
    if (end_page_keywords || is_load_more_keywords) {
      return;
    }
    const params = `?tk=${username}&token=${token}&page=${page_key + 1}`;
    this.setState({is_load_more_keywords: true}, () => {
      this.loadmoreTimeoutKeyword = setTimeout(() => {
        Apis.getKeywords(params).then(res => {
          // console.log(res, "get more Keyword", params)
          if (this._mounted) {
            this.setState({is_load_more_keywords: false});
          }
          if (res.status == 1 && res.data && res.data.keywords) {
            this.setState({
              page_key: page_key + 1,
              keywords: [...keywords, ...res.data.keywords],
              end_page_keywords: res.data.keywords.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _loadmorePopular = () => {
    const {loginState} = this.props;
    const {token, username} = loginState;
    const {is_load_more_populars, end_page_populars, populars, page_popular} =
      this.state;
    if (end_page_populars || is_load_more_populars) {
      return;
    }
    const params = `?tk=${username}&token=${token}&page=${page_popular + 1}`;
    this.setState({is_load_more_populars: true}, () => {
      this.loadmoreTimeoutPopular = setTimeout(() => {
        Apis.getSearchPopular(params).then(res => {
          // console.log(res, "get more getSearchPopular", params)
          if (this._mounted) {
            this.setState({is_load_more_populars: false});
          }
          if (res.status == 1 && res.data && res.data.populars) {
            this.setState({
              page_popular: page_popular + 1,
              populars: [...populars, ...res.data.populars],
              end_page_populars: res.data.populars.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  _onPressKeyword = item => {
    this.setState({keyword: item.title});
  };

  _goDetail = item => {
    switch (item.type) {
      case 'product':
        Routers.goDetail({item, typeObject: Contants.TypeObject.PRODUCT});
        break;
      case 'service':
        Routers.goDetail({item, typeObject: Contants.TypeObject.SERVICE});
        break;
      case 'news':
        Routers.goNewDetail({item});
        break;
      default:
        break;
    }
  };

  _onSearch = async () => {
    if (!this.state.keyword) {
      return;
    }
    this.setState({is_result: true});
    const {keyword} = this.state;
    const {loginState} = this.props;
    const {token, username} = loginState;
    this._params = `?token=${token}&tk=${username}&kw=${keyword}`;
    this.BaseView.showLoadCenter();
    await this._searchKeyword();
    if (this.BaseView) {
      this.BaseView.hideLoadCenter();
    }
  };

  _searchKeyword = () => {
    const params = `${this._params}&page=0`;
    return Apis.searchKeywords(params).then(res => {
      // console.log('search KeyWord', params, res)
      if (res.status == 1 && res.data && res.data.results) {
        if (this._mounted) {
          this.setState({
            page_result: 0,
            results: res.data.results,
            end_page_results: res.data.results.length < res.data.size,
          });
        }
      }
      return res;
    });
  };

  _loadmoreResult = () => {
    const {is_load_more_results, end_page_results, results, page_result} =
      this.state;
    if (end_page_results || is_load_more_results) {
      return;
    }
    const params = `${this._params}&page=${page_result + 1}`;
    this.setState({is_load_more_results: true}, () => {
      this.loadmoreTimeoutResult = setTimeout(() => {
        Apis.searchKeywords(params).then(res => {
          // console.log(res, "get more result:", params)
          if (this._mounted) {
            this.setState({is_load_more_results: false});
          }
          if (res.status == 1 && res.data && res.data.results) {
            this.setState({
              page_result: page_result + 1,
              results: [...results, ...res.data.results],
              end_page_results: res.data.results.length < res.data.size,
            });
          }
        });
      }, 500);
    });
  };

  render() {
    const {keyword, is_result, is_refresh, is_load_more_results, results} =
      this.state;
    const data = is_result ? results : null;
    return (
      <Views.BaseView
        ref={ref => (this.BaseView = ref)}
        headerView={
          <Header
            value={keyword}
            goBack={this._goBack}
            onSearch={this._onSearch}
            onDeleteIcon={() => this.setState({keyword: ''})}
            onChangeText={keyword => this.setState({keyword})}
          />
        }
        data={data}
        onNoData={this._onSearch}>
        {is_result ? (
          <Result
            results={results}
            is_refresh={is_refresh}
            is_load_more={is_load_more_results}
            onRefresh={this._onRefresh}
            onLoadMore={this._loadmoreResult}
            onPressItem={this._goDetail}
          />
        ) : (
          <Default
            {...this.state}
            onRefresh={this._onRefresh}
            onPressItemKeyword={this._onPressKeyword}
            onPressItemPopular={this._goDetail}
            onLoadMoreSearch={this._loadmoreTopSearch}
            onLoadMorePopular={this._loadmorePopular}
          />
        )}
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(Search);
