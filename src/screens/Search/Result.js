import React from 'react';
import {View, FlatList} from 'react-native';

import * as Views from '../../views/ViewManagers';
import * as Constants from '../../config/Constants';

class Result extends React.PureComponent {
  render() {
    const {
      results,
      is_load_more,
      is_refresh,
      onRefresh,
      onPressItem,
      onLoadMore,
    } = this.props;
    return (
      <FlatList
        data={results}
        numColumns={2}
        initialNumToRender={2}
        refreshing={is_refresh}
        onRefresh={onRefresh}
        keyExtractor={(item, index) => index + ''}
        ListHeaderComponent={<View style={{height: 10}} />}
        renderItem={({item, index}) => (
          <Views.ItemProduct
            search={true}
            index={index}
            item={item}
            onPress={onPressItem}
          />
        )}
        ItemSeparatorComponent={() => <View style={{width: 5, height: 5}} />}
        onEndReached={onLoadMore}
        onEndReachedThreshold={0.2}
        ListFooterComponent={
          <View style={{marginBottom: 10}}>
            <Views.ProgressLoading
              size={40}
              visible={is_load_more}
              typeLoading={Constants.TypeLoading.LOAD_MORE}
            />
          </View>
        }
      />
    );
  }
}

export default Result;
