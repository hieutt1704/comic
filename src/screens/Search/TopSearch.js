import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Views from '../../views/ViewManagers';
import * as Constants from '../../config/Constants';

export default class TopSearch extends React.PureComponent {
  render() {
    const {keywords, onPressItem, onLoadMore, is_load_more, end_page} =
      this.props;
    return (
      <View style={{marginBottom: 10}}>
        <View style={styles.dash} />

        {/* <Text style={styles.title}>Từ khoá phổ biến</Text> */}

        {keywords.length > 0 ? (
          keywords.map((item, index) => (
            <TouchableOpacity
              key={index}
              activeOpacity={0.7}
              style={styles.vKey}
              onPress={() => onPressItem(item)}>
              <Text style={styles.text}>{item.title}</Text>
            </TouchableOpacity>
          ))
        ) : (
          <Text style={styles.no}>{'Chưa có từ khoá nào'}</Text>
        )}

        {end_page ? null : is_load_more ? (
          <Views.ProgressLoading
            size={40}
            visible={is_load_more}
            typeLoading={Constants.TypeLoading.LOAD_MORE}
          />
        ) : (
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.more}
            onPress={onLoadMore}>
            <Text style={styles.moreText}>Hiển thị nhiều hơn</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dash: {
    backgroundColor: '#d4d4d4',
    height: 7.5,
    width: '100%',
  },
  vKey: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderColor: Colors.greyCONMIK,
    borderBottomWidth: 0.5,
  },
  more: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    marginBottom: 10,
  },
  title: {
    fontSize: 18,
    // fontWeight: 'bold',
    marginLeft: 30,
    color: 'black',
    marginVertical: 10,
    marginTop: 20,
    fontFamily: Fonts.myriadpro_semibold,
  },
  moreText: {
    color: '#606060',
    fontSize: 17,
    fontFamily: Fonts.myriadpro_regular,
  },
  no: {
    color: '#606060',
    fontSize: 17,
    marginLeft: 30,
    marginVertical: 10,
    fontFamily: Fonts.myriadpro_regular,
  },
  text: {
    fontSize: 17,
    color: 'black',
    fontFamily: Fonts.myriadpro_regular,
  },
});
