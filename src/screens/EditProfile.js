import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Platform,
  Keyboard,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import moment from 'moment';
import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Apis from '../networking/Apis';
import * as Colors from '../config/Colors';
import * as Views from '../views/ViewManagers';
// import FastImage from 'react-native-fast-image';
import * as Icons from '../config/IconManagers';
import {fontSize} from '../modules/fontSize';
import * as Routers from '../navigations/Navigations';
// import DateTimePicker from 'react-native-modal-datetime-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const {width} = Dimensions.get('window');

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.profile = this.props.profile || {};
    const {
      name,
      avatar,
      birthday,
      telephone,
      email,
      address,
      position,
      company,
    } = this.profile;
    this.state = {
      image: {},
      avatar,
      name,
      birthday,
      email,
      address,
      telephone,
      company,
      position,
      isDateTimePickerVisible: false,
    };
  }

  _goBack = () => {
    Keyboard.dismiss();
    Routers.goBack(this.props.componentId);
  };

  _showModalPickImage = () => {
    if (this.PickImageModal) {
      this.PickImageModal.showModal();
    }
  };

  _changeAvatar = async res => {
    const dataImage = await Utils.resizeImage(res);
    const image = {
      uri: dataImage.uri,
      name: dataImage.fileName,
      type: dataImage.type,
    };
    this.setState({avatar: res.uri, image});
  };

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = date => {
    this.setState({birthday: moment(date).format('DD/MM/YYYY')});
    this.hideDateTimePicker();
  };

  _updateAvatar = () => {
    const {token, username} = this.props.loginState;
    const {image} = this.state;
    if (image.uri) {
      const params = `?tk=${username}&token=${token}`;
      const data = new FormData();
      data.append('avatar', image);
      return Apis.updateAvatar(params, data).then(res => {
        // console.log('updateAvartar', res, data)
        return res;
      });
    }
  };

  _updateInfo = () => {
    const {token, username} = this.props.loginState;
    const {birthday, address, name, email, telephone, company, position} =
      this.state;
    const data = {
      tk: username,
      token,
      name,
      birthday,
      address,
      email,
      telephone,
      donvi: company,
      chucdanh: position,
    };
    const params = Utils.convertOjbToParams(data);
    return Apis.updateProfile(params).then(res => {
      console.log('updateINFO', res, params);
      return res;
    });
  };

  _save = async () => {
    if (!this._checkEdit()) {
      return;
    }
    const {image} = this.state;
    this.ProgressLoading.show();
    if (image.uri) {
      if (this._checkEditInfo()) {
        const res = await Promise.all([
          this._updateInfo(),
          this._updateAvatar(),
        ]);
        if (res[0].status != 1 && res[1] && res[1].status != 1) {
          Utils.showToast(res[0].msg || 'Opps, lỗi kết nối với hệ thống');
        } else if (res[0].status != 1) {
          Utils.showToast(res[0].msg || 'Ko cập nhật được thông tin');
          this._updateProfile({...this.profile, avatar: this.state.avatar});
        } else if (res[1] && res[1].status != 1) {
          Utils.showToast(res[1].msg || 'Ko cập nhật được ảnh đại diện');
          this._updateProfile({...this.state, avatar: this.profile.avatar});
        } else {
          this._updateProfile(this.state);
        }
      } else {
        const res = await this._updateAvatar();
        if (res.status == 1) {
          this._updateProfile(this.state);
        } else {
          Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      }
    } else {
      const res = await this._updateInfo();
      if (res.status == 1) {
        this._updateProfile(this.state);
      } else {
        Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
      }
    }
    if (this.ProgressLoading) {
      this.ProgressLoading.hide();
    }
  };

  _updateProfile = profile => {
    this._goBack(this.props.updateProfile(profile));
  };

  _checkEditInfo = () => {
    const {birthday, address, name, email, telephone, position, company} =
      this.profile;
    return (
      birthday != this.state.birthday ||
      telephone != this.state.telephone ||
      address != this.state.address ||
      email != this.state.email ||
      name != this.state.name ||
      position != this.state.position ||
      company != this.state.company
    );
  };

  _checkEdit = () => {
    const {avatar} = this.profile;
    return avatar != this.state.avatar || this._checkEditInfo();
  };

  //file:// content://
  _checkUrl = url => {
    return url.indexOf('http') == 0 ||
      url.indexOf('file://') == 0 ||
      url.indexOf('content://') == 0
      ? {uri: url}
      : {uri: Apis.domain + url};
  };

  render() {
    const {
      isDateTimePickerVisible,
      birthday,
      avatar,
      address,
      name,
      email,
      telephone,
      company,
      position,
    } = this.state;
    const avatar_source = avatar ? this._checkUrl(avatar) : Icons.no_image;
    const birthday_value = birthday || 'Chọn';
    const initDate = birthday ? Utils.toDate(birthday_value) : new Date();
    const isEdit = this._checkEdit();
    const activeOpacityRight = isEdit ? 0.5 : 1;
    const styleEdit = [
      styles.edit,
      isEdit ? {color: '#45709d'} : {color: '#00000030'},
    ];
    return (
      <Views.BaseView
        title="Chỉnh sửa hồ sơ"
        leftText="Huỷ"
        rightText="Xong"
        onLeftPress={this._goBack}
        onRightPress={this._save}
        rightTextStyle={styleEdit}
        activeOpacityRight={activeOpacityRight}
        footerModal={
          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor="#00000030"
          />
        }>
        <KeyboardAwareScrollView
          enableAutomaticScroll
          enableOnAndroid={true}
          style={{flexGrow: 1}}
          extraScrollHeight={Platform.OS == 'ios' ? 0 : 115}>
          <View style={{paddingHorizontal: 10, width}}>
            {/* <View style={styles.vAvatar}>
                            <TouchableOpacity
                                style={styles.viewAvatar}
                                activeOpacity={0.7}
                                // onPress={this._showModalPickImage}
                            >
                                <FastImage
                                    source={avatar_source}
                                    style={styles.avatar}
                                />

                                <View style={styles.vcamera}>
                                    <FastImage
                                        style={styles.camera}
                                        tintColor="black"
                                        source={Icons.ICON_CAMERA}
                                        resizeMode="contain"
                                    />
                                </View>
                            </TouchableOpacity>
                        </View> */}

            <View
              style={{
                flexDirection: 'row',
                width: width - 20,
                marginVertical: 10,
                height: 5,
              }}>
              <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
              <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
              <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
            </View>

            <View style={[styles.info, {marginTop: 10}]}>
              <Text style={styles.text}>Tên</Text>

              <Views.InputView
                ref={ref => (this.Name = ref)}
                value={name}
                formText={true}
                placeholder="Họ và tên"
                returnKeyType={'next'}
                styleContainer={styles.inputView}
                styleForm={styles.form}
                onChangeText={name => this.setState({name})}
                style={styles.textinput}
                onSubmitEditing={() => this.PhoneNumber.focus()}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Số điện thoại</Text>

              <Views.InputView
                ref={ref => (this.PhoneNumber = ref)}
                value={telephone}
                formText={true}
                placeholder="Số điện thoại"
                returnKeyType={'next'}
                styleContainer={styles.inputView}
                styleForm={styles.form}
                keyboardType="phone-pad"
                style={styles.textinput}
                onChangeText={telephone => this.setState({telephone})}
                onSubmitEditing={() => this.Email.focus()}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Email</Text>

              <Views.InputView
                ref={ref => (this.Email = ref)}
                value={email}
                autoCapitalize="none"
                formText={true}
                placeholder="Email"
                returnKeyType={'next'}
                styleContainer={styles.inputView}
                styleForm={styles.form}
                onChangeText={email => this.setState({email})}
                style={styles.textinput}
                onSubmitEditing={this.showDateTimePicker}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Năm sinh</Text>

              <Views.ChooseView
                ref={ref => (this.Birthday = ref)}
                value={birthday_value}
                styleContainer={styles.inputView}
                styleForm={styles.form}
                onPress={this.showDateTimePicker}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Đơn vị</Text>

              <Views.InputView
                ref={ref => (this.Company = ref)}
                value={company}
                formText={true}
                placeholder="Đơn vị"
                returnKeyType={'next'}
                styleContainer={styles.inputView}
                styleForm={[styles.form]}
                onChangeText={company => this.setState({company})}
                onSubmitEditing={() => this.Position.focus()}
                style={styles.textinput}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Chức danh</Text>

              <Views.InputView
                ref={ref => (this.Position = ref)}
                value={position}
                formText={true}
                placeholder="Chức danh"
                returnKeyType={'next'}
                styleContainer={styles.inputView}
                styleForm={[styles.form]}
                onChangeText={position => this.setState({position})}
                onSubmitEditing={() => this.Address.focus()}
                style={styles.textinput}
              />
            </View>

            <View style={styles.info}>
              <Text style={styles.text}>Địa chỉ</Text>

              <Views.InputView
                ref={ref => (this.Address = ref)}
                value={address}
                formText={true}
                placeholder="Địa chỉ"
                returnKeyType={'go'}
                styleContainer={styles.inputView}
                styleForm={[styles.form]}
                onChangeText={address => this.setState({address})}
                onSubmitEditing={this._save}
                style={styles.textinput}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>

        {/* <DateTimePicker
          locale="vi"
          isVisible={isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          titleIOS={'Chọn ngày'}
          confirmTextIOS={'Xác nhận'}
          cancelTextIOS={'Thôi'}
          date={initDate}
        /> */}

        <Views.PickImageModal
          ref={ref => (this.PickImageModal = ref)}
          pickImage={this._changeAvatar}
        />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(EditProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 10,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  inputView: {
    flex: 3,
    marginBottom: 10,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'grey',
    borderBottomWidth: 0.3,
    height: 45,
    paddingRight: 5,
  },
  vAvatar: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewAvatar: {
    overflow: 'hidden',
    marginVertical: 15,
    height: 140,
    width: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor: 'grey',
    shadowOffset: {width: 0, height: 2},
    shadowColor: 'black',
    shadowOpacity: 1,
    elevation: 5,
    borderWidth: 2,
    borderColor: 'white',
  },
  vcamera: {
    position: 'absolute',
    bottom: 15,
    right: 15,
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#DDDDDD',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'white',
    elevation: 5,
  },
  camera: {
    width: 16,
    height: 16,
  },
  text: {
    flex: 1,
    fontSize: 14,
    color: '#606060',
    marginBottom: 8,
    fontFamily: Fonts.utm_avo,
  },
  textinput: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.utm_avo,
  },
  info: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  edit: {
    fontSize,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
});
