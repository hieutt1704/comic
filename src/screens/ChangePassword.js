import React, {Component} from 'react';
import {
  Keyboard,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Colors from '../config/Colors';
import * as Apis from '../networking/Apis';
import {fontSize} from '../modules/fontSize';
import * as Views from '../views/ViewManagers';
import FastImage from 'react-native-fast-image';
import * as Icons from '../config/IconManagers';
import {logout} from '../redux/actions/loginAction';
import * as Routers from '../navigations/Navigations';

const {height, width} = Dimensions.get('window');

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
      new_password: '',
      confirm_password: '',
    };
  }

  _goBack = () => {
    Keyboard.dismiss();
    Routers.goBack(this.props.componentId);
  };

  _changePassword = async () => {
    const {new_password, old_password, confirm_password} = this.state;
    if (!old_password) {
      Utils.showToast('Chưa nhập mật khẩu hiện tại');
    } else if (!new_password) {
      Utils.showToast('Chưa nhập mật khẩu mới');
    } else if (!confirm_password) {
      Utils.showToast('Chưa nhập lại mật khẩu');
    } else if (confirm_password != new_password) {
      Utils.showToast('Mật khẩu xác nhận không trùng mật khẩu mới');
    } else {
      const {loginState} = this.props;
      const params = `?token=${loginState.token}&mkmoi=${new_password}&mkcu=${old_password}`;
      if (this.ProgressLoading) {
        this.ProgressLoading.show();
      }
      Apis.changePassword(params).then(res => {
        // console.log('change pass:', params)
        if (this.ProgressLoading) {
          this.ProgressLoading.hide();
        }
        if (res.status == 1) {
          Utils.showToast('Đổi mật khẩu thành công');
          logout();
        } else {
          Utils.showToast(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      });
    }
  };

  render() {
    const {new_password, old_password, confirm_password} = this.state;
    return (
      <Views.BaseView
        title={'Đổi mật khẩu'}
        onLeftPress={this._goBack}
        footerModal={
          <Views.ProgressLoading ref={ref => (this.ProgressLoading = ref)} />
        }>
        <FastImage
          source={Icons.background}
          style={{width, height: height - 50}}
        />

        <ScrollView style={styles.container}>
          <View style={styles.info}>
            <Views.InputView
              tintColor="white"
              placeholderTextColor="#FFFFFF80"
              icon={Icons.password}
              value={old_password}
              styleContainer={styles.inputView}
              styleForm={styles.form}
              ref={ref => (this.OldPassword = ref)}
              placeholder={'Mật khẩu hiện tại'}
              style={styles.textinput}
              formText={true}
              secureTextEntry={true}
              returnKeyType={'next'}
              autoCapitalize={'none'}
              onSubmitEditing={() => this.NewPassword.focus()}
              onChangeText={old_password => this.setState({old_password})}
            />

            <Views.InputView
              tintColor="white"
              placeholderTextColor="#FFFFFF80"
              icon={Icons.password}
              value={new_password}
              styleContainer={styles.inputView}
              styleForm={styles.form}
              ref={ref => (this.NewPassword = ref)}
              placeholder={'Mật khẩu mới'}
              style={styles.textinput}
              secureTextEntry={true}
              formText={true}
              returnKeyType={'next'}
              autoCapitalize={'none'}
              onSubmitEditing={() => this.ConfirmPassword.focus()}
              onChangeText={new_password => this.setState({new_password})}
            />

            <Views.InputView
              tintColor="white"
              placeholderTextColor="#FFFFFF80"
              icon={Icons.password}
              value={confirm_password}
              styleContainer={styles.inputView}
              styleForm={styles.form}
              ref={ref => (this.ConfirmPassword = ref)}
              placeholder={'Xác nhận mật khẩu'}
              style={styles.textinput}
              secureTextEntry={true}
              formText={true}
              autoCapitalize={'none'}
              onSubmitEditing={this._changePassword}
              onChangeText={confirm_password =>
                this.setState({confirm_password})
              }
            />

            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.button}
              onPress={this._changePassword}>
              <Text style={[styles.text, {fontSize: fontSize + 3}]}>
                {'Xác nhận'.toUpperCase()}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(ChangePassword);

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    left: 0,
    top: 0,
  },
  inputView: {
    marginHorizontal: 15,
    marginTop: 20,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'white',
    borderBottomWidth: 0.5,
    padding: 10,
    height: 45,
  },
  textinput: {
    flex: 1,
    color: 'white',
    fontSize: 17,
    fontFamily: Fonts.myriadpro_regular,
  },
  button: {
    margin: 40,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 50,
  },
  text: {
    fontWeight: 'bold',
    color: Colors.redCONMIK,
  },
  info: {
    margin: 30,
    backgroundColor: '#00000070',
    borderRadius: 25,
    paddingTop: 10,
  },
});
