import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  View,
  Text,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';

import * as Utils from '../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../config/Fonts';
import * as Colors from '../config/Colors';
import * as Apis from '../networking/Apis';
import * as Views from '../views/ViewManagers';
import FastImage from 'react-native-fast-image';
import * as Icons from '../config/IconManagers';
import * as Routers from '../navigations/Navigations';
import AutoHeightWebView from 'react-native-autoheight-webview';

const Entities = require('html-entities').AllHtmlEntities;

const {height, width} = Dimensions.get('window');
const SIZE = 60;
const HEIGHT_IMAGE = (2 * width) / 3;
const WIDTH_WEBVIEW = width - 20;

class NewDetail extends Component {
  _mounted = true;
  constructor(props) {
    super(props);
    const {item} = this.props;
    this.state = {
      item,
      is_refresh: false,
      isShowFile: false,
    };
  }

  componentDidMount = async () => {
    if (this.ProgressLoading) {
      this.ProgressLoading.show();
    }
    await this._getData();
  };

  componentWillUnmount() {
    this._mounted = false;
  }

  _getData = () => {
    const {item, loginState} = this.props;
    const {token, username} = loginState;
    const params = `?tk=${username}&token=${token}&id=${
      item.id
    }&data_type=${'news'}`;
    return Apis.getDetail(params).then(res => {
      console.log('Get detail: ', res, params);
      if (res.status == 1 && res.data) {
        if (this._mounted) {
          this.setState({
            item: {...res.data, color: res.data.color || item.color},
            is_error: false,
          });
        }
        const html = res.data.content ? res.data.content : '';
        if (!html) {
          this.hideLoading();
        }
      } else {
        this.hideLoading();
        Utils.showToast(res.msg || 'Không cập nhật được bài viết chi tiết');
      }
      return res;
    });
  };

  _onRefresh = async () => {
    if (this.state.is_refresh) {
      return;
    }
    this.setState({is_refresh: true});
    await this._getData();
    if (this._mounted) {
      this.setState({is_refresh: false});
    }
  };

  _goBack = () => {
    Routers.goBack(this.props.componentId);
  };

  hideLoading = () => {
    if (this.ProgressLoading) {
      this.ProgressLoading.hide();
    }
    this.setState({isShowFile: true});
  };

  _showImage = url => {
    if (this.ImageModal) {
      this.ImageModal.showModal([{url}]);
    }
  };

  _signup = () => {
    if (this.SignupModal) {
      this.SignupModal.showModal();
    }
  };

  handleWebViewNavigationStateChange = newNavState => {
    const {url} = newNavState;
    if (!url) {
      return;
    }
    // handle certain doctypes
    if (url.indexOf('http') == 0) {
      // this.webview.goForward();
      // this.webview.stopLoading();
      // open a modal with the PDF viewer
    }
  };

  handleWebViewRequest = request => {
    const {url} = request;
    if (url.indexOf('http') == 0 && Platform.OS == 'ios') {
      return false;
    }
    return true;
  };

  render() {
    const {is_login} = this.props.loginState;
    const {is_refresh, item, isShowFile} = this.state;
    const title = item && item.title ? item.title : '---';
    const html = item && item.content ? item.content : '';
    const file_html =
      is_login && item && item.fileurl && item.fileurl != 'null'
        ? item.fileurl
        : '';
    return (
      <Views.BaseView
        title={title}
        onLeftPress={this._goBack}
        footerModal={
          <Views.ProgressLoading
            ref={ref => (this.ProgressLoading = ref)}
            backgroundColor={'transparent'}
            style={styles.loading}
            size={SIZE}
          />
        }
        bottomModal={
          <Views.SignupModal ref={ref => (this.SignupModal = ref)} />
        }>
        <ScrollView
          contentContainerStyle={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={is_refresh}
              onRefresh={this._onRefresh}
            />
          }>
          <Info {...this.props} item={item} onPress={this._showImage} />

          {html ? (
            <AutoHeightWebView
              style={styles.webview}
              source={{
                html: Utils.getContentHtml(html),
                baseUrl: 'http://index',
              }}
              originWhitelist={['*']}
              onLoadEnd={this.hideLoading}
              scrollEnabled={false}
              onShouldStartLoadWithRequest={() => true}
              viewportContent={'width=device-width, user-scalable=no'}
              useWebKit
            />
          ) : null}

          <View
            style={{
              flexDirection: 'row',
              width: width - 20,
              marginHorizontal: 10,
              height: 5,
              marginBottom: 10,
            }}>
            <View style={{backgroundColor: Colors.greenCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.pinkCONMIK, flex: 1}} />
            <View style={{backgroundColor: Colors.orangeCONMIK, flex: 1}} />
          </View>

          {is_login ? (
            file_html || !isShowFile ? (
              <AutoHeightWebView
                useWebKit
                style={styles.webview}
                source={{html: file_html}}
                originWhitelist={['*']}
                scrollEnabled={false}
                viewportContent={'width=device-width, user-scalable=no'}
                onShouldStartLoadWithRequest={this.handleWebViewRequest}
                onNavigationStateChange={
                  this.handleWebViewNavigationStateChange
                }
              />
            ) : (
              <View style={styles.webview}>
                <Text style={styles.textNo}>Không có tài liệu đính kèm</Text>
              </View>
            )
          ) : (
            <View style={styles.viewDownload}>
              <TouchableOpacity
                onPress={this._signup}
                style={styles.buttonDownload}
                activeOpacity={0.7}>
                <FastImage
                  source={Icons.download}
                  style={styles.download}
                  resizeMode="contain"
                />

                <Text style={styles.textDown}>Tải tài liệu</Text>
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>

        <Views.ImageModal ref={ref => (this.ImageModal = ref)} />
      </Views.BaseView>
    );
  }
}

const mapStateToProps = state => ({
  loginState: state.loginState,
});

export default connect(mapStateToProps)(NewDetail);

class Info extends React.PureComponent {
  render() {
    const {item, onPress} = this.props;
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const title = item && item.title ? item.title : '---';
    const description =
      item && item.intro
        ? // item.intro.length > 500 ?
          //     item.intro.slice(0, 499) :
          item.intro
        : '---';
    return (
      <View style={{margin: 10}}>
        <Text style={styles.title}>{title.toUpperCase()}</Text>

        <Text style={styles.content}>{Entities?.decode(description)}</Text>

        <TouchableOpacity activeOpacity={0.7} onPress={() => onPress(image)}>
          <FastImage source={{uri: image}} style={styles.image} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    top: (height - SIZE) / 2,
    left: (width - SIZE) / 2,
  },
  title: {
    marginTop: 10,
    fontSize: 18,
    color: 'black',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
  },
  image: {
    width: width - 20,
    height: HEIGHT_IMAGE,
    backgroundColor: Colors.silverCONMIK,
  },
  content: {
    color: '#606060',
    fontSize: 16,
    marginVertical: 15,
    marginHorizontal: 5,
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
  },
  viewDownload: {
    width,
    alignItems: 'center',
  },
  buttonDownload: {
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  download: {
    width: 16,
    height: 16,
    marginRight: 10,
  },
  textDown: {
    color: 'black',
    fontFamily: Fonts.utm_avo_bold,
    fontSize: 16,
  },
  webview: {
    width: WIDTH_WEBVIEW,
    marginHorizontal: 10,
    marginBottom: 10,
    opacity: 0.99,
  },
  textNo: {
    color: 'grey',
    fontFamily: Fonts.utm_avo_bold,
    fontSize: 16,
  },
});
