import React, {PureComponent} from 'react';
import {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import * as Utils from '../../Utils';
import * as Colors from '../../config/Colors';
import {fontSize} from '../../modules/fontSize';
import InputView from '../common/InputView';

class ItemDrop extends PureComponent {
  render() {
    const {item, pickItemDrop, typeModal, selected, fontWeight} = this.props;
    const color =
      selected && JSON.stringify(item) == JSON.stringify(selected)
        ? 'black'
        : 'grey';
    return (
      <TouchableOpacity
        style={styles.item}
        activeOpacity={0.7}
        onPress={() => pickItemDrop(item, typeModal)}>
        <Text
          style={[styles.text, {fontSize, color, fontWeight}]}
          numberOfLines={2}>
          {item.title || '---'}
        </Text>
      </TouchableOpacity>
    );
  }
}

class DropDownModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      marginTop: 0,
      width: 0,
      marginLeft: 0,
      data: [],
      all_data: [],
      typeModal: '',
      allValue: '',
      selected: {},
    };
  }

  showModal = (
    marginTop,
    width,
    marginLeft,
    array,
    selected,
    typeModal,
    allValue,
  ) => {
    this.setState({
      modalVisible: true,
      marginTop,
      width,
      marginLeft,
      data: array || [],
      all_data: array || [],
      typeModal,
      allValue,
      selected,
    });
    const {placeholder} = this.props;
    if (placeholder) {
      setTimeout(() => {
        if (this.InputView) {
          this.InputView.focus();
        }
      }, 100);
    }
  };

  hideModal = () => {
    this.setState({
      modalVisible: false,
    });
    if (this.props.hideModal) {
      this.props.hideModal();
    }
  };

  _pickItemDrop = (item, typeModal) => {
    this.hideModal();
    if (this.props.pickItemDrop) {
      this.props.pickItemDrop(item, typeModal);
    }
  };

  _onChangeText = text => {
    const {all_data} = this.state;
    this.setState({
      data: all_data.filter(e => {
        if (
          Utils.convertString(e.title).indexOf(Utils.convertString(text)) > -1
        ) {
          return e;
        }
      }),
    });
  };

  _getData = () => {
    const {data} = this.state;
    return data.filter((e, i) => {
      if (i < 50) {
        return e;
      }
    });
  };

  render() {
    const {modalVisible, marginTop, data, width, marginLeft, allValue} =
      this.state;
    const {placeholder, marginBottom} = this.props;
    const opacity = 1;
    const maxHeight = 250;
    const margin_top = marginTop;
    // data.length == 0 ?
    //   marginTop + 200 :
    //   data.length == 1 ?
    //     marginTop + 150 :
    //     data.length == 2 ?
    //       marginTop + 100 :
    //       data.length == 3 ?
    //         marginTop + 50 :
    //         marginTop
    const array = this._getData();
    const animetionType = 'none';
    return (
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={this.hideModal}
        animationType={animetionType}>
        <TouchableOpacity
          activeOpacity={1}
          style={[styles.container, {opacity}]}
          onPress={this.hideModal}>
          <View
            style={[
              styles.modal,
              {
                marginTop: margin_top,
                width,
                marginLeft,
                maxHeight,
                marginBottom,
              },
            ]}>
            {placeholder ? (
              <InputView
                ref={ref => (this.InputView = ref)}
                placeholder={placeholder || ''}
                style={[styles.textinput, {fontSize}]}
                onChangeText={this._onChangeText}
                keyboardType={'email-address'}
                onSubmitEditing={this.hideModal}
                autoCapitalize={'sentences'}
              />
            ) : null}

            <ScrollView keyboardShouldPersistTaps="always">
              {allValue ? (
                <ItemDrop
                  item={{tilte: allValue}}
                  {...this.state}
                  pickItemDrop={this._pickItemDrop}
                />
              ) : null}

              {array.map((item, index) => (
                <ItemDrop
                  key={index}
                  item={item}
                  index={index}
                  {...this.state}
                  {...this.props}
                  pickItemDrop={this._pickItemDrop}
                />
              ))}
            </ScrollView>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

export default DropDownModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(130, 130, 130, 0.44)',
  },
  modal: {
    backgroundColor: 'white',
    borderWidth: 0.5,
    borderColor: 'grey',
  },
  item: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  text: {
    color: 'black',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  textinput: {
    height: 50,
    margin: 5,
    borderWidth: 0.5,
    borderRadius: 5,
    borderColor: Colors.blueMDC,
    paddingHorizontal: 10,
    color: 'black',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
