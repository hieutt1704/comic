import React, {PureComponent} from 'react';
import {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  Platform,
} from 'react-native';

import * as Utils from '../../Utils';
import {connect} from 'react-redux';
import * as Fonts from '../../config/Fonts';
import InputView from '../common/InputView';
import * as Apis from '../../networking/Apis';
import * as Storage from '../../modules/asyncStorge';
import ProgressLoading from '../common/ProgressLoading';
import {changeIsLogin} from '../../redux/actions/loginAction';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const {width, height} = Dimensions.get('window');
const STATUS_HEIGHT = getStatusBarHeight(true);

class SignupModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      name: '',
      email: '',
      address: '',
      telephone: '',
      company: '',
      title: '',
    };
  }

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  hideModal = () => {
    Keyboard.dismiss;
    this.setState({
      modalVisible: false,
    });
  };

  _onSignup = () => {
    const {address, name, email, telephone, company, title} = this.state;
    if (!telephone) {
      Utils.alert('Yêu cầu nhập số điện thoại');
    } else if (!email) {
      Utils.alert('Yêu cầu nhập email');
    } else {
      const params = `?sdt=${telephone}&email=${email}&ten=${name}&donvi=${company}&chucdanh=${title}&diachi=${address}`;
      if (this.ProgressLoading) {
        this.ProgressLoading.show();
      }
      Apis.signup(params).then(res => {
        console.log('signup', res);
        if (this.ProgressLoading) {
          this.ProgressLoading.hide();
        }
        if (res.status == 1 && res.data && res.data.token) {
          const user = {
            username: telephone,
            token: res.data.token,
          };
          Storage.setStorageData(Storage.is_login, JSON.stringify(user));
          this.props.changeIsLogin(user);
          this.hideModal();
        } else {
          Utils.alert(res.msg || 'Opps, lỗi kết nối với hệ thống');
        }
      });
    }
  };

  render() {
    const {modalVisible, address, name, email, telephone, company, title} =
      this.state;
    return (
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={this.hideModal}
        animationType="slide">
        <TouchableOpacity
          activeOpacity={1}
          onPress={this.hideModal}
          style={{height: height + 100, backgroundColor: '#00000030'}}>
          <View style={{height: STATUS_HEIGHT}} />

          <KeyboardAwareScrollView
            enableAutomaticScroll
            enableOnAndroid={true}
            style={{flexGrow: 1}}
            viewIsInsideTabBar={true}
            extraScrollHeight={Platform.OS == 'ios' ? 0 : 115}
            contentContainerStyle={[styles.container]}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={this.hideModal}
              style={styles.container}>
              <View style={[styles.modal, {marginBottom: 2 * STATUS_HEIGHT}]}>
                <View style={styles.header}>
                  <Text style={styles.title}>Đăng ký</Text>
                </View>

                <View style={[styles.info, {marginTop: 10}]}>
                  <Text style={styles.text}>Số điện thoại*</Text>

                  <InputView
                    ref={ref => (this.PhoneNumber = ref)}
                    value={telephone}
                    formText={true}
                    placeholder="Số điện thoại"
                    returnKeyType={'next'}
                    styleContainer={styles.inputView}
                    styleForm={styles.form}
                    keyboardType="phone-pad"
                    style={styles.textinput}
                    onChangeText={telephone => this.setState({telephone})}
                    onSubmitEditing={() => this.Email.focus()}
                  />
                </View>

                <View style={styles.info}>
                  <Text style={styles.text}>Email*</Text>

                  <InputView
                    ref={ref => (this.Email = ref)}
                    value={email}
                    formText={true}
                    autoCapitalize="none"
                    placeholder="Email"
                    returnKeyType={'next'}
                    styleContainer={styles.inputView}
                    styleForm={styles.form}
                    onChangeText={email => this.setState({email})}
                    style={styles.textinput}
                    onSubmitEditing={() => this.Name.focus()}
                  />
                </View>

                <View style={[styles.info, {marginTop: 10}]}>
                  <Text style={styles.text}>Tên</Text>

                  <InputView
                    ref={ref => (this.Name = ref)}
                    value={name}
                    formText={true}
                    placeholder="Họ và tên"
                    returnKeyType={'next'}
                    styleContainer={styles.inputView}
                    styleForm={styles.form}
                    onChangeText={name => this.setState({name})}
                    style={styles.textinput}
                    onSubmitEditing={() => this.Company.focus()}
                  />
                </View>

                <View style={[styles.info, {marginTop: 10}]}>
                  <Text style={styles.text}>Đơn vị</Text>

                  <InputView
                    ref={ref => (this.Company = ref)}
                    value={company}
                    formText={true}
                    placeholder="Đơn vị"
                    returnKeyType={'next'}
                    styleContainer={styles.inputView}
                    styleForm={styles.form}
                    onChangeText={company => this.setState({company})}
                    style={styles.textinput}
                    onSubmitEditing={() => this.Title.focus()}
                  />
                </View>

                <View style={[styles.info, {marginTop: 10}]}>
                  <Text style={styles.text}>Chức danh</Text>

                  <InputView
                    ref={ref => (this.Title = ref)}
                    value={title}
                    formText={true}
                    placeholder="Chức danh"
                    returnKeyType={'next'}
                    styleContainer={styles.inputView}
                    styleForm={styles.form}
                    onChangeText={title => this.setState({title})}
                    style={styles.textinput}
                    onSubmitEditing={() => this.Address.focus()}
                  />
                </View>

                <View style={[styles.info]}>
                  <Text style={styles.text}>Địa chỉ</Text>

                  <InputView
                    ref={ref => (this.Address = ref)}
                    value={address}
                    formText={true}
                    placeholder="Địa chỉ"
                    returnKeyType={'go'}
                    styleContainer={styles.inputView}
                    styleForm={[styles.form]}
                    onChangeText={address => this.setState({address})}
                    onSubmitEditing={this._onSignup}
                    style={styles.textinput}
                  />
                </View>

                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={this._onSignup}
                  style={styles.button}>
                  <Text style={styles.tButton}>Gửi</Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </KeyboardAwareScrollView>

          <View style={{height: 100}} />
        </TouchableOpacity>

        <ProgressLoading
          ref={ref => (this.ProgressLoading = ref)}
          backgroundColor="#00000030"
        />
      </Modal>
    );
  }
}

export default connect(null, {changeIsLogin}, null, {forwardRef: true})(
  SignupModal,
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#00000030'
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 40,
  },
  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'grey',
    borderBottomWidth: 0.3,
    width: width - 80,
  },
  textinput: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.utm_avo,
  },
  info: {
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputView: {
    flex: 3,
    marginBottom: 10,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'grey',
    borderBottomWidth: 0.3,
    height: 45,
    paddingRight: 5,
  },
  text: {
    flex: 1,
    fontSize: 14,
    color: '#606060',
    marginBottom: 8,
    fontFamily: Fonts.utm_avo,
  },
  title: {
    fontSize: 16,
    color: '#606060',
    fontFamily: Fonts.utm_avo_bold,
  },
  button: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
    backgroundColor: '#eb1e25',
    borderRadius: 5,
    paddingHorizontal: 15,
  },
  tButton: {
    color: 'white',
    fontSize: 16,
    fontFamily: Fonts.utm_avo_bold,
  },
});
