import React from 'react';
import {Modal, StyleSheet, SafeAreaView, TouchableOpacity} from 'react-native';

import * as Icons from '../../config/IconManagers';
import FastImage from 'react-native-fast-image';
import ImageViewer from 'react-native-image-zoom-viewer';
import ProgressLoading from '../common/ProgressLoading';

// crash when zooom local image twice
export default class ImageModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      images: [],
      modalVisible: false,
    };
  }

  showModal = (images, index) => {
    this.setState({
      modalVisible: true,
      index: index || 0,
      images: images.map(e => {
        return {...e, url: e.uri || e.image || e.url};
      }),
    });
  };

  hideModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  _onChange = index => {
    if (this.props.onChange) {
      this.props.onChange(index);
    }
  };

  render() {
    const {rotate} = this.props;
    const {modalVisible, index, images} = this.state;
    const styleImage =
      rotate && rotate != '0deg' ? {transform: [{rotate}]} : {};
    return (
      <Modal visible={modalVisible} transparent={true}>
        <ImageViewer
          index={index}
          imageUrls={images}
          enableSwipeDown={true}
          onSwipeDown={this.hideModal}
          saveToLocalByLongPress={false}
          failImageSource={Icons.no_image}
          onChange={this._onChange}
          loadingRender={() => (
            <ProgressLoading visible={true} style={{}} size={40} />
          )}
          renderImage={props => {
            return <FastImage {...props} style={[props.style, styleImage]} />;
          }}
        />

        <SafeAreaView style={styles.header}>
          <TouchableOpacity activeOpacity={1} onPress={this.hideModal}>
            <FastImage
              source={Icons.remove}
              resizeMode="contain"
              tintColor="white"
              style={styles.image}
            />
          </TouchableOpacity>
        </SafeAreaView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 10,
    right: 0,
  },
  image: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
});
