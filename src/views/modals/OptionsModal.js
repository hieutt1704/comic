import React, {PureComponent} from 'react';
import {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
} from 'react-native';

import I18n from '../../modules/i18n';
import * as Colors from '../../config/Colors';
import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';

class OptionsModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  hideModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  _onPress = item => {
    this.hideModal();
    if (item.onPress) {
      item.onPress();
    }
  };

  render() {
    const {title, options, icon} = this.props;
    const {modalVisible} = this.state;
    return (
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={this.hideModal}
        animationType="slide">
        <TouchableOpacity
          activeOpacity={1}
          onPress={Keyboard.dismiss}
          style={styles.container}>
          <View style={styles.modal}>
            <View style={styles.vIcon}>
              <FastImage
                source={icon}
                style={styles.icon}
                resizeMode={'contain'}
                tintColor="white"
              />
            </View>

            <Text style={[styles.title, {fontSize}]}>{title}</Text>

            {options.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={styles.button}
                activeOpacity={0.7}
                onPress={() => this._onPress(item)}>
                <Text style={[styles.text, {fontSize}]} numberOfLines={1}>
                  {item.text}
                </Text>
              </TouchableOpacity>
            ))}

            <TouchableOpacity
              style={[styles.button, {backgroundColor: Colors.greyViedu}]}
              activeOpacity={0.7}
              onPress={this.hideModal}>
              <Text style={[styles.text, {fontSize}]}>{I18n.t('cancel')}</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

export default OptionsModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000030',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: 250,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  vIcon: {
    backgroundColor: Colors.blueViedu,
    borderColor: 'white',
    borderWidth: 3,
    borderRadius: 25,
    height: 50,
    width: 50,
    position: 'absolute',
    top: -25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 20,
    width: 20,
  },
  title: {
    marginTop: 15,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
  },
  button: {
    width: '100%',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.blueViedu,
    borderRadius: 5,
    marginTop: 10,
  },
  text: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
});
