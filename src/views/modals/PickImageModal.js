import React, {PureComponent} from 'react';
import {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
} from 'react-native';

import I18n from '../../modules/i18n';
import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';
import * as Icons from '../../config/IconManagers';
import {showCamera, showLibrary} from '../../modules/pickImage';

class PickImageModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  hideModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  _openCamera = () => {
    this.hideModal();
    showCamera(res => {
      this._pickImage(res);
    });
  };

  _openLibrary = () => {
    this.hideModal();
    showLibrary(res => {
      this._pickImage(res);
    });
  };

  _pickImage = res => {
    this.hideModal();
    if (this.props.pickImage) {
      this.props.pickImage(res);
    }
  };

  render() {
    const {title} = this.props;
    const {modalVisible} = this.state;
    return (
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={this.hideModal}
        animationType="slide">
        <TouchableOpacity
          activeOpacity={1}
          onPress={Keyboard.dismiss}
          style={styles.container}>
          <View style={styles.modal}>
            <View style={styles.vIcon}>
              <FastImage
                source={Icons.ICON_CAMERA}
                style={styles.icon}
                resizeMode={'contain'}
              />
            </View>

            <Text style={[styles.title, {fontSize}]}>
              {title || I18n.t('create_avatar')}
            </Text>

            <TouchableOpacity
              style={styles.button}
              activeOpacity={0.7}
              onPress={this._openCamera}>
              <Text style={[styles.text, {fontSize}]} numberOfLines={1}>
                {I18n.t('take_photo')}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              activeOpacity={0.7}
              onPress={this._openLibrary}>
              <Text style={[styles.text, {fontSize}]} numberOfLines={1}>
                {I18n.t('choose_image')}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.button, {backgroundColor: 'grey'}]}
              activeOpacity={0.7}
              onPress={this.hideModal}>
              <Text style={[styles.text, {fontSize}]} numberOfLines={1}>
                {I18n.t('cancel')}
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

export default PickImageModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000030',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: 250,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  vIcon: {
    backgroundColor: Colors.redCONMIK,
    borderColor: 'white',
    borderWidth: 3,
    borderRadius: 25,
    height: 50,
    width: 50,
    position: 'absolute',
    top: -25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 20,
    width: 20,
  },
  title: {
    marginTop: 15,
    color: 'black',
    marginBottom: 5,
    textAlign: 'center',
    fontFamily: Fonts.utm_avo_bold,
  },
  button: {
    width: '100%',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.redCONMIK,
    borderRadius: 5,
    marginTop: 10,
  },
  text: {
    color: 'white',
    fontFamily: Fonts.utm_avo_bold,
  },
});
