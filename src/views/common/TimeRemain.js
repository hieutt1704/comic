import React, {PureComponent} from 'react';
import {Text, View} from 'react-native';

import * as Utils from '../../Utils';
import I18n from '../../modules/i18n';
import * as Colors from '../../config/Colors';
import {fontSize} from '../../modules/fontSize';

class TimeRemain extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      time: 300, //s
    };
  }

  componentDidMount() {
    this.Interval = setInterval(
      () => this.setState({time: this.state.time - 1}),
      1000,
    );
  }

  componentWillUnmount() {
    clearInterval(this.Interval);
  }

  componentDidUpdate() {
    if (this.state.time == 0) {
      clearInterval(this.Interval);
      if (this.props.endWatingTime) {
        this.props.endWatingTime();
      }
    }
  }

  render() {
    const {time} = this.state;
    return (
      <View
        style={{marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontSize,
            color: Colors.redViedu,
            fontWeight: 'bold',
            marginBottom: 5,
          }}>
          {I18n.t('time_wait')}:
        </Text>
        <Text style={{fontSize, color: Colors.redViedu, fontWeight: 'bold'}}>
          {Utils.secondsToTime(time)}
        </Text>
      </View>
    );
  }
}

export default TimeRemain;
