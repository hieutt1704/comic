import React, {PureComponent} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';

class CheckBox extends PureComponent {
  render() {
    const {
      style,
      actionIcon,
      unactionIcon,
      title,
      styleText,
      flex,
      styleIcon,
      visible,
      onPress,
    } = this.props;
    const icon = visible ? actionIcon : unactionIcon;
    return (
      <TouchableOpacity
        style={style || [styles.check, {flex}]}
        onPress={onPress}
        activeOpacity={0.7}>
        <FastImage
          source={icon}
          resizeMode="contain"
          style={styleIcon || styles.icon}
        />

        {title ? (
          <Text
            style={[styleText || styles.text, {fontSize}]}
            numberOfLines={1}>
            {title}
          </Text>
        ) : null}
      </TouchableOpacity>
    );
  }
}

export default CheckBox;

const styles = StyleSheet.create({
  check: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderWidth: 0.5,
    borderColor: '#7c7c7c',
    marginTop: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    color: 'black',
  },
  icon: {
    height: 17,
    width: 17,
    marginRight: 5,
  },
});
