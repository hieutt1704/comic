import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';

import * as Fonts from '../../config/Fonts';
import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';
import * as Icons from '../../config/IconManagers';

class ChosseView extends PureComponent {
  measure(cb) {
    if (this.View) {
      this.View.measure((x, y, w, h, pageX, pageY) =>
        cb(x, y, w, h, pageX, pageY),
      );
    }
  }

  render() {
    const {
      styleContainer,
      title,
      styleForm,
      styleText,
      styleHoder,
      noIconRight,
      activeOpacity,
      iconLeft,
      value,
      placeHoder,
      onPress,
    } = this.props;
    return (
      <View
        style={styleContainer}
        ref={ref => (this.View = ref)}
        collapsable={false}>
        {title ? (
          <Text
            numberOfLines={1}
            style={{fontSize: fontSize - 2, color: 'black', marginBottom: 5}}>
            {title.toUpperCase()}
          </Text>
        ) : null}

        <TouchableOpacity
          style={styleForm || styles.pick}
          onPress={onPress}
          activeOpacity={activeOpacity || 0.7}>
          {iconLeft ? (
            <FastImage
              source={iconLeft}
              resizeMode="contain"
              style={{width: 20, height: 20, marginRight: 10}}
            />
          ) : null}

          {value ? (
            <Text style={[styleText || styles.text]}>{value}</Text>
          ) : (
            <Text style={[styleHoder || styles.placeHoder]}>
              {placeHoder || ''}
            </Text>
          )}

          {noIconRight ? null : (
            <FastImage
              source={Icons.down}
              resizeMode="contain"
              style={styles.icon}
            />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

export default ChosseView;

const styles = StyleSheet.create({
  pick: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 0.5,
    borderColor: '#7c7c7c',
    marginTop: 10,
    borderRadius: 5,
    alignItems: 'center',
  },
  text: {
    flex: 1,
    color: 'black',
    fontFamily: Fonts.utm_avo,
    fontSize: 14,
  },
  placeHoder: {
    flex: 1,
    color: 'grey',
    fontFamily: Fonts.utm_avo,
    fontSize: 14,
  },
  icon: {
    height: 10,
    width: 10,
  },
});
