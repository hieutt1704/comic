import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import Spinner from 'react-native-spinkit';
import * as Colors from '../../config/Colors';
import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';
import Menu, {MenuItem} from 'react-native-material-menu';

class IconText extends React.PureComponent {
  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  onPress = () => {
    const {item, index, onPress} = this.props;
    if (item && item.selections) {
      this.showMenu();
    } else if (onPress) {
      onPress(index, item);
    }
  };

  onMenu = (item, index) => {
    this.hideMenu();
    if (this.props.onMenu) {
      this.props.onMenu(this.props.index, item, index);
    }
  };

  render() {
    const {
      item,
      icon,
      title,
      borderColor,
      style,
      styleIcon,
      styleText,
      showAnimetion,
      tintColor,
      activeOpacity,
    } = this.props;
    const styleItem = item
      ? styles.action
      : [styles.box, {borderColor: borderColor || Colors.greyViedu}];
    const styleTitle = item
      ? [styles.title, item.selections ? {marginRight: 10} : {flex: 1}]
      : styles.title;
    const text = item ? item.title : title;
    return (
      <TouchableOpacity
        activeOpacity={activeOpacity || 0.7}
        style={style || styleItem}
        onPress={this.onPress}>
        <FastImage
          source={item ? item.icon : icon}
          style={item ? styles.icon : styleIcon || styles.iconBox}
          resizeMode="contain"
          tintColor={tintColor}
        />

        {text ? (
          <Text style={[styleText || styleTitle, {fontSize}]} numberOfLines={1}>
            {text}
          </Text>
        ) : null}

        {item && item.selections ? (
          <Menu
            ref={ref => (this._menu = ref)}
            button={<Text />}
            style={{borderWidth: 0.2, borderColor: 'grey'}}>
            {item.selections.map((item, index) => (
              <MenuItem
                textStyle={{
                  fontSize: item.fontSize || fontSize,
                  textAlign: 'center',
                  color: item.disabled ? Colors.blueViedu : 'black',
                }}
                onPress={() => this.onMenu(item, index)}
                key={index.toString()}>
                {item.title.toString()}
              </MenuItem>
            ))}
          </Menu>
        ) : null}

        {showAnimetion ? (
          <Spinner
            isVisible={true}
            size={30}
            type={'Wave'}
            color={Colors.greenViedu}
            style={{position: 'absolute', top: 0}}
          />
        ) : null}
      </TouchableOpacity>
    );
  }
}

export default IconText;

const styles = StyleSheet.create({
  action: {
    flexDirection: 'row',
    marginVertical: 15,
    marginLeft: 20,
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  iconBox: {
    width: 20,
    height: 20,
  },
  title: {
    color: 'black',
  },
  box: {
    // maxWidth: 80,
    minWidth: 70, //chua check iphone5
    minHeight: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    marginLeft: 10,
    borderRadius: 5,
    padding: 5,
  },
});
