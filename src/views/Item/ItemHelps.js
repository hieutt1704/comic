import React, {PureComponent} from 'react';
import {Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

import FastImage from 'react-native-fast-image';
import {fontSize} from '../../modules/fontSize';
import * as Colors from '../../config/Colors';
import * as Fonts from '../../config/Fonts';

export default class ItemHelps extends PureComponent {
  render() {
    const {item, onPress, backgroundColor, color} = this.props;
    let imageUrl;

    if (!!item.icon && item.icon.toString().includes('http') == false) {
      imageUrl = item.icon;
    } else {
      imageUrl = {uri: item.icon};
    }
    // console.log('itemmmmmmmmmmm:',imageUrl)
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => onPress(item)}
        style={[
          styles.container,
          {backgroundColor: backgroundColor || Colors.silverCONMIK},
        ]}>
        <FastImage
          source={
            !!item.icon && item.icon.toString().includes('http') == false
              ? item.icon
              : {uri: item.icon.toString()}
          }
          style={styles.icon}
          resizeMode="contain"
          tintColor={color}
        />

        <Text style={[styles.text, {color}]} numberOfLines={1}>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingHorizontal: 15,
    backgroundColor: Colors.silverCONMIK,
    borderRadius: 5,
    marginTop: 10,
  },
  icon: {
    width: 30,
    height: 30,
    marginRight: 15,
  },
  text: {
    flex: 1,
    fontSize,
    color: 'black',
    fontFamily: Fonts.utm_avo,
  },
  next: {
    width: 10,
    height: 10,
  },
});
