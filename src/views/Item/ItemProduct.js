import React, {PureComponent} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  View,
  Text,
} from 'react-native';

import FastImage from 'react-native-fast-image';
import * as Colors from '../../config/Colors';
import * as Icons from '../../config/IconManagers';
import * as Apis from '../../networking/Apis';
import * as Fonts from '../../config/Fonts';

const {width} = Dimensions.get('window');
const WIDTH_COVER = width - 20;
const WIDTH_IMAGE = (WIDTH_COVER - 5) / 2;

export default class ItemProduct extends PureComponent {
  render() {
    const {item, index, onPress, search} = this.props;
    const title = item && item.title ? item.title : '---';
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const marginLeft = search && index % 2 == 0 ? 10 : 0;
    const sale =
      item && item.sale
        ? parseInt(item.sale) >= 100
          ? 'free'
          : parseInt(item.sale) > 0
          ? `-${item.sale}%`
          : ''
        : '';
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => onPress(item)}
        style={{marginRight: 5, overflow: 'hidden', marginLeft}}>
        <FastImage source={{uri: image}} style={styles.image_product} />

        <View style={[styles.header, {backgroundColor: Colors.redCONMIK}]}>
          <Text numberOfLines={3} style={[styles.text]}>
            {title}
          </Text>
        </View>
        {/* <View style={[styles.header, { backgroundColor: Colors.redCONMIK }]}>
                    <Text numberOfLines={2} style={[styles.text,{color:'#333', fontSize:14}]}>
                        {title}
                    </Text>
                </View> */}

        {/* <View style={styles.trans} /> */}

        {/* <View style={styles.vText}>
                    <Text style={styles.text} numberOfLines={4}>
                        {title.toUpperCase()}
                    </Text>
                </View> */}

        {sale ? (
          <View style={styles.sale}>
            <FastImage source={Icons.sale} style={styles.icon} />

            <Text style={styles.textSale}>{sale}</Text>
          </View>
        ) : null}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  image_product: {
    width: WIDTH_IMAGE,
    height: WIDTH_IMAGE / 1.3,
    backgroundColor: Colors.silverCONMIK,
  },
  header: {
    // height: 145,
    height: 60,
    width: WIDTH_IMAGE,
    backgroundColor: 'orange',
    // justifyContent: 'center',
    // alignItems: 'center',
    textAlign: 'left',
    textAlignVertical: 'top',
    // paddingHorizontal: 20,
  },
  trans: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    width: WIDTH_IMAGE,
    backgroundColor: '#00000050',
    transform: [
      {rotate: '-35deg'},
      {translateX: 10},
      {translateY: WIDTH_IMAGE / 4 + 15},
    ],
  },
  vText: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    width: (3 * WIDTH_IMAGE) / 5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  text: {
    color: 'white',
    fontSize: 12,
    textAlign: 'center',
    // textAlign:'justify',
    fontWeight: 'bold',
    // writingDirection:'rtl',
    // paddingRight: 60,
    paddingTop: 5,
    paddingHorizontal: 5,
    paddingLeft: 10,
    fontFamily: Fonts.utm_avo_bold,
    // fontStyle: "italic"
    textTransform: 'uppercase',
  },
  sale: {
    position: 'absolute',
    left: 5,
  },
  icon: {
    width: 37,
    height: 37,
  },
  textSale: {
    color: 'white',
    position: 'absolute',
    width: 37,
    height: 37,
    textAlign: 'center',
    paddingTop: 8,
    fontSize: 12,
    fontWeight: 'bold',
  },
});
