import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Apis from '../../networking/Apis';
import FastImage from 'react-native-fast-image';
import * as Constants from '../../config/Constants';

const Entities = require('html-entities').AllHtmlEntities;

const {width} = Dimensions.get('window');

export default class ItemService extends PureComponent {
  render() {
    const {item, index, onPress, type} = this.props;
    const title = item && item.title ? item.title : '---';
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const content = item && item.intro ? item.intro : '---';
    const backgroundColor =
      item && item.color
        ? item.color
        : (index + 1) % 6 == 1
        ? Colors.orangeSERVICE
        : (index + 1) % 6 == 2
        ? Colors.blueSERVICE
        : (index + 1) % 6 == 3
        ? Colors.yellowSERVICE
        : (index + 1) % 6 == 4
        ? Colors.greenSERVICE
        : (index + 1) % 6 == 5
        ? Colors.greenSERVICE
        : Colors.silverSERVICE;
    const size = type == Constants.TypeObject.SERVICE ? 5 : 0;
    const WIDTH_ITEM = (width - size - 25) / 2;
    const WIDTH_IMAGE = WIDTH_ITEM - 0.6;
    const HEIGHT_IMAGE =
      type == Constants.TypeObject.SERVICE ? WIDTH_IMAGE : WIDTH_IMAGE / 2;
    const marginLeft = type == Constants.TypeObject.SERVICE ? 10 : 0;
    const marginRight = type == Constants.TypeObject.SERVICE ? 0 : 5;
    return (
      <TouchableOpacity
        style={[styles.container, {marginLeft, marginRight, width: WIDTH_ITEM}]}
        activeOpacity={0.8}
        onPress={() => onPress({...item, color: backgroundColor})}>
        <View style={[styles.header, {backgroundColor}]}>
          <Text numberOfLines={2} style={styles.title}>
            {title.toUpperCase()}
          </Text>
        </View>

        <FastImage
          style={[styles.image, {height: HEIGHT_IMAGE, width: WIDTH_IMAGE}]}
          source={{uri: image}}
        />

        <View style={styles.info}>
          <Text numberOfLines={3} style={styles.content}>
            {Entities?.decode(content)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: 'grey',
    borderWidth: 0.3,
  },
  header: {
    height: 45,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  image: {
    backgroundColor: Colors.silverCONMIK,
  },
  info: {
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 5,
  },
  title: {
    fontSize: 11,
    color: 'white',
    textAlign: 'center',
    fontFamily: Fonts.utm_avo_bold,
    fontWeight: '700',
  },
  content: {
    fontSize: 14,
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_regular,
    color: 'black',
    padding: 0,
  },
});
