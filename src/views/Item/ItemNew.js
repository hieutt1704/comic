import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import FastImage from 'react-native-fast-image';
import * as Fonts from '../../config/Fonts';
import * as Colors from '../../config/Colors';
import * as Icons from '../../config/IconManagers';
import * as Apis from '../../networking/Apis';
const Entities = require('html-entities').AllHtmlEntities;

const {width} = Dimensions.get('window');
const WIDTH_IMAGE = width - 20;
const HEIGHT_IMAGE = WIDTH_IMAGE / 2;

export default class ItemNew extends PureComponent {
  render() {
    const {item, onPress, isHome} = this.props;
    const title = item && item.title ? item.title : '---';
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const content = item && item.intro ? item.intro : '---';
    const marginTop = isHome ? 0 : 15;
    const marginHorizontal = isHome ? 0 : 10;
    return (
      <TouchableOpacity
        style={[styles.container, {marginTop, marginHorizontal}]}
        activeOpacity={0.8}
        onPress={() => onPress(item)}>
        <FastImage style={[styles.image]} source={{uri: image}} />

        <View style={{flexDirection: 'row'}}>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>

          {isHome ? null : (
            <FastImage
              style={[styles.icon]}
              source={Icons.next}
              resizeMode="contain"
            />
          )}
        </View>

        <Text numberOfLines={3} style={styles.content}>
          {Entities?.decode(content)}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  image: {
    backgroundColor: Colors.silverCONMIK,
    height: HEIGHT_IMAGE,
    width: WIDTH_IMAGE,
    marginBottom: 5,
  },
  title: {
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_semibold,
    margin: 10,
    marginBottom: 5,
  },
  content: {
    fontSize: 14,
    color: '#606060',
    textAlign: 'justify',
    fontFamily: Fonts.myriadpro_regular,
    marginHorizontal: 10,
  },
  icon: {
    marginTop: 15,
    height: 18,
    width: 18,
  },
});
