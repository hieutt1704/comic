import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import FastImage from 'react-native-fast-image';
import * as Colors from '../../config/Colors';
import * as Apis from '../../networking/Apis';
import * as Fonts from '../../config/Fonts';

const {width} = Dimensions.get('window');
const WIDTH_IMAGE = width - 20;
const HEIGHT_IMAGE = 150;

export default class ItemCate extends PureComponent {
  render() {
    const {item, index, onPress} = this.props;
    const title = item && item.title ? item.title : '---';
    const image =
      item && item.image
        ? item.image.indexOf('http') == 0
          ? item.image
          : Apis.domain + item.image
        : Apis.domain;
    const backgroundColor =
      item && item.color
        ? item.color
        : (index + 1) % 6 == 1
        ? Colors.orangeSERVICE
        : (index + 1) % 6 == 2
        ? Colors.blueSERVICE
        : (index + 1) % 6 == 3
        ? Colors.yellowSERVICE
        : (index + 1) % 6 == 4
        ? Colors.greenSERVICE
        : (index + 1) % 6 == 5
        ? Colors.greenSERVICE
        : Colors.silverSERVICE;
    return (
      <TouchableOpacity
        style={[styles.container]}
        activeOpacity={0.8}
        onPress={onPress}>
        <FastImage
          style={[styles.image, {height: HEIGHT_IMAGE}]}
          source={{uri: image}}
        />

        <View style={[styles.info, {backgroundColor}]} opacity={0.85}>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: WIDTH_IMAGE,
    backgroundColor: Colors.silverCONMIK,
    marginLeft: 10,
  },
  image: {
    width: WIDTH_IMAGE,
    backgroundColor: Colors.silverCONMIK,
  },
  info: {
    position: 'absolute',
    top: HEIGHT_IMAGE / 4,
    paddingVertical: 10,
    paddingHorizontal: 20,
    height: HEIGHT_IMAGE / 2,
    width: WIDTH_IMAGE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    fontFamily: Fonts.utm_avo_bold,
  },
});
