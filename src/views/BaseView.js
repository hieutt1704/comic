import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  Keyboard,
  StatusBar,
} from 'react-native';

import NoData from './common/NoData';
import I18n from '../modules/i18n';
import Header from './common/Header';
import * as Colors from '../config/Colors';
import {fontSize} from '../modules/fontSize';
import * as Icons from '../config/IconManagers';
import NetInfo from '@react-native-community/netinfo';
import ProgressLoading from './common/ProgressLoading';

class BaseView extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      fast_loading: true,
      load_center: false,
      isConnected: true,
      showNetworking: false,
      is_error: false,
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectionInfoChange,
    );
    // Handle screens loading faster
    const {timeOut} = this.props;
    this.timeoutFastLoading = setTimeout(() => {
      if (this._isMounted) {
        this.setState({fast_loading: false});
      }
    }, timeOut || 10);
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
    clearTimeout(this.timeOutNetworking);
    clearTimeout(this.timeoutFastLoading);
    this._isMounted = false;
  }

  _handleConnectionInfoChange = connectionInfo => {
    if (this.state.isConnected != connectionInfo.isConnected) {
      this.setState({isConnected: connectionInfo.isConnected});
    }
    if (connectionInfo.isConnected) {
      this.timeOutNetworking = setTimeout(() => {
        if (this._isMounted) {
          this.setState({showNetworking: false});
        }
      }, 1000);
    } else {
      this.setState({showNetworking: true, load_center: false});
    }
  };

  showLoadCenter() {
    this.setState({load_center: true});
  }

  hideLoadCenter() {
    if (this._isMounted && this.state.load_center) {
      this.setState({load_center: false});
    }
  }

  loadError() {
    if (this._isMounted && !this.state.is_error) {
      this.setState({is_error: true});
    }
  }

  loadDone() {
    if (this._isMounted && this.state.is_error) {
      this.setState({is_error: false});
    }
  }

  getConnect() {
    return this.state.isConnected;
  }

  _onNoData = () => {
    Keyboard.dismiss();
    if (this.props.onNoData) {
      this.showLoadCenter();
      setTimeout(() => {
        this.props.onNoData();
      }, 500);
    }
  };

  refresh = () => {
    this.setState({fast_loading: false});
  };

  render() {
    const {isConnected, showNetworking, load_center, is_error, fast_loading} =
      this.state;
    const {
      style,
      title,
      largeTitle,
      children,
      data,
      supperView,
      headerView,
      footerView,
      noViewNetWorking,
      footerModal,
      bottomModal,
      visibleError,
      iconFail,
      textFail,
      hideTextRetry,
      textNoData,
      iconNoData,
    } = this.props;
    const icon =
      is_error || visibleError
        ? iconFail || Icons.error
        : iconNoData || Icons.box_empty;
    const text =
      is_error || visibleError
        ? textFail || I18n.t('something_wrong')
        : textNoData || I18n.t('no_data');
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        <SafeAreaView style={styles.safe}>
          <View style={styles.container}>
            {showNetworking && !noViewNetWorking ? (
              <NoNetworking isConnected={isConnected} />
            ) : null}

            {title || largeTitle ? <Header {...this.props} /> : <View />}

            {supperView}

            {fast_loading ? null : (
              <View style={style || styles.container}>
                {headerView}

                {load_center ? (
                  <ProgressLoading
                    visible={true}
                    style={styles.center}
                    backgroundColor="white"
                  />
                ) : (data && data.length == 0) || visibleError ? (
                  <NoData
                    onNoData={this._onNoData}
                    iconFail={icon}
                    textFail={text}
                    hideTextRetry={hideTextRetry}
                  />
                ) : (
                  children
                )}

                {footerView}
              </View>
            )}
          </View>
        </SafeAreaView>

        {footerModal}

        {bottomModal}
      </View>
    );
  }
}

export default BaseView;

class NoNetworking extends React.PureComponent {
  render() {
    const {isConnected} = this.props;
    const backgroundColor = isConnected ? Colors.greenCONMIK : Colors.redCONMIK;
    return (
      <View style={[styles.vNetworking, {backgroundColor}]}>
        <Text style={[styles.tNetworking, {fontSize}]}>
          {isConnected ? I18n.t('conected') : I18n.t('no_conected')}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  safe: {
    flex: 1,
    backgroundColor: Colors.greyCONMIK,
  },
  vNetworking: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
  },
  tNetworking: {
    color: 'white',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
