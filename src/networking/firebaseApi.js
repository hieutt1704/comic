
export const firebaseDomain = "https://conmik-8116d-default-rtdb.firebaseio.com"

async function request(
  api,
  method = 'GET',
  header = {'Content-Type': 'application/json'},
  body = '{}',
  type = 'json',
) {
  try {
    let response;
    if (method == 'GET') {
      response = await fetch(api, {headers: new Headers(header), method});
    } else {
      response = await fetch(api, {headers: new Headers(header), method, body});
    }
    // console.log(api, response);
    const responseJson =
      type == 'json' ? await response.json() : await response.text();
    return responseJson;
  } catch (error) {
    return {
      status: 0,
      error,
    };
  }
}

/************************************* API *******************************************/

export function getFlag() {
  const api = firebaseDomain + '/flag.json?print=pretty';
  return request(api, 'GET');
}
