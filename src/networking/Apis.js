// import NetInfo from "@react-native-community/netinfo"
// import * as Icons from '../config/IconManagers';
const isTest = false; //true//

export const domain = isTest
  ? 'http://118.70.180.81:4000/'
  : 'http://conmik.com/'; //192.168.0.126
export const apiUrl = domain + 'api_m/';

// Noti
export const AppIdOneSignal = 'ebbf0745-10a1-4684-b89e-ac8e7335d838';

export let token = '';

export const changeToken = code => (token = code);

async function request(
  api,
  method = isTest ? 'GET' : 'POST',
  header = {'Content-Type': 'application/json'},
  body = '{}',
  type = 'json',
) {
  // Access-Control-Allow-Headers
  // if (!isTest || isAuthorization)
  //     header.Authorization = `Bearer ${token}`
  // const networking = await NetInfo.fetch()
  // if (!networking.isConnected)
  //     return {
  //         status: 0,
  //         smg: 'Không có kết nối mạng'
  //     }
  try {
    let response;
    if (method == 'GET') {
      response = await fetch(api, {headers: new Headers(header), method});
    } else {
      response = await fetch(api, {headers: new Headers(header), method, body});
    }
    // console.log(api, response);
    const responseJson =
      type == 'json' ? await response.json() : await response.text();
    return responseJson;
  } catch (error) {
    return {
      status: 0,
      error,
    };
  }
}

/************************************* LOGIN *******************************************/

export function login(params) {
  const api = apiUrl + 'dangnhap/' + params; //?sdt=097.xxx.xxxx&mk=xxx.xxx
  return request(api);
}

export function signup(params) {
  const api = apiUrl + 'dangky/' + params; //?sdt=097.xxx.xxxx
  return request(api);
}

export function forgot(params) {
  //?sdt=097.xxx.xxxx&rs=0
  const api = apiUrl + 'matkhau/' + params;
  return request(api);
}

export function loginSocial(params) {
  //?accessToken=xxx.xxx@gmail.com
  const api = apiUrl + 'dangkysocial/' + params;
  return request(api);
}

export function checkToken(params) {
  //?token=******
  const api = apiUrl + 'kiemtratoken/' + params;
  return request(api);
}

export function changePassword(params) {
  //?token=****&mkcu=&mkmoi=
  const api = apiUrl + 'doimatkhau/' + params;
  return request(api);
}

/************************************* PROFILE *******************************************/

export function getProfile(params) {
  const api = apiUrl + 'khachhang/' + params; //tk=xxx.xxx&token=
  return request(api, 'GET');
}

export function updateProfile(params) {
  const api =
    apiUrl + (isTest ? 'avatarkhachhang.json' : 'capnhatkhachhang/') + params; //tk=xxx.xxx&token=&name=&birthday=&address=&email=&telephone=
  return request(api, isTest ? 'GET' : 'PUT');
}

export function updateAvatar(params, data) {
  const api = apiUrl + 'avatarkhachhang/' + params; //tk=xxx.xxx&token=
  return request(
    true,
    api,
    isTest ? 'GET' : 'POST',
    {'Content-Type': 'multipart/form-data'},
    data,
  );
}

export function updateCover(params, data) {
  const api = apiUrl + 'coverkhachhang/' + params; //tk=xxx.xxx&token=
  return request(
    true,
    api,
    isTest ? 'GET' : 'POST',
    {'Content-Type': 'multipart/form-data'},
    data,
  );
}

/************************************* App *******************************************/

export function getHome(params) {
  const api = apiUrl + 'trangchu/' + params; //?tk=..&token=..
  console.log('api', api);
  return request(api, 'GET');
}

export function getCates(params) {
  const api = apiUrl + 'danhmucsanpham/' + params; //?tk=..&token=..&page=...
  console.log('getCates', api);
  return request(api, 'GET');
}

export function getProducts(params) {
  const api = apiUrl + 'sanpham/' + params; //?tk=..&token=..&page=...
  console.log('getProducts', api);
  return request(api, 'GET');
}

export function getServices(params) {
  const api = apiUrl + 'dichvu/' + params; //?tk=..&token=..&page=...
  console.log('getServices', api);
  return request(api, 'GET');
}

export function getKeywords(params) {
  const api = apiUrl + 'tukhoa/' + params; //?tk=xxx.xxx&token=...&page=...
  return request(api, 'GET');
}

export function getSearchPopular(params) {
  const api = apiUrl + 'timkiemphobien/' + params; //?tk=xxx.xxx&token=...&page=...
  return request(api, 'GET');
}

export function searchKeywords(params) {
  const api = apiUrl + 'timkiem/' + params; //?tk=xxx.xxx&token=...&kw=&page=...
  return request(api, 'GET');
}

export function getNews(params) {
  const api = apiUrl + 'tintuc/' + params; //?tk=xxx.xxx&token=...&page=...
  return request(api, 'GET');
}

export function getContact(params) {
  const api = apiUrl + 'lienhe/' + params; //?tk=xxx.xxx&token=...
  return request(api, 'GET');
}

export function getDetail(params) {
  const api = apiUrl + 'chitietbaiviet/' + params; //?tk=xxx.xxx&token=...&id=..&data_type=...
  return request(api, 'GET');
}

export function getSales(params) {
  const api = apiUrl + 'khuyenmai/' + params; //?tk=xxx.xxx&token=...
  return request(api, 'GET');
}

export function getNoti(params) {
  const api = apiUrl + 'thongbao/' + params; //?tk=xxx.xxx&token=...&type=...
  return request(api, 'GET');
}

export function getAbout(params) {
  const api = apiUrl + 'gioithieu/' + params; //?tk=xxx.xxx&token=...
  return request(api, 'GET');
}

export function getMenu(params) {
  const api = apiUrl + 'menu-app/';
  // const api = apiUrl + "menu/" + params//?tk=xxx.xxx&token=...
  return request(api, 'GET');
}
