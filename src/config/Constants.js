export const TypeLoading = {
  CENTER: 'CENTER',
  LOAD_MORE: 'LOAD_MORE',
};

export const TypeObject = {
  PRODUCT: 'PRODUCT',
  NEW: 'NEW',
  SERVICE: 'SERVICE',
  PROJECT: 'PROJECT',
  ABOUT: 'ABOUT',
  NOTI: 'NOTI',
};
