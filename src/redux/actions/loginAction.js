import * as Types from '../types';
import * as Apis from '../../networking/Apis';
import * as Storage from '../../modules/asyncStorge';
import NetInfo from '@react-native-community/netinfo';
import * as Routers from '../../navigations/Navigations';

export const login = data => {
  return async dispatch => {
    const {phone, password} = data;
    const params = `?sdt=${phone}&mk=${password}`;
    return Apis.login(params).then(async res => {
      // console.log('login', res, data)
      if (res.status == 1 && res.data && res.data.token) {
        const user = {
          type: Types.LOGIN,
          token: res.data.token,
          username: phone,
        };
        Apis.changeToken(res.data.token);
        Storage.setStorageData(Storage.login, JSON.stringify(user));
        dispatch(user);
      }
      return res;
    });
  };
};

export const loginSocial = data => {
  return async dispatch => {
    const params = `?tk=${data.username}&accessToken=${data.token}&type=${data.type}`;
    // alert(data.username)
    return Apis.loginSocial(params).then(async res => {
      // console.log('loginSoc', res, data)
      if (res.status == 1 && res.data && res.data.data && res.data.data.token) {
        const user = {
          type: Types.LOGIN,
          token: res.data.data.token,
          username: data.username,
          type_account: data.type,
        };
        Apis.changeToken(res.data.data.token);
        Routers.goHome();
        Storage.setStorageData(Storage.login, JSON.stringify(user));
        dispatch(user);
      }
      return res;
    });
  };
};

export const changeIsLogin = user => {
  return {
    type: Types.IS_LOGIN,
    username: user.username,
    token: user.token,
  };
};

export const reLogin = () => {
  return async dispatch => {
    // kiem tra co mang hay khong nham app loading nhanh hon khi mat mang
    const networking = await NetInfo.fetch();
    const user_string = await Storage.getStorageData(Storage.login);
    if (user_string) {
      //Có token được lưu
      const user = JSON.parse(user_string);
      Apis.changeToken(user.token);
      if (networking.isConnected) {
        //Có kết nối mạng => check token
        return Apis.checkToken(`?token=${user.token}`).then(async res => {
          // console.log('relogin', res, user)
          if (res.status == 1 || (res.status == 0 && res.error)) {
            dispatch(user);
            Routers.goHome();
          } else {
            //check token fail => logout
            logout();
          }
        });
      } else {
        // Ko có kết nối => login
        dispatch(user);
        Routers.goHome();
      }
    } else {
      Routers.goLogin();
    }
  };
};

// không cần đưa vào storage
export const logout = async () => {
  //clear token && profile
  Apis.changeToken('');
  Storage.setStorageData(Storage.profile, '');
  Storage.setStorageData(Storage.login, '');
  Storage.setStorageData(Storage.is_login, '');
  Routers.goLogin();
};
