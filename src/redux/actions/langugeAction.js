// import moment from 'moment'
import * as Types from '../types';
import I18n from '../../modules/i18n';
import {changeLimit} from '../../modules/limit';
import * as Storage from '../../modules/asyncStorge';
import {changeFontSize} from '../../modules/fontSize';

export const changeLanguage = locale => {
  return async dispatch => {
    I18n.locale = locale;
    // if (locale == 'vi') {
    //     require('moment/locale/vi')
    //     moment.locale('vi')
    // }
    // else {
    //     require('moment/locale/en-gb')
    //     moment.locale('en-gb')
    // }
    await Storage.setStorageData(Storage.language, locale);
    dispatch({
      type: Types.CHANGE_LANGUAGE,
      data: locale,
    });
  };
};

export const changeSize = size => {
  return async dispatch => {
    changeFontSize(size);
    await Storage.setStorageData(Storage.size_text, size.toString());
    dispatch({
      type: Types.CHANGE_SIZE,
      data: size,
    });
  };
};

export const changeListLimit = limit => {
  return async dispatch => {
    changeLimit(limit);
    await Storage.setStorageData(Storage.limit, limit.toString());
    dispatch({
      type: Types.CHANGE_LIMIT,
      data: limit,
    });
  };
};
