export const LOGIN = 'LOGIN';
export const IS_LOGIN = 'IS_LOGIN';

export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const CHANGE_SIZE = 'CHANGE_SIZE';
export const CHANGE_LIMIT = 'CHANGE_LIMIT';
