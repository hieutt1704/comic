import * as Types from '../types';

const defaultState = {
  locale: 'vi',
  fontSize: 16,
  limit: 1000,
};

const languageReducer = (state = defaultState, action) => {
  if (action.type == Types.CHANGE_LANGUAGE) {
    return {
      ...state,
      locale: action.data,
    };
  }
  if (action.type == Types.CHANGE_SIZE) {
    return {
      ...state,
      fontSize: action.data,
    };
  }
  if (action.type == Types.CHANGE_LIMIT) {
    return {
      ...state,
      limit: action.data,
    };
  }
  return state;
};

export default languageReducer;
