import * as Types from '../types';

const defaultLogin = {
  is_login: false,
  token: 'abc',
  username: ' ',
  type_account: '',
};

const loginReducer = (state = defaultLogin, action) => {
  if (action.type == Types.LOGIN) {
    return {
      ...state,
      token: action.token,
      username: action.username,
      type_account: action.type_account || '',
    };
  }

  if (action.type == Types.IS_LOGIN) {
    return {
      ...state,
      is_login: true,
      username: action.username,
      token: action.token,
    };
  }

  return state;
};

export default loginReducer;
