import {combineReducers} from 'redux';
import loginReducer from './loginReducer';
import languageReducer from './languageReducer';

const reducer = combineReducers({
  loginState: loginReducer,
  languageState: languageReducer,
});

export default reducer;
