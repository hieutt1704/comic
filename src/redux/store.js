import { thunk } from 'redux-thunk'
import reducer from './reducers/reducer';
import {createStore, applyMiddleware} from 'redux';

export default function configureStore(initialState) {
  return createStore(reducer, initialState, applyMiddleware(thunk));
}
