import * as Screens from './ScreenManager';
import * as Icons from '../config/IconManagers';
import {Navigation} from 'react-native-navigation';
import * as Colors from '../config/Colors';
import {Dimensions, Platform} from 'react-native';

const isIOS = Platform.OS == 'ios';
const {width} = Dimensions.get('window');

const DEFAULT_OPTIONS = {
  topBar: {
    visible: false,
    drawBehind: false,
    animate: false,
  },
  layout: {
    orientation: ['portrait'],
  },
  statusBar: {
    style: 'light',
    backgroundColor: Colors.greyCONMIK,
  },
};

const createMainStack = (name, passProps) => {
  return {
    id: Screens.MainStackId,
    children: [
      {
        component: {
          name,
          passProps,
          options: DEFAULT_OPTIONS,
        },
      },
    ],
    options: {
      ...DEFAULT_OPTIONS,
      statusBar: {
        style: 'light',
        backgroundColor: Colors.greyCONMIK,
      },
    },
  };
};

const createBotomTab = (name, icon, text) => {
  return {
    component: {
      name,
      options: {
        ...DEFAULT_OPTIONS,
        bottomTab: {
          icon,
          text,
          // iconInsets: { top: 5, bottom: -5 },
          selectedIconColor: Colors.redCONMIK,
          selectedTextColor: Colors.redCONMIK,
          textColor: Colors.greyCONMIK,
        },
        statusBar: {
          style: 'light',
          backgroundColor: Colors.greyCONMIK,
        },
      },
    },
  };
};

const icon_home = isIOS ? Icons.home : {uri: 'home'};
const icon_news = isIOS ? Icons.news : {uri: 'news'};
const icon_account = isIOS ? Icons.account : {uri: 'account'};
const icon_contact = isIOS ? Icons.contact : {uri: 'contact'};

export const goHome = () =>
  Navigation.setRoot({
    root: {
      sideMenu: {
        id: Screens.SideMenuId,
        left: {
          component: {
            name: Screens.ScreenHelps,
            options: {
              ...DEFAULT_OPTIONS,
              statusBar: {
                style: 'light',
                backgroundColor: Colors.greyCONMIK,
              },
            },
          },
        },
        center: {
          stack: {
            id: Screens.MainStackId,
            children: [
              {
                bottomTabs: {
                  id: Screens.BottomTabsId,
                  children: [
                    createBotomTab(Screens.ScreenHome, icon_home, 'Trang chủ'),
                    createBotomTab(Screens.ScreenCatess, icon_news, 'Sản phẩm'),
                    createBotomTab(
                      Screens.ScreenServices,
                      icon_account,
                      'Dịch vụ',
                    ),
                    createBotomTab(
                      Screens.ScreenContact,
                      icon_contact,
                      'Liên hệ',
                    ),
                  ],
                  options: {
                    ...DEFAULT_OPTIONS,
                    bottomTabs: {
                      animate: false,
                      titleDisplayMode: 'alwaysShow',
                    },
                    statusBar: {
                      style: 'light',
                      backgroundColor: Colors.greyCONMIK,
                    },
                  },
                },
              },
            ],
            options: {
              ...DEFAULT_OPTIONS,
              statusBar: {
                style: 'light',
                backgroundColor: Colors.greyCONMIK,
              },
            },
          },
        },
        options: {
          ...DEFAULT_OPTIONS,
          sideMenu: {
            left: {
              width: (3 * width) / 4,
            },
          },
          statusBar: {
            style: 'light',
            backgroundColor: Colors.greyCONMIK,
          },
        },
      },
    },
  });

export const goLogin = () =>
  Navigation.setRoot({
    root: {
      stack: createMainStack(Screens.ScreenLogin),
    },
  });

const goToScreen = (name, passProps) =>
  Navigation.push(Screens.MainStackId, {
    component: {
      name,
      passProps,
      options: {
        ...DEFAULT_OPTIONS,
        statusBar: {
          style: 'light',
          backgroundColor: Colors.greyCONMIK,
        },
      },
    },
  });

export const showMenu = visible =>
  Navigation.mergeOptions(Screens.SideMenuId, {
    sideMenu: {
      left: {
        visible,
      },
    },
  });

export const goBack = componentId => Navigation.pop(componentId);
export const goBackToRoot = () => Navigation.popToRoot(Screens.MainStackId);

export const goProfile = passProps =>
  goToScreen(Screens.ScreenProfile, passProps);
export const goEditProfile = passProps =>
  goToScreen(Screens.ScreenEditProfile, passProps);
export const goChangePassword = passProps =>
  goToScreen(Screens.ScreenChangePassword, passProps);
export const goContact = passProps =>
  goToScreen(Screens.ScreenContact, passProps);
export const goAbout = passProps => goToScreen(Screens.ScreenAbout, passProps);
export const goForgot = passProps =>
  goToScreen(Screens.ScreenForgot, passProps);
export const goProducts = passProps =>
  goToScreen(Screens.ScreenProducts, passProps);
export const goNews = passProps => goToScreen(Screens.ScreenNews, passProps);
export const goSignup = passProps =>
  goToScreen(Screens.ScreenSignup, passProps);
export const goSearch = passProps =>
  goToScreen(Screens.ScreenSearch, passProps);
export const goDetail = passProps =>
  goToScreen(Screens.ScreenDetail, passProps);
export const goNewDetail = passProps =>
  goToScreen(Screens.ScreenNewDetail, passProps);
export const goNoti = passProps => goToScreen(Screens.ScreenNoti, passProps);
export const goNotiDetail = passProps =>
  goToScreen(Screens.ScreenNotiDetail, passProps);
export const goFilePDF = passProps =>
  goToScreen(Screens.ScreenFilePDF, passProps);
