import React from 'react';
import * as Screens from './ScreenManager';
import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';
import configureStore from '../redux/store';

import App from '../../App';
import Login from '../screens/Login';
import Cates from '../screens/Cates';
import Helps from '../screens/Helps';
import Profile from '../screens/Profile';
import ChangePassword from '../screens/ChangePassword';
import Contact from '../screens/Contact/index';
import About from '../screens/About';
import ForgotPassword from '../screens/ForgotPassword';
import Home from '../screens/Home/index';
import Services from '../screens/Services';
import Products from '../screens/Products';
import News from '../screens/News';
import Signup from '../screens/Signup';
import EditProfile from '../screens/EditProfile';
import Search from '../screens/Search/index';
import Detail from '../screens/Detail';
import NewDetail from '../screens/NewDetail';
import Noti from '../screens/Noti';
import NotiDetail from '../screens/NotiDetail';
import FilePDF from '../screens/FilePDF';

const store = configureStore();

const mapProviderStore = Component => () => props =>
  (
    <Provider store={store}>
      <Component {...props} />
    </Provider>
  );

export function registerScreens() {
  Navigation.registerComponent(
    Screens.ScreenApp,
    mapProviderStore(App),
    () => App,
  );

  Navigation.registerComponent(
    Screens.ScreenLogin,
    mapProviderStore(Login),
    () => Login,
  );

  Navigation.registerComponent(
    Screens.ScreenSignup,
    mapProviderStore(Signup),
    () => Signup,
  );

  Navigation.registerComponent(
    Screens.ScreenChangePassword,
    mapProviderStore(ChangePassword),
    () => ChangePassword,
  );

  Navigation.registerComponent(
    Screens.ScreenHome,
    mapProviderStore(Home),
    () => Home,
  );

  Navigation.registerComponent(
    Screens.ScreenCatess,
    mapProviderStore(Cates),
    () => Cates,
  );

  Navigation.registerComponent(
    Screens.ScreenHelps,
    mapProviderStore(Helps),
    () => Helps,
  );

  Navigation.registerComponent(
    Screens.ScreenProfile,
    mapProviderStore(Profile),
    () => Profile,
  );

  Navigation.registerComponent(
    Screens.ScreenEditProfile,
    mapProviderStore(EditProfile),
    () => EditProfile,
  );

  Navigation.registerComponent(
    Screens.ScreenContact,
    mapProviderStore(Contact),
    () => Contact,
  );

  Navigation.registerComponent(
    Screens.ScreenAbout,
    mapProviderStore(About),
    () => About,
  );

  Navigation.registerComponent(
    Screens.ScreenForgot,
    mapProviderStore(ForgotPassword),
    () => ForgotPassword,
  );

  Navigation.registerComponent(
    Screens.ScreenServices,
    mapProviderStore(Services),
    () => Services,
  );

  Navigation.registerComponent(
    Screens.ScreenProducts,
    mapProviderStore(Products),
    () => Products,
  );

  Navigation.registerComponent(
    Screens.ScreenNews,
    mapProviderStore(News),
    () => News,
  );

  Navigation.registerComponent(
    Screens.ScreenSearch,
    mapProviderStore(Search),
    () => Search,
  );

  Navigation.registerComponent(
    Screens.ScreenDetail,
    mapProviderStore(Detail),
    () => Detail,
  );

  Navigation.registerComponent(
    Screens.ScreenNewDetail,
    mapProviderStore(NewDetail),
    () => NewDetail,
  );

  Navigation.registerComponent(
    Screens.ScreenNoti,
    mapProviderStore(Noti),
    () => Noti,
  );

  Navigation.registerComponent(
    Screens.ScreenNotiDetail,
    mapProviderStore(NotiDetail),
    () => NotiDetail,
  );

  Navigation.registerComponent(
    Screens.ScreenFilePDF,
    mapProviderStore(FilePDF),
    () => FilePDF,
  );
}
