// ROOT
export const SideMenuId = 'conmik.SideMenuId';
export const BottomTabsId = 'conmik.BottomTabsId';
export const MainStackId = 'conmik.MainStackId';
// SCREEN
export const ScreenApp = 'conmik.App';
export const ScreenLogin = 'conmik.Login';
export const ScreenSignup = 'conmik.Signup';
export const ScreenForgot = 'conmik.Forgot';
export const ScreenChangePassword = 'conmik.ChangePassword';
export const ScreenHome = 'conmik.Home';
export const ScreenCatess = 'conmik.Catess';
export const ScreenProfile = 'conmik.Profile';
export const ScreenEditProfile = 'conmik.EditProfile';
export const ScreenHelps = 'conmik.Helps';
export const ScreenContact = 'conmik.Contact';
export const ScreenAbout = 'conmik.About';
export const ScreenServices = 'conmik.Services';
export const ScreenProducts = 'conmik.Products';
export const ScreenNews = 'conmik.News';
export const ScreenSearch = 'conmik.Search';
export const ScreenDetail = 'conmik.Detail';
export const ScreenNewDetail = 'conmik.NewDetail';
export const ScreenNoti = 'conmik.Noti';
export const ScreenNotiDetail = 'conmik.NotiDetail';
export const ScreenFilePDF = 'conmik.FilePDF';
