import AsyncStorage from '@react-native-community/async-storage';

export const login = 'LOGIN';
export const is_login = 'IS_LOGIN';
export const profile = 'PROFILE';
export const home = 'HOME';
export const cates = 'CATES';
export const services = 'SEVICES';
export const contact = 'contact';
export const news = 'NEWS';
export const keywords = 'KEYWORDS';
export const populars = 'POPULARS';
export const sales = 'SALES';
export const noti_sales = 'NOTI_SALES';
export const noti_products = 'NOTI_PRODUCTS';
export const noti_services = 'NOTI_SEVICES';
export const noti_news = 'NOTI_NEWS';
export const about = 'ABOUT';

export const setStorageData = async (storage_Key, stored_value) => {
  try {
    await AsyncStorage.setItem(storage_Key, stored_value);
  } catch (e) {
    // console.log('saving error: ' + e)
  }
};

export const getStorageData = async storage_Key => {
  try {
    const string = await AsyncStorage.getItem(storage_Key);
    return string;
  } catch (e) {
    // console.log('error reading value: ' + e)
    return null;
  }
};

export const clearStorageData = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    // console.log('error reading value: ' + e)
  }
};
