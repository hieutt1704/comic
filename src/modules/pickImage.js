import I18n from './i18n';
// import * as permissions from "./permissions";
import ImagePicker from 'react-native-image-picker';

// image picker have listener permission on android
const options = title => {
  return {
    title: title || I18n.t('create_avatar'),
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
    mediaType: 'photo',
    cancelButtonTitle: I18n.t('cancel'),
    takePhotoButtonTitle: I18n.t('take_photo'),
    chooseFromLibraryButtonTitle: I18n.t('choose_image'),
  };
};

// bug take image from camera rotate 90 deg but take image 90deg
// from library is return a 0deg Image with originalRotation: 90deg
// and cant know when press camera when press library
export const pickImage = (cb, title) => {
  ImagePicker.showImagePicker(options(title), async response => {
    // permissions.checkCameraPhoto(response)
    if (response.didCancel) {
      // console.log("User cancelled image picker");
    } else if (response.error) {
      // console.log("ImagePicker Error: ", response.error);
    } else if (response.customButton) {
      // console.log("User tapped custom button: ", response.customButton);
    } else {
      cb(response);
    }
  });
};

const optionsCamera = {
  storageOptions: {
    skipBackup: true,
    waitUntilSaved: true,
    cameraRoll: true,
    path: '/',
  },
  mediaType: 'photo',
  cameraType: 'back',
  noData: true,
};

export const showCamera = async cb => {
  // await permissions.checkCamera()
  ImagePicker.launchCamera(optionsCamera, response => {
    if (response.didCancel) {
      // console.log("User cancelled image picker");
    } else if (response.error) {
      // console.log("ImagePicker Error: ", response.error);
      // alert("ImagePicker Error: " + response.error)
    } else if (response.customButton) {
      // console.log("User tapped custom button: ", response.customButton);
    } else {
      // console.log(response)
      cb({...response, camera: true});
    }
  });
};

export const showLibrary = async cb => {
  // await permissions.checkPhoto()
  ImagePicker.launchImageLibrary(optionsCamera, response => {
    if (response.didCancel) {
      // console.log("User cancelled image picker");
    } else if (response.error) {
      // console.log("ImagePicker Error: ", response.error);
      // alert("ImagePicker Error: " + response.error)
    } else if (response.customButton) {
      // console.log("User tapped custom button: ", response.customButton);
    } else {
      // console.log(response)
      cb(response);
    }
  });
};
