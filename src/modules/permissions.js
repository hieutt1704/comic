// import { Platform, Alert } from "react-native";

// import I18n from '../modules/i18n'
// // import Permissions from "react-native-permissions";
// import AndroidOpenSettings from 'react-native-android-open-settings'
// // import NotificationManager from 'react-native-check-notification-enable'

// //one of: 'authorized', 'denied', 'restricted', or 'undetermined'
// //- "authorized"	User has authorized this permission
// //- "denied"	User has denied this permission at least once. On iOS this means that the user will not be prompted again.
// //  Android users can be prompted multiple times until they select 'Never ask me again'
// //- "restricted" iOS - this means user is not able to grant this permission, either because it's not supported by
// //  the device or because it has been blocked by parental controls.
// //  Android - this means that the user has selected 'Never ask me again' while denying permission
// //- "undetermined"	User has not yet been prompted with a permission dialog

// const is_iOS = Platform.OS === "ios";

// function _requestPermission(permission, response, cb) {
//   const title = permission == "photo" ?
//     I18n.t("permisstion_photo")
//     : permission == "camera" ?
//       I18n.t("permisstion_camera")
//       : permission == "microphone" ?
//         I18n.t("permisstion_microphone")
//         : permission == "notification" ?
//           I18n.t("permisstion_notification")
//           : I18n.t("permisstion_storage")
//   const message = permission == "photo" ?
//     I18n.t("permisstion_photo_note")
//     : permission == "camera" ?
//       I18n.t("permisstion_camera_note")
//       : permission == "microphone" ?
//         I18n.t("permisstion_microphone_note")
//         : permission == "notification" ?
//           I18n.t("permisstion_notification_note")
//           : I18n.t("permisstion_storage_note")
//   if (is_iOS && response !== "undetermined" || !is_iOS && response == "restricted")
//     Alert.alert(
//       title,
//       message,
//       [
//         {
//           text: I18n.t("no_way"),
//           // onPress: () => console.log("Permission denied"),
//           style: "cancel"
//         },
//         {
//           text: I18n.t("open_setting"),
//           onPress: is_iOS ? Permissions.openSettings : AndroidOpenSettings.appDetailsSettings
//         }
//       ]
//     )
//   else
//     return Permissions.request(permission).then(response => {
//       // console.log("Request android:", response)
//       if (response == "authorized")
//         cb && cb()
//       return response
//     })
// }

// export const checkMicrophone = cb => {
//   if (is_iOS)
//     return Permissions.check("microphone").then(res => {
//       if (res !== 'authorized')
//         _requestPermission("microphone", res, cb)
//       else
//         cb && cb()
//       return res
//     })
//   else
//     return Promise.all([Permissions.check("microphone"), Permissions.check("storage")]).then(res => {
//       if (res[0] !== 'authorized')
//         _requestPermission("microphone", res[0], res[1] == 'authorized' ? cb : null)
//       else if (res[1] !== 'authorized')
//         _requestPermission("storage", res[1], res[0] == 'authorized' ? cb : null)
//       else
//         cb && cb()
//       return res
//     })
// }

// export const checkNotification = () => {
//   if (is_iOS)
//     return Permissions.check("notification").then(response => {
//       if (response !== 'authorized')
//         _requestPermission("notification", response)
//       return response
//     })
//   return false
//   // else if (NotificationManager)
//   //   return NotificationManager.areNotificationsEnabled().then((status) => {
//   //     if (!status)
//   //       _requestPermission("notification", "restricted")
//   //     return status
//   //   }).catch((error) => {
//   //     // console.log("Notification Permission:", error);
//   //     // _requestPermission("notification", "restricted")
//   //     return false
//   //   })
// };

// export const checkCamera = () => {
//   if (is_iOS)
//     return Permissions.check("camera").then(response => {
//       if (response !== 'authorized')
//         _requestPermission("camera", response);
//       return response
//     })
//   else
//     return checkCameraPhoto() //on android rnpickimage need permission camera and photo to open camera
// };

// export const checkPhoto = () => {
//   if (is_iOS)
//     return Permissions.check('photo').then(response => {
//       if (response !== 'authorized')
//         _requestPermission('photo', response);
//       return response
//     })
//   else
//     return checkCameraPhoto() //on android rnpickimage need permission camera and photo to open camera
// };

// export const checkCameraPhoto = resPickImage => {
//   return Permissions.checkMultiple(['camera', 'photo']).then(res => {
//     //ios: camera chưa được cấp quyền và pickimage chọn camera => request permisstion
//     //android: camera chưa được cấp quyền => request permisstion
//     if (is_iOS && res.camera !== 'authorized'
//       && resPickImage.error && resPickImage.error.indexOf('Camera') != -1
//       || !is_iOS && res.camera !== 'authorized')
//       return _requestPermission("camera", res.camera)
//     //photo chưa được cấp quyền và pickimage chọn photo => request permisstion
//     //android: photo chưa được cấp quyền => request permisstion
//     else if (is_iOS && res.photo !== 'authorized'
//       && resPickImage.error && resPickImage.error.indexOf('Photo') != -1
//       || !is_iOS && res.photo !== 'authorized')
//       return _requestPermission("photo", res.photo)
//     else {
//       return res
//     }
//   })
// }
