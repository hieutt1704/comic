import { Navigation } from "react-native-navigation";
import * as Screens from './src/navigations/ScreenManager';
import {registerScreens} from './src/navigations/ScreenRegister';

registerScreens();

Navigation.events().registerAppLaunchedListener(async() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: Screens.ScreenApp,
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false,
                },
                layout: {
                  orientation: ['portrait'],
                },
                statusBar: {
                  style: 'light',
                  backgroundColor: '#d4d4d4',
                },
              },
            },
          },
        ],
      },
    },
  });
});

// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       component: {
//         name: Screens.ScreenApp,
//         options: {
//           topBar: {
//             visible: false,
//             drawBehind: true,
//             animate: false,
//           },
//           layout: {
//             orientation: ['portrait'],
//           },
//           statusBar: {
//             style: 'light',
//             backgroundColor: '#d4d4d4',
//           },
//         },
//       },
//     },
//   });
// });

/**
 * @format
 */

//  import {AppRegistry} from 'react-native';
//  import App from './App';
//  import {name as appName} from './app.json';

//  AppRegistry.registerComponent(appName, () => App);
